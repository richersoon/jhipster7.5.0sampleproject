import { Authority } from '@/shared/security/authority';
/* tslint:disable */
// prettier-ignore
const Entities = () => import('@/entities/entities.vue');

const Company = () => import('@/entities/company/company.vue');
const CompanyUpdate = () => import('@/entities/company/company-update.vue');
const CompanyDetails = () => import('@/entities/company/company-details.vue');

const Shop = () => import('@/entities/shop/shop.vue');
const ShopUpdate = () => import('@/entities/shop/shop-update.vue');
const ShopDetails = () => import('@/entities/shop/shop-details.vue');

const UserCompany = () => import('@/entities/user-company/user-company.vue');
const UserCompanyUpdate = () => import('@/entities/user-company/user-company-update.vue');
const UserCompanyDetails = () => import('@/entities/user-company/user-company-details.vue');

const UserShop = () => import('@/entities/user-shop/user-shop.vue');
const UserShopUpdate = () => import('@/entities/user-shop/user-shop-update.vue');
const UserShopDetails = () => import('@/entities/user-shop/user-shop-details.vue');

const ProductType = () => import('@/entities/product-type/product-type.vue');
const ProductTypeUpdate = () => import('@/entities/product-type/product-type-update.vue');
const ProductTypeDetails = () => import('@/entities/product-type/product-type-details.vue');

const Product = () => import('@/entities/product/product.vue');
const ProductUpdate = () => import('@/entities/product/product-update.vue');
const ProductDetails = () => import('@/entities/product/product-details.vue');

const Order = () => import('@/entities/order/order.vue');
const OrderUpdate = () => import('@/entities/order/order-update.vue');
const OrderDetails = () => import('@/entities/order/order-details.vue');

const OrderItem = () => import('@/entities/order-item/order-item.vue');
const OrderItemUpdate = () => import('@/entities/order-item/order-item-update.vue');
const OrderItemDetails = () => import('@/entities/order-item/order-item-details.vue');

const OrderNote = () => import('@/entities/order-note/order-note.vue');
const OrderNoteUpdate = () => import('@/entities/order-note/order-note-update.vue');
const OrderNoteDetails = () => import('@/entities/order-note/order-note-details.vue');

const OrderApprovalNotification = () => import('@/entities/order-approval-notification/order-approval-notification.vue');
const OrderApprovalNotificationUpdate = () => import('@/entities/order-approval-notification/order-approval-notification-update.vue');
const OrderApprovalNotificationDetails = () => import('@/entities/order-approval-notification/order-approval-notification-details.vue');

const DefaultEmailTemplate = () => import('@/entities/default-email-template/default-email-template.vue');
const DefaultEmailTemplateUpdate = () => import('@/entities/default-email-template/default-email-template-update.vue');
const DefaultEmailTemplateDetails = () => import('@/entities/default-email-template/default-email-template-details.vue');

const ShopEmailTemplate = () => import('@/entities/shop-email-template/shop-email-template.vue');
const ShopEmailTemplateUpdate = () => import('@/entities/shop-email-template/shop-email-template-update.vue');
const ShopEmailTemplateDetails = () => import('@/entities/shop-email-template/shop-email-template-details.vue');

const ShopSmtp = () => import('@/entities/shop-smtp/shop-smtp.vue');
const ShopSmtpUpdate = () => import('@/entities/shop-smtp/shop-smtp-update.vue');
const ShopSmtpDetails = () => import('@/entities/shop-smtp/shop-smtp-details.vue');

const ShopApprovalRule = () => import('@/entities/shop-approval-rule/shop-approval-rule.vue');
const ShopApprovalRuleUpdate = () => import('@/entities/shop-approval-rule/shop-approval-rule-update.vue');
const ShopApprovalRuleDetails = () => import('@/entities/shop-approval-rule/shop-approval-rule-details.vue');

const WebhookSubscription = () => import('@/entities/webhook-subscription/webhook-subscription.vue');
const WebhookSubscriptionUpdate = () => import('@/entities/webhook-subscription/webhook-subscription-update.vue');
const WebhookSubscriptionDetails = () => import('@/entities/webhook-subscription/webhook-subscription-details.vue');

const EventMessage = () => import('@/entities/event-message/event-message.vue');
const EventMessageUpdate = () => import('@/entities/event-message/event-message-update.vue');
const EventMessageDetails = () => import('@/entities/event-message/event-message-details.vue');

const File = () => import('@/entities/file/file.vue');
const FileUpdate = () => import('@/entities/file/file-update.vue');
const FileDetails = () => import('@/entities/file/file-details.vue');

// jhipster-needle-add-entity-to-router-import - JHipster will import entities to the router here

export default {
  path: '/',
  component: Entities,
  children: [
    {
      path: 'company',
      name: 'Company',
      component: Company,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'company/new',
      name: 'CompanyCreate',
      component: CompanyUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'company/:companyId/edit',
      name: 'CompanyEdit',
      component: CompanyUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'company/:companyId/view',
      name: 'CompanyView',
      component: CompanyDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'shop',
      name: 'Shop',
      component: Shop,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'shop/new',
      name: 'ShopCreate',
      component: ShopUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'shop/:shopId/edit',
      name: 'ShopEdit',
      component: ShopUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'shop/:shopId/view',
      name: 'ShopView',
      component: ShopDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'user-company',
      name: 'UserCompany',
      component: UserCompany,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'user-company/new',
      name: 'UserCompanyCreate',
      component: UserCompanyUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'user-company/:userCompanyId/edit',
      name: 'UserCompanyEdit',
      component: UserCompanyUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'user-company/:userCompanyId/view',
      name: 'UserCompanyView',
      component: UserCompanyDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'user-shop',
      name: 'UserShop',
      component: UserShop,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'user-shop/new',
      name: 'UserShopCreate',
      component: UserShopUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'user-shop/:userShopId/edit',
      name: 'UserShopEdit',
      component: UserShopUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'user-shop/:userShopId/view',
      name: 'UserShopView',
      component: UserShopDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'product-type',
      name: 'ProductType',
      component: ProductType,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'product-type/new',
      name: 'ProductTypeCreate',
      component: ProductTypeUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'product-type/:productTypeId/edit',
      name: 'ProductTypeEdit',
      component: ProductTypeUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'product-type/:productTypeId/view',
      name: 'ProductTypeView',
      component: ProductTypeDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'product',
      name: 'Product',
      component: Product,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'product/new',
      name: 'ProductCreate',
      component: ProductUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'product/:productId/edit',
      name: 'ProductEdit',
      component: ProductUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'product/:productId/view',
      name: 'ProductView',
      component: ProductDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'order',
      name: 'Order',
      component: Order,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'order/new',
      name: 'OrderCreate',
      component: OrderUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'order/:orderId/edit',
      name: 'OrderEdit',
      component: OrderUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'order/:orderId/view',
      name: 'OrderView',
      component: OrderDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'order-item',
      name: 'OrderItem',
      component: OrderItem,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'order-item/new',
      name: 'OrderItemCreate',
      component: OrderItemUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'order-item/:orderItemId/edit',
      name: 'OrderItemEdit',
      component: OrderItemUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'order-item/:orderItemId/view',
      name: 'OrderItemView',
      component: OrderItemDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'order-note',
      name: 'OrderNote',
      component: OrderNote,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'order-note/new',
      name: 'OrderNoteCreate',
      component: OrderNoteUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'order-note/:orderNoteId/edit',
      name: 'OrderNoteEdit',
      component: OrderNoteUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'order-note/:orderNoteId/view',
      name: 'OrderNoteView',
      component: OrderNoteDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'order-approval-notification',
      name: 'OrderApprovalNotification',
      component: OrderApprovalNotification,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'order-approval-notification/new',
      name: 'OrderApprovalNotificationCreate',
      component: OrderApprovalNotificationUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'order-approval-notification/:orderApprovalNotificationId/edit',
      name: 'OrderApprovalNotificationEdit',
      component: OrderApprovalNotificationUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'order-approval-notification/:orderApprovalNotificationId/view',
      name: 'OrderApprovalNotificationView',
      component: OrderApprovalNotificationDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'default-email-template',
      name: 'DefaultEmailTemplate',
      component: DefaultEmailTemplate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'default-email-template/new',
      name: 'DefaultEmailTemplateCreate',
      component: DefaultEmailTemplateUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'default-email-template/:defaultEmailTemplateId/edit',
      name: 'DefaultEmailTemplateEdit',
      component: DefaultEmailTemplateUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'default-email-template/:defaultEmailTemplateId/view',
      name: 'DefaultEmailTemplateView',
      component: DefaultEmailTemplateDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'shop-email-template',
      name: 'ShopEmailTemplate',
      component: ShopEmailTemplate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'shop-email-template/new',
      name: 'ShopEmailTemplateCreate',
      component: ShopEmailTemplateUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'shop-email-template/:shopEmailTemplateId/edit',
      name: 'ShopEmailTemplateEdit',
      component: ShopEmailTemplateUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'shop-email-template/:shopEmailTemplateId/view',
      name: 'ShopEmailTemplateView',
      component: ShopEmailTemplateDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'shop-smtp',
      name: 'ShopSmtp',
      component: ShopSmtp,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'shop-smtp/new',
      name: 'ShopSmtpCreate',
      component: ShopSmtpUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'shop-smtp/:shopSmtpId/edit',
      name: 'ShopSmtpEdit',
      component: ShopSmtpUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'shop-smtp/:shopSmtpId/view',
      name: 'ShopSmtpView',
      component: ShopSmtpDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'shop-approval-rule',
      name: 'ShopApprovalRule',
      component: ShopApprovalRule,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'shop-approval-rule/new',
      name: 'ShopApprovalRuleCreate',
      component: ShopApprovalRuleUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'shop-approval-rule/:shopApprovalRuleId/edit',
      name: 'ShopApprovalRuleEdit',
      component: ShopApprovalRuleUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'shop-approval-rule/:shopApprovalRuleId/view',
      name: 'ShopApprovalRuleView',
      component: ShopApprovalRuleDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'webhook-subscription',
      name: 'WebhookSubscription',
      component: WebhookSubscription,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'webhook-subscription/new',
      name: 'WebhookSubscriptionCreate',
      component: WebhookSubscriptionUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'webhook-subscription/:webhookSubscriptionId/edit',
      name: 'WebhookSubscriptionEdit',
      component: WebhookSubscriptionUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'webhook-subscription/:webhookSubscriptionId/view',
      name: 'WebhookSubscriptionView',
      component: WebhookSubscriptionDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'event-message',
      name: 'EventMessage',
      component: EventMessage,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'event-message/new',
      name: 'EventMessageCreate',
      component: EventMessageUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'event-message/:eventMessageId/edit',
      name: 'EventMessageEdit',
      component: EventMessageUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'event-message/:eventMessageId/view',
      name: 'EventMessageView',
      component: EventMessageDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'file',
      name: 'File',
      component: File,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'file/new',
      name: 'FileCreate',
      component: FileUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'file/:fileId/edit',
      name: 'FileEdit',
      component: FileUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'file/:fileId/view',
      name: 'FileView',
      component: FileDetails,
      meta: { authorities: [Authority.USER] },
    },
    // jhipster-needle-add-entity-to-router - JHipster will add entities to the router here
  ],
};
