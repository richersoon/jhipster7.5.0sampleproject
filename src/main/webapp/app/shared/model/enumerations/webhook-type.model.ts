export enum WebhookType {
  ORDER_PAID = 'ORDER_PAID',

  APP_UNINSTALLED = 'APP_UNINSTALLED',
}
