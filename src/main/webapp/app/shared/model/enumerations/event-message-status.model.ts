export enum EventMessageStatus {
  PENDING = 'PENDING',

  COMPLETED = 'COMPLETED',

  ERROR = 'ERROR',
}
