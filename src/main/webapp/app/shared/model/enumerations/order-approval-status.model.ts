export enum OrderApprovalStatus {
  PENDING = 'PENDING',

  COMPLETED = 'COMPLETED',

  ERROR = 'ERROR',
}
