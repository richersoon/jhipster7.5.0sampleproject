export enum ExternalType {
  SHOPIFY = 'SHOPIFY',

  WOOCOMMERCE = 'WOOCOMMERCE',

  EBAY = 'EBAY',
}
