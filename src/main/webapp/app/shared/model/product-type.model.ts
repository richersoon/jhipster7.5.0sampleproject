import { IProduct } from '@/shared/model/product.model';
import { IOrderItem } from '@/shared/model/order-item.model';
import { IShop } from '@/shared/model/shop.model';

export interface IProductType {
  id?: number;
  externalId?: string | null;
  identifier?: string;
  saneName?: string;
  createdBy?: string | null;
  createdDate?: Date | null;
  lastModifiedBy?: string | null;
  lastModifiedDate?: Date | null;
  version?: number | null;
  products?: IProduct[] | null;
  orderItems?: IOrderItem[] | null;
  shop?: IShop;
}

export class ProductType implements IProductType {
  constructor(
    public id?: number,
    public externalId?: string | null,
    public identifier?: string,
    public saneName?: string,
    public createdBy?: string | null,
    public createdDate?: Date | null,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: Date | null,
    public version?: number | null,
    public products?: IProduct[] | null,
    public orderItems?: IOrderItem[] | null,
    public shop?: IShop
  ) {}
}
