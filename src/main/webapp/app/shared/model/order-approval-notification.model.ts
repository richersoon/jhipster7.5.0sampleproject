import { IOrder } from '@/shared/model/order.model';

import { OrderApprovalStatus } from '@/shared/model/enumerations/order-approval-status.model';
export interface IOrderApprovalNotification {
  id?: number;
  status?: OrderApprovalStatus | null;
  browserIpCity?: string | null;
  browserIpProvince?: string | null;
  browserIpCountry?: string | null;
  browserIpLatitude?: string | null;
  browserIpLongitude?: string | null;
  browserIpZip?: string | null;
  distanceKm?: number | null;
  durationSeconds?: number | null;
  note?: string | null;
  createdBy?: string | null;
  createdDate?: Date | null;
  lastModifiedBy?: string | null;
  lastModifiedDate?: Date | null;
  version?: number | null;
  order?: IOrder | null;
}

export class OrderApprovalNotification implements IOrderApprovalNotification {
  constructor(
    public id?: number,
    public status?: OrderApprovalStatus | null,
    public browserIpCity?: string | null,
    public browserIpProvince?: string | null,
    public browserIpCountry?: string | null,
    public browserIpLatitude?: string | null,
    public browserIpLongitude?: string | null,
    public browserIpZip?: string | null,
    public distanceKm?: number | null,
    public durationSeconds?: number | null,
    public note?: string | null,
    public createdBy?: string | null,
    public createdDate?: Date | null,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: Date | null,
    public version?: number | null,
    public order?: IOrder | null
  ) {}
}
