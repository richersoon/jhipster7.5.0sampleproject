import { IProductType } from '@/shared/model/product-type.model';
import { IOrder } from '@/shared/model/order.model';
import { IProduct } from '@/shared/model/product.model';

import { OrderItemStatus } from '@/shared/model/enumerations/order-item-status.model';
export interface IOrderItem {
  id?: number;
  notes?: string | null;
  status?: OrderItemStatus;
  productTypeIdentifier?: string | null;
  createdBy?: string | null;
  createdDate?: Date | null;
  lastModifiedBy?: string | null;
  lastModifiedDate?: Date | null;
  version?: number | null;
  productType?: IProductType | null;
  order?: IOrder;
  product?: IProduct | null;
}

export class OrderItem implements IOrderItem {
  constructor(
    public id?: number,
    public notes?: string | null,
    public status?: OrderItemStatus,
    public productTypeIdentifier?: string | null,
    public createdBy?: string | null,
    public createdDate?: Date | null,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: Date | null,
    public version?: number | null,
    public productType?: IProductType | null,
    public order?: IOrder,
    public product?: IProduct | null
  ) {}
}
