import { ICompany } from '@/shared/model/company.model';
import { IShop } from '@/shared/model/shop.model';
import { IUser } from '@/shared/model/user.model';

export interface IFile {
  id?: number;
  name?: string | null;
  url?: string;
  mimeType?: string;
  createdBy?: string | null;
  createdDate?: Date | null;
  lastModifiedBy?: string | null;
  lastModifiedDate?: Date | null;
  version?: number | null;
  companies?: ICompany[] | null;
  shops?: IShop[] | null;
  uploaderUser?: IUser;
}

export class File implements IFile {
  constructor(
    public id?: number,
    public name?: string | null,
    public url?: string,
    public mimeType?: string,
    public createdBy?: string | null,
    public createdDate?: Date | null,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: Date | null,
    public version?: number | null,
    public companies?: ICompany[] | null,
    public shops?: IShop[] | null,
    public uploaderUser?: IUser
  ) {}
}
