import { IOrder } from '@/shared/model/order.model';

export interface IOrderNote {
  id?: number;
  note?: string | null;
  createdBy?: string | null;
  createdDate?: Date | null;
  lastModifiedBy?: string | null;
  lastModifiedDate?: Date | null;
  version?: number | null;
  order?: IOrder;
}

export class OrderNote implements IOrderNote {
  constructor(
    public id?: number,
    public note?: string | null,
    public createdBy?: string | null,
    public createdDate?: Date | null,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: Date | null,
    public version?: number | null,
    public order?: IOrder
  ) {}
}
