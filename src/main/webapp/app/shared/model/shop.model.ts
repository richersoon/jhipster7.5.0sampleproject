import { IUserShop } from '@/shared/model/user-shop.model';
import { IProductType } from '@/shared/model/product-type.model';
import { IOrder } from '@/shared/model/order.model';
import { IShopEmailTemplate } from '@/shared/model/shop-email-template.model';
import { IShopApprovalRule } from '@/shared/model/shop-approval-rule.model';
import { IWebhookSubscription } from '@/shared/model/webhook-subscription.model';
import { IShopSmtp } from '@/shared/model/shop-smtp.model';
import { ICompany } from '@/shared/model/company.model';
import { IFile } from '@/shared/model/file.model';

import { ExternalType } from '@/shared/model/enumerations/external-type.model';
export interface IShop {
  id?: number;
  name?: string;
  url?: string;
  externalType?: ExternalType | null;
  authorizationCode?: string | null;
  generatedToken?: string | null;
  accessToken?: string | null;
  consumerKey?: string | null;
  consumerSecret?: string | null;
  createdBy?: string | null;
  createdDate?: Date | null;
  lastModifiedBy?: string | null;
  lastModifiedDate?: Date | null;
  version?: number | null;
  userShops?: IUserShop[] | null;
  productTypes?: IProductType[] | null;
  orders?: IOrder[] | null;
  shopEmailTemplates?: IShopEmailTemplate[] | null;
  shopApprovalRules?: IShopApprovalRule[] | null;
  webhookSubscriptions?: IWebhookSubscription[] | null;
  shopSmtp?: IShopSmtp | null;
  company?: ICompany;
  logoFile?: IFile | null;
}

export class Shop implements IShop {
  constructor(
    public id?: number,
    public name?: string,
    public url?: string,
    public externalType?: ExternalType | null,
    public authorizationCode?: string | null,
    public generatedToken?: string | null,
    public accessToken?: string | null,
    public consumerKey?: string | null,
    public consumerSecret?: string | null,
    public createdBy?: string | null,
    public createdDate?: Date | null,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: Date | null,
    public version?: number | null,
    public userShops?: IUserShop[] | null,
    public productTypes?: IProductType[] | null,
    public orders?: IOrder[] | null,
    public shopEmailTemplates?: IShopEmailTemplate[] | null,
    public shopApprovalRules?: IShopApprovalRule[] | null,
    public webhookSubscriptions?: IWebhookSubscription[] | null,
    public shopSmtp?: IShopSmtp | null,
    public company?: ICompany,
    public logoFile?: IFile | null
  ) {}
}
