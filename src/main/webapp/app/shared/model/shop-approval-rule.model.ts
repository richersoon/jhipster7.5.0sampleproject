import { IShop } from '@/shared/model/shop.model';

import { ApprovalRule } from '@/shared/model/enumerations/approval-rule.model';
export interface IShopApprovalRule {
  id?: number;
  approvalRule?: ApprovalRule;
  value?: string;
  createdBy?: string | null;
  createdDate?: Date | null;
  lastModifiedBy?: string | null;
  lastModifiedDate?: Date | null;
  version?: number | null;
  shop?: IShop;
}

export class ShopApprovalRule implements IShopApprovalRule {
  constructor(
    public id?: number,
    public approvalRule?: ApprovalRule,
    public value?: string,
    public createdBy?: string | null,
    public createdDate?: Date | null,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: Date | null,
    public version?: number | null,
    public shop?: IShop
  ) {}
}
