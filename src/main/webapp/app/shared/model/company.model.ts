import { IShop } from '@/shared/model/shop.model';
import { IUserCompany } from '@/shared/model/user-company.model';
import { IFile } from '@/shared/model/file.model';

export interface ICompany {
  id?: number;
  name?: string;
  createdBy?: string | null;
  createdDate?: Date | null;
  lastModifiedBy?: string | null;
  lastModifiedDate?: Date | null;
  version?: number | null;
  shops?: IShop[] | null;
  userCompanies?: IUserCompany[] | null;
  logoFile?: IFile | null;
}

export class Company implements ICompany {
  constructor(
    public id?: number,
    public name?: string,
    public createdBy?: string | null,
    public createdDate?: Date | null,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: Date | null,
    public version?: number | null,
    public shops?: IShop[] | null,
    public userCompanies?: IUserCompany[] | null,
    public logoFile?: IFile | null
  ) {}
}
