import { IOrderItem } from '@/shared/model/order-item.model';
import { IProductType } from '@/shared/model/product-type.model';

export interface IProduct {
  id?: number;
  product?: string;
  soldQuantity?: number;
  availableQuantity?: number;
  invalidatedDate?: Date | null;
  createdBy?: string | null;
  createdDate?: Date | null;
  lastModifiedBy?: string | null;
  lastModifiedDate?: Date | null;
  version?: number | null;
  orderItems?: IOrderItem[] | null;
  productType?: IProductType;
}

export class Product implements IProduct {
  constructor(
    public id?: number,
    public product?: string,
    public soldQuantity?: number,
    public availableQuantity?: number,
    public invalidatedDate?: Date | null,
    public createdBy?: string | null,
    public createdDate?: Date | null,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: Date | null,
    public version?: number | null,
    public orderItems?: IOrderItem[] | null,
    public productType?: IProductType
  ) {}
}
