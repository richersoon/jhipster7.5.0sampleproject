import { IShop } from '@/shared/model/shop.model';

export interface IShopSmtp {
  id?: number;
  host?: string;
  port?: number;
  fromEmail?: string;
  fromName?: string;
  replyTo?: string;
  username?: string;
  password?: string;
  createdBy?: string | null;
  createdDate?: Date | null;
  lastModifiedBy?: string | null;
  lastModifiedDate?: Date | null;
  version?: number | null;
  shop?: IShop;
}

export class ShopSmtp implements IShopSmtp {
  constructor(
    public id?: number,
    public host?: string,
    public port?: number,
    public fromEmail?: string,
    public fromName?: string,
    public replyTo?: string,
    public username?: string,
    public password?: string,
    public createdBy?: string | null,
    public createdDate?: Date | null,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: Date | null,
    public version?: number | null,
    public shop?: IShop
  ) {}
}
