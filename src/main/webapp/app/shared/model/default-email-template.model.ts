import { IShopEmailTemplate } from '@/shared/model/shop-email-template.model';

import { Language } from '@/shared/model/enumerations/language.model';
export interface IDefaultEmailTemplate {
  id?: number;
  subject?: string;
  header?: string;
  body?: string;
  footer?: string;
  language?: Language;
  createdBy?: string | null;
  createdDate?: Date | null;
  lastModifiedBy?: string | null;
  lastModifiedDate?: Date | null;
  version?: number | null;
  shopEmailTemplates?: IShopEmailTemplate[] | null;
}

export class DefaultEmailTemplate implements IDefaultEmailTemplate {
  constructor(
    public id?: number,
    public subject?: string,
    public header?: string,
    public body?: string,
    public footer?: string,
    public language?: Language,
    public createdBy?: string | null,
    public createdDate?: Date | null,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: Date | null,
    public version?: number | null,
    public shopEmailTemplates?: IShopEmailTemplate[] | null
  ) {}
}
