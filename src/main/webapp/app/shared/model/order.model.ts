import { IOrderItem } from '@/shared/model/order-item.model';
import { IOrderNote } from '@/shared/model/order-note.model';
import { IEventMessage } from '@/shared/model/event-message.model';
import { IOrderApprovalNotification } from '@/shared/model/order-approval-notification.model';
import { IShop } from '@/shared/model/shop.model';

import { ExternalType } from '@/shared/model/enumerations/external-type.model';
import { OrderStatus } from '@/shared/model/enumerations/order-status.model';
export interface IOrder {
  id?: number;
  orderNo?: string | null;
  externalType?: ExternalType | null;
  firstname?: string | null;
  lastname?: string | null;
  email?: string | null;
  language?: string | null;
  totalPrice?: number | null;
  paymentGateway?: string | null;
  status?: OrderStatus | null;
  ip?: string | null;
  street?: string | null;
  city?: string | null;
  province?: string | null;
  country?: string | null;
  zip?: string | null;
  latitude?: string | null;
  longitude?: string | null;
  createdBy?: string | null;
  createdDate?: Date | null;
  lastModifiedBy?: string | null;
  lastModifiedDate?: Date | null;
  version?: number | null;
  orderItems?: IOrderItem[] | null;
  orderNotes?: IOrderNote[] | null;
  eventMessages?: IEventMessage[] | null;
  orderApprovalNotifications?: IOrderApprovalNotification[] | null;
  shop?: IShop;
}

export class Order implements IOrder {
  constructor(
    public id?: number,
    public orderNo?: string | null,
    public externalType?: ExternalType | null,
    public firstname?: string | null,
    public lastname?: string | null,
    public email?: string | null,
    public language?: string | null,
    public totalPrice?: number | null,
    public paymentGateway?: string | null,
    public status?: OrderStatus | null,
    public ip?: string | null,
    public street?: string | null,
    public city?: string | null,
    public province?: string | null,
    public country?: string | null,
    public zip?: string | null,
    public latitude?: string | null,
    public longitude?: string | null,
    public createdBy?: string | null,
    public createdDate?: Date | null,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: Date | null,
    public version?: number | null,
    public orderItems?: IOrderItem[] | null,
    public orderNotes?: IOrderNote[] | null,
    public eventMessages?: IEventMessage[] | null,
    public orderApprovalNotifications?: IOrderApprovalNotification[] | null,
    public shop?: IShop
  ) {}
}
