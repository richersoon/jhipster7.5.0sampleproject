import { IUser } from '@/shared/model/user.model';
import { ICompany } from '@/shared/model/company.model';

import { Role } from '@/shared/model/enumerations/role.model';
export interface IUserCompany {
  id?: number;
  role?: Role;
  permanent?: boolean;
  createdBy?: string | null;
  createdDate?: Date | null;
  lastModifiedBy?: string | null;
  lastModifiedDate?: Date | null;
  version?: number | null;
  user?: IUser;
  company?: ICompany;
}

export class UserCompany implements IUserCompany {
  constructor(
    public id?: number,
    public role?: Role,
    public permanent?: boolean,
    public createdBy?: string | null,
    public createdDate?: Date | null,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: Date | null,
    public version?: number | null,
    public user?: IUser,
    public company?: ICompany
  ) {
    this.permanent = this.permanent ?? false;
  }
}
