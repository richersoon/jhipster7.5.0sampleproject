import { IUser } from '@/shared/model/user.model';
import { IShop } from '@/shared/model/shop.model';

import { Role } from '@/shared/model/enumerations/role.model';
export interface IUserShop {
  id?: number;
  role?: Role;
  createdBy?: string | null;
  createdDate?: Date | null;
  lastModifiedBy?: string | null;
  lastModifiedDate?: Date | null;
  version?: number | null;
  user?: IUser;
  shop?: IShop;
}

export class UserShop implements IUserShop {
  constructor(
    public id?: number,
    public role?: Role,
    public createdBy?: string | null,
    public createdDate?: Date | null,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: Date | null,
    public version?: number | null,
    public user?: IUser,
    public shop?: IShop
  ) {}
}
