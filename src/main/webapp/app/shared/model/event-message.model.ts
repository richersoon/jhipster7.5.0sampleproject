import { IOrder } from '@/shared/model/order.model';

import { EventMessageStatus } from '@/shared/model/enumerations/event-message-status.model';
export interface IEventMessage {
  id?: number;
  payload?: string;
  header?: string;
  status?: EventMessageStatus;
  notes?: string | null;
  createdBy?: string | null;
  createdDate?: Date | null;
  lastModifiedBy?: string | null;
  lastModifiedDate?: Date | null;
  version?: number | null;
  order?: IOrder | null;
}

export class EventMessage implements IEventMessage {
  constructor(
    public id?: number,
    public payload?: string,
    public header?: string,
    public status?: EventMessageStatus,
    public notes?: string | null,
    public createdBy?: string | null,
    public createdDate?: Date | null,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: Date | null,
    public version?: number | null,
    public order?: IOrder | null
  ) {}
}
