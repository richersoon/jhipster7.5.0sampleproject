import { IShop } from '@/shared/model/shop.model';

import { WebhookType } from '@/shared/model/enumerations/webhook-type.model';
export interface IWebhookSubscription {
  id?: number;
  externalId?: string;
  type?: WebhookType;
  createdBy?: string | null;
  createdDate?: Date | null;
  lastModifiedBy?: string | null;
  lastModifiedDate?: Date | null;
  version?: number | null;
  shop?: IShop;
}

export class WebhookSubscription implements IWebhookSubscription {
  constructor(
    public id?: number,
    public externalId?: string,
    public type?: WebhookType,
    public createdBy?: string | null,
    public createdDate?: Date | null,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: Date | null,
    public version?: number | null,
    public shop?: IShop
  ) {}
}
