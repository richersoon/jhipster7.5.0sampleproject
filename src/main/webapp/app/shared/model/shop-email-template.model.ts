import { IDefaultEmailTemplate } from '@/shared/model/default-email-template.model';
import { IShop } from '@/shared/model/shop.model';

export interface IShopEmailTemplate {
  id?: number;
  subject?: string;
  header?: string;
  body?: string;
  footer?: string;
  replyTo?: string;
  createdBy?: string | null;
  createdDate?: Date | null;
  lastModifiedBy?: string | null;
  lastModifiedDate?: Date | null;
  version?: number | null;
  defaultEmailTemplate?: IDefaultEmailTemplate;
  shop?: IShop;
}

export class ShopEmailTemplate implements IShopEmailTemplate {
  constructor(
    public id?: number,
    public subject?: string,
    public header?: string,
    public body?: string,
    public footer?: string,
    public replyTo?: string,
    public createdBy?: string | null,
    public createdDate?: Date | null,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: Date | null,
    public version?: number | null,
    public defaultEmailTemplate?: IDefaultEmailTemplate,
    public shop?: IShop
  ) {}
}
