import { Component, Provide, Vue } from 'vue-property-decorator';

import UserService from '@/entities/user/user.service';
import CompanyService from './company/company.service';
import ShopService from './shop/shop.service';
import UserCompanyService from './user-company/user-company.service';
import UserShopService from './user-shop/user-shop.service';
import ProductTypeService from './product-type/product-type.service';
import ProductService from './product/product.service';
import OrderService from './order/order.service';
import OrderItemService from './order-item/order-item.service';
import OrderNoteService from './order-note/order-note.service';
import OrderApprovalNotificationService from './order-approval-notification/order-approval-notification.service';
import DefaultEmailTemplateService from './default-email-template/default-email-template.service';
import ShopEmailTemplateService from './shop-email-template/shop-email-template.service';
import ShopSmtpService from './shop-smtp/shop-smtp.service';
import ShopApprovalRuleService from './shop-approval-rule/shop-approval-rule.service';
import WebhookSubscriptionService from './webhook-subscription/webhook-subscription.service';
import EventMessageService from './event-message/event-message.service';
import FileService from './file/file.service';
// jhipster-needle-add-entity-service-to-entities-component-import - JHipster will import entities services here

@Component
export default class Entities extends Vue {
  @Provide('userService') private userService = () => new UserService();
  @Provide('companyService') private companyService = () => new CompanyService();
  @Provide('shopService') private shopService = () => new ShopService();
  @Provide('userCompanyService') private userCompanyService = () => new UserCompanyService();
  @Provide('userShopService') private userShopService = () => new UserShopService();
  @Provide('productTypeService') private productTypeService = () => new ProductTypeService();
  @Provide('productService') private productService = () => new ProductService();
  @Provide('orderService') private orderService = () => new OrderService();
  @Provide('orderItemService') private orderItemService = () => new OrderItemService();
  @Provide('orderNoteService') private orderNoteService = () => new OrderNoteService();
  @Provide('orderApprovalNotificationService') private orderApprovalNotificationService = () => new OrderApprovalNotificationService();
  @Provide('defaultEmailTemplateService') private defaultEmailTemplateService = () => new DefaultEmailTemplateService();
  @Provide('shopEmailTemplateService') private shopEmailTemplateService = () => new ShopEmailTemplateService();
  @Provide('shopSmtpService') private shopSmtpService = () => new ShopSmtpService();
  @Provide('shopApprovalRuleService') private shopApprovalRuleService = () => new ShopApprovalRuleService();
  @Provide('webhookSubscriptionService') private webhookSubscriptionService = () => new WebhookSubscriptionService();
  @Provide('eventMessageService') private eventMessageService = () => new EventMessageService();
  @Provide('fileService') private fileService = () => new FileService();
  // jhipster-needle-add-entity-service-to-entities-component - JHipster will import entities services here
}
