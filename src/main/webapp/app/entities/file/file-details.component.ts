import { Component, Vue, Inject } from 'vue-property-decorator';

import { IFile } from '@/shared/model/file.model';
import FileService from './file.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class FileDetails extends Vue {
  @Inject('fileService') private fileService: () => FileService;
  @Inject('alertService') private alertService: () => AlertService;

  public file: IFile = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.fileId) {
        vm.retrieveFile(to.params.fileId);
      }
    });
  }

  public retrieveFile(fileId) {
    this.fileService()
      .find(fileId)
      .then(res => {
        this.file = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
