import { Component, Vue, Inject } from 'vue-property-decorator';

import { IShopApprovalRule } from '@/shared/model/shop-approval-rule.model';
import ShopApprovalRuleService from './shop-approval-rule.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class ShopApprovalRuleDetails extends Vue {
  @Inject('shopApprovalRuleService') private shopApprovalRuleService: () => ShopApprovalRuleService;
  @Inject('alertService') private alertService: () => AlertService;

  public shopApprovalRule: IShopApprovalRule = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.shopApprovalRuleId) {
        vm.retrieveShopApprovalRule(to.params.shopApprovalRuleId);
      }
    });
  }

  public retrieveShopApprovalRule(shopApprovalRuleId) {
    this.shopApprovalRuleService()
      .find(shopApprovalRuleId)
      .then(res => {
        this.shopApprovalRule = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
