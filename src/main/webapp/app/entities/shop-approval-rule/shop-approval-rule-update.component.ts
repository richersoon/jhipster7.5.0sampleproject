import { Component, Vue, Inject } from 'vue-property-decorator';

import { required } from 'vuelidate/lib/validators';
import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import ShopService from '@/entities/shop/shop.service';
import { IShop } from '@/shared/model/shop.model';

import { IShopApprovalRule, ShopApprovalRule } from '@/shared/model/shop-approval-rule.model';
import ShopApprovalRuleService from './shop-approval-rule.service';
import { ApprovalRule } from '@/shared/model/enumerations/approval-rule.model';

const validations: any = {
  shopApprovalRule: {
    approvalRule: {
      required,
    },
    value: {
      required,
    },
    createdBy: {},
    createdDate: {},
    lastModifiedBy: {},
    lastModifiedDate: {},
    version: {},
    shop: {
      required,
    },
  },
};

@Component({
  validations,
})
export default class ShopApprovalRuleUpdate extends Vue {
  @Inject('shopApprovalRuleService') private shopApprovalRuleService: () => ShopApprovalRuleService;
  @Inject('alertService') private alertService: () => AlertService;

  public shopApprovalRule: IShopApprovalRule = new ShopApprovalRule();

  @Inject('shopService') private shopService: () => ShopService;

  public shops: IShop[] = [];
  public approvalRuleValues: string[] = Object.keys(ApprovalRule);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.shopApprovalRuleId) {
        vm.retrieveShopApprovalRule(to.params.shopApprovalRuleId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.shopApprovalRule.id) {
      this.shopApprovalRuleService()
        .update(this.shopApprovalRule)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('slm2App.shopApprovalRule.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.shopApprovalRuleService()
        .create(this.shopApprovalRule)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('slm2App.shopApprovalRule.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.shopApprovalRule[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.shopApprovalRule[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.shopApprovalRule[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.shopApprovalRule[field] = null;
    }
  }

  public retrieveShopApprovalRule(shopApprovalRuleId): void {
    this.shopApprovalRuleService()
      .find(shopApprovalRuleId)
      .then(res => {
        res.createdDate = new Date(res.createdDate);
        res.lastModifiedDate = new Date(res.lastModifiedDate);
        this.shopApprovalRule = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.shopService()
      .retrieve()
      .then(res => {
        this.shops = res.data;
      });
  }
}
