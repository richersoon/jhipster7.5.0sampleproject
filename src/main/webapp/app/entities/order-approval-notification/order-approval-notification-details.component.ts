import { Component, Vue, Inject } from 'vue-property-decorator';

import { IOrderApprovalNotification } from '@/shared/model/order-approval-notification.model';
import OrderApprovalNotificationService from './order-approval-notification.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class OrderApprovalNotificationDetails extends Vue {
  @Inject('orderApprovalNotificationService') private orderApprovalNotificationService: () => OrderApprovalNotificationService;
  @Inject('alertService') private alertService: () => AlertService;

  public orderApprovalNotification: IOrderApprovalNotification = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.orderApprovalNotificationId) {
        vm.retrieveOrderApprovalNotification(to.params.orderApprovalNotificationId);
      }
    });
  }

  public retrieveOrderApprovalNotification(orderApprovalNotificationId) {
    this.orderApprovalNotificationService()
      .find(orderApprovalNotificationId)
      .then(res => {
        this.orderApprovalNotification = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
