import { Component, Vue, Inject } from 'vue-property-decorator';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import OrderService from '@/entities/order/order.service';
import { IOrder } from '@/shared/model/order.model';

import { IOrderApprovalNotification, OrderApprovalNotification } from '@/shared/model/order-approval-notification.model';
import OrderApprovalNotificationService from './order-approval-notification.service';
import { OrderApprovalStatus } from '@/shared/model/enumerations/order-approval-status.model';

const validations: any = {
  orderApprovalNotification: {
    status: {},
    browserIpCity: {},
    browserIpProvince: {},
    browserIpCountry: {},
    browserIpLatitude: {},
    browserIpLongitude: {},
    browserIpZip: {},
    distanceKm: {},
    durationSeconds: {},
    note: {},
    createdBy: {},
    createdDate: {},
    lastModifiedBy: {},
    lastModifiedDate: {},
    version: {},
  },
};

@Component({
  validations,
})
export default class OrderApprovalNotificationUpdate extends Vue {
  @Inject('orderApprovalNotificationService') private orderApprovalNotificationService: () => OrderApprovalNotificationService;
  @Inject('alertService') private alertService: () => AlertService;

  public orderApprovalNotification: IOrderApprovalNotification = new OrderApprovalNotification();

  @Inject('orderService') private orderService: () => OrderService;

  public orders: IOrder[] = [];
  public orderApprovalStatusValues: string[] = Object.keys(OrderApprovalStatus);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.orderApprovalNotificationId) {
        vm.retrieveOrderApprovalNotification(to.params.orderApprovalNotificationId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.orderApprovalNotification.id) {
      this.orderApprovalNotificationService()
        .update(this.orderApprovalNotification)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('slm2App.orderApprovalNotification.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.orderApprovalNotificationService()
        .create(this.orderApprovalNotification)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('slm2App.orderApprovalNotification.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.orderApprovalNotification[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.orderApprovalNotification[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.orderApprovalNotification[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.orderApprovalNotification[field] = null;
    }
  }

  public retrieveOrderApprovalNotification(orderApprovalNotificationId): void {
    this.orderApprovalNotificationService()
      .find(orderApprovalNotificationId)
      .then(res => {
        res.createdDate = new Date(res.createdDate);
        res.lastModifiedDate = new Date(res.lastModifiedDate);
        this.orderApprovalNotification = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.orderService()
      .retrieve()
      .then(res => {
        this.orders = res.data;
      });
  }
}
