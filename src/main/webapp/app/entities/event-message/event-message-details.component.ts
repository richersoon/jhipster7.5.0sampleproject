import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { IEventMessage } from '@/shared/model/event-message.model';
import EventMessageService from './event-message.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class EventMessageDetails extends mixins(JhiDataUtils) {
  @Inject('eventMessageService') private eventMessageService: () => EventMessageService;
  @Inject('alertService') private alertService: () => AlertService;

  public eventMessage: IEventMessage = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.eventMessageId) {
        vm.retrieveEventMessage(to.params.eventMessageId);
      }
    });
  }

  public retrieveEventMessage(eventMessageId) {
    this.eventMessageService()
      .find(eventMessageId)
      .then(res => {
        this.eventMessage = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
