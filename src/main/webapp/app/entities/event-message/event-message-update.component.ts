import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { required } from 'vuelidate/lib/validators';
import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import OrderService from '@/entities/order/order.service';
import { IOrder } from '@/shared/model/order.model';

import { IEventMessage, EventMessage } from '@/shared/model/event-message.model';
import EventMessageService from './event-message.service';
import { EventMessageStatus } from '@/shared/model/enumerations/event-message-status.model';

const validations: any = {
  eventMessage: {
    payload: {
      required,
    },
    header: {
      required,
    },
    status: {
      required,
    },
    notes: {},
    createdBy: {},
    createdDate: {},
    lastModifiedBy: {},
    lastModifiedDate: {},
    version: {},
  },
};

@Component({
  validations,
})
export default class EventMessageUpdate extends mixins(JhiDataUtils) {
  @Inject('eventMessageService') private eventMessageService: () => EventMessageService;
  @Inject('alertService') private alertService: () => AlertService;

  public eventMessage: IEventMessage = new EventMessage();

  @Inject('orderService') private orderService: () => OrderService;

  public orders: IOrder[] = [];
  public eventMessageStatusValues: string[] = Object.keys(EventMessageStatus);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.eventMessageId) {
        vm.retrieveEventMessage(to.params.eventMessageId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.eventMessage.id) {
      this.eventMessageService()
        .update(this.eventMessage)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('slm2App.eventMessage.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.eventMessageService()
        .create(this.eventMessage)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('slm2App.eventMessage.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.eventMessage[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.eventMessage[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.eventMessage[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.eventMessage[field] = null;
    }
  }

  public retrieveEventMessage(eventMessageId): void {
    this.eventMessageService()
      .find(eventMessageId)
      .then(res => {
        res.createdDate = new Date(res.createdDate);
        res.lastModifiedDate = new Date(res.lastModifiedDate);
        this.eventMessage = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.orderService()
      .retrieve()
      .then(res => {
        this.orders = res.data;
      });
  }
}
