import { Component, Vue, Inject } from 'vue-property-decorator';

import { IUserCompany } from '@/shared/model/user-company.model';
import UserCompanyService from './user-company.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class UserCompanyDetails extends Vue {
  @Inject('userCompanyService') private userCompanyService: () => UserCompanyService;
  @Inject('alertService') private alertService: () => AlertService;

  public userCompany: IUserCompany = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.userCompanyId) {
        vm.retrieveUserCompany(to.params.userCompanyId);
      }
    });
  }

  public retrieveUserCompany(userCompanyId) {
    this.userCompanyService()
      .find(userCompanyId)
      .then(res => {
        this.userCompany = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
