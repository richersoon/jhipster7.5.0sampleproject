import { Component, Vue, Inject } from 'vue-property-decorator';

import { required } from 'vuelidate/lib/validators';
import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import UserShopService from '@/entities/user-shop/user-shop.service';
import { IUserShop } from '@/shared/model/user-shop.model';

import ProductTypeService from '@/entities/product-type/product-type.service';
import { IProductType } from '@/shared/model/product-type.model';

import OrderService from '@/entities/order/order.service';
import { IOrder } from '@/shared/model/order.model';

import ShopEmailTemplateService from '@/entities/shop-email-template/shop-email-template.service';
import { IShopEmailTemplate } from '@/shared/model/shop-email-template.model';

import ShopApprovalRuleService from '@/entities/shop-approval-rule/shop-approval-rule.service';
import { IShopApprovalRule } from '@/shared/model/shop-approval-rule.model';

import WebhookSubscriptionService from '@/entities/webhook-subscription/webhook-subscription.service';
import { IWebhookSubscription } from '@/shared/model/webhook-subscription.model';

import ShopSmtpService from '@/entities/shop-smtp/shop-smtp.service';
import { IShopSmtp } from '@/shared/model/shop-smtp.model';

import CompanyService from '@/entities/company/company.service';
import { ICompany } from '@/shared/model/company.model';

import FileService from '@/entities/file/file.service';
import { IFile } from '@/shared/model/file.model';

import { IShop, Shop } from '@/shared/model/shop.model';
import ShopService from './shop.service';
import { ExternalType } from '@/shared/model/enumerations/external-type.model';

const validations: any = {
  shop: {
    name: {
      required,
    },
    url: {
      required,
    },
    externalType: {},
    authorizationCode: {},
    generatedToken: {},
    accessToken: {},
    consumerKey: {},
    consumerSecret: {},
    createdBy: {},
    createdDate: {},
    lastModifiedBy: {},
    lastModifiedDate: {},
    version: {},
    company: {
      required,
    },
  },
};

@Component({
  validations,
})
export default class ShopUpdate extends Vue {
  @Inject('shopService') private shopService: () => ShopService;
  @Inject('alertService') private alertService: () => AlertService;

  public shop: IShop = new Shop();

  @Inject('userShopService') private userShopService: () => UserShopService;

  public userShops: IUserShop[] = [];

  @Inject('productTypeService') private productTypeService: () => ProductTypeService;

  public productTypes: IProductType[] = [];

  @Inject('orderService') private orderService: () => OrderService;

  public orders: IOrder[] = [];

  @Inject('shopEmailTemplateService') private shopEmailTemplateService: () => ShopEmailTemplateService;

  public shopEmailTemplates: IShopEmailTemplate[] = [];

  @Inject('shopApprovalRuleService') private shopApprovalRuleService: () => ShopApprovalRuleService;

  public shopApprovalRules: IShopApprovalRule[] = [];

  @Inject('webhookSubscriptionService') private webhookSubscriptionService: () => WebhookSubscriptionService;

  public webhookSubscriptions: IWebhookSubscription[] = [];

  @Inject('shopSmtpService') private shopSmtpService: () => ShopSmtpService;

  public shopSmtps: IShopSmtp[] = [];

  @Inject('companyService') private companyService: () => CompanyService;

  public companies: ICompany[] = [];

  @Inject('fileService') private fileService: () => FileService;

  public files: IFile[] = [];
  public externalTypeValues: string[] = Object.keys(ExternalType);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.shopId) {
        vm.retrieveShop(to.params.shopId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.shop.id) {
      this.shopService()
        .update(this.shop)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('slm2App.shop.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.shopService()
        .create(this.shop)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('slm2App.shop.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.shop[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.shop[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.shop[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.shop[field] = null;
    }
  }

  public retrieveShop(shopId): void {
    this.shopService()
      .find(shopId)
      .then(res => {
        res.createdDate = new Date(res.createdDate);
        res.lastModifiedDate = new Date(res.lastModifiedDate);
        this.shop = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.userShopService()
      .retrieve()
      .then(res => {
        this.userShops = res.data;
      });
    this.productTypeService()
      .retrieve()
      .then(res => {
        this.productTypes = res.data;
      });
    this.orderService()
      .retrieve()
      .then(res => {
        this.orders = res.data;
      });
    this.shopEmailTemplateService()
      .retrieve()
      .then(res => {
        this.shopEmailTemplates = res.data;
      });
    this.shopApprovalRuleService()
      .retrieve()
      .then(res => {
        this.shopApprovalRules = res.data;
      });
    this.webhookSubscriptionService()
      .retrieve()
      .then(res => {
        this.webhookSubscriptions = res.data;
      });
    this.shopSmtpService()
      .retrieve()
      .then(res => {
        this.shopSmtps = res.data;
      });
    this.companyService()
      .retrieve()
      .then(res => {
        this.companies = res.data;
      });
    this.fileService()
      .retrieve()
      .then(res => {
        this.files = res.data;
      });
  }
}
