import { Component, Vue, Inject } from 'vue-property-decorator';

import { IUserShop } from '@/shared/model/user-shop.model';
import UserShopService from './user-shop.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class UserShopDetails extends Vue {
  @Inject('userShopService') private userShopService: () => UserShopService;
  @Inject('alertService') private alertService: () => AlertService;

  public userShop: IUserShop = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.userShopId) {
        vm.retrieveUserShop(to.params.userShopId);
      }
    });
  }

  public retrieveUserShop(userShopId) {
    this.userShopService()
      .find(userShopId)
      .then(res => {
        this.userShop = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
