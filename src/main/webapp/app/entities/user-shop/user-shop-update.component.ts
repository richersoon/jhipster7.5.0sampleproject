import { Component, Vue, Inject } from 'vue-property-decorator';

import { required } from 'vuelidate/lib/validators';
import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import UserService from '@/entities/user/user.service';

import ShopService from '@/entities/shop/shop.service';
import { IShop } from '@/shared/model/shop.model';

import { IUserShop, UserShop } from '@/shared/model/user-shop.model';
import UserShopService from './user-shop.service';
import { Role } from '@/shared/model/enumerations/role.model';

const validations: any = {
  userShop: {
    role: {
      required,
    },
    createdBy: {},
    createdDate: {},
    lastModifiedBy: {},
    lastModifiedDate: {},
    version: {},
    user: {
      required,
    },
    shop: {
      required,
    },
  },
};

@Component({
  validations,
})
export default class UserShopUpdate extends Vue {
  @Inject('userShopService') private userShopService: () => UserShopService;
  @Inject('alertService') private alertService: () => AlertService;

  public userShop: IUserShop = new UserShop();

  @Inject('userService') private userService: () => UserService;

  public users: Array<any> = [];

  @Inject('shopService') private shopService: () => ShopService;

  public shops: IShop[] = [];
  public roleValues: string[] = Object.keys(Role);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.userShopId) {
        vm.retrieveUserShop(to.params.userShopId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.userShop.id) {
      this.userShopService()
        .update(this.userShop)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('slm2App.userShop.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.userShopService()
        .create(this.userShop)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('slm2App.userShop.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.userShop[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.userShop[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.userShop[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.userShop[field] = null;
    }
  }

  public retrieveUserShop(userShopId): void {
    this.userShopService()
      .find(userShopId)
      .then(res => {
        res.createdDate = new Date(res.createdDate);
        res.lastModifiedDate = new Date(res.lastModifiedDate);
        this.userShop = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.userService()
      .retrieve()
      .then(res => {
        this.users = res.data;
      });
    this.shopService()
      .retrieve()
      .then(res => {
        this.shops = res.data;
      });
  }
}
