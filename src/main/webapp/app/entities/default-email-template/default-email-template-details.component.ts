import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { IDefaultEmailTemplate } from '@/shared/model/default-email-template.model';
import DefaultEmailTemplateService from './default-email-template.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class DefaultEmailTemplateDetails extends mixins(JhiDataUtils) {
  @Inject('defaultEmailTemplateService') private defaultEmailTemplateService: () => DefaultEmailTemplateService;
  @Inject('alertService') private alertService: () => AlertService;

  public defaultEmailTemplate: IDefaultEmailTemplate = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.defaultEmailTemplateId) {
        vm.retrieveDefaultEmailTemplate(to.params.defaultEmailTemplateId);
      }
    });
  }

  public retrieveDefaultEmailTemplate(defaultEmailTemplateId) {
    this.defaultEmailTemplateService()
      .find(defaultEmailTemplateId)
      .then(res => {
        this.defaultEmailTemplate = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
