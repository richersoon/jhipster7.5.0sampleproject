import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { required } from 'vuelidate/lib/validators';
import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import ShopEmailTemplateService from '@/entities/shop-email-template/shop-email-template.service';
import { IShopEmailTemplate } from '@/shared/model/shop-email-template.model';

import { IDefaultEmailTemplate, DefaultEmailTemplate } from '@/shared/model/default-email-template.model';
import DefaultEmailTemplateService from './default-email-template.service';
import { Language } from '@/shared/model/enumerations/language.model';

const validations: any = {
  defaultEmailTemplate: {
    subject: {
      required,
    },
    header: {
      required,
    },
    body: {
      required,
    },
    footer: {
      required,
    },
    language: {
      required,
    },
    createdBy: {},
    createdDate: {},
    lastModifiedBy: {},
    lastModifiedDate: {},
    version: {},
  },
};

@Component({
  validations,
})
export default class DefaultEmailTemplateUpdate extends mixins(JhiDataUtils) {
  @Inject('defaultEmailTemplateService') private defaultEmailTemplateService: () => DefaultEmailTemplateService;
  @Inject('alertService') private alertService: () => AlertService;

  public defaultEmailTemplate: IDefaultEmailTemplate = new DefaultEmailTemplate();

  @Inject('shopEmailTemplateService') private shopEmailTemplateService: () => ShopEmailTemplateService;

  public shopEmailTemplates: IShopEmailTemplate[] = [];
  public languageValues: string[] = Object.keys(Language);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.defaultEmailTemplateId) {
        vm.retrieveDefaultEmailTemplate(to.params.defaultEmailTemplateId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.defaultEmailTemplate.id) {
      this.defaultEmailTemplateService()
        .update(this.defaultEmailTemplate)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('slm2App.defaultEmailTemplate.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.defaultEmailTemplateService()
        .create(this.defaultEmailTemplate)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('slm2App.defaultEmailTemplate.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.defaultEmailTemplate[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.defaultEmailTemplate[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.defaultEmailTemplate[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.defaultEmailTemplate[field] = null;
    }
  }

  public retrieveDefaultEmailTemplate(defaultEmailTemplateId): void {
    this.defaultEmailTemplateService()
      .find(defaultEmailTemplateId)
      .then(res => {
        res.createdDate = new Date(res.createdDate);
        res.lastModifiedDate = new Date(res.lastModifiedDate);
        this.defaultEmailTemplate = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.shopEmailTemplateService()
      .retrieve()
      .then(res => {
        this.shopEmailTemplates = res.data;
      });
  }
}
