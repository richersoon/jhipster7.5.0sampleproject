import { Component, Vue, Inject } from 'vue-property-decorator';

import { required } from 'vuelidate/lib/validators';
import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import ProductTypeService from '@/entities/product-type/product-type.service';
import { IProductType } from '@/shared/model/product-type.model';

import OrderService from '@/entities/order/order.service';
import { IOrder } from '@/shared/model/order.model';

import ProductService from '@/entities/product/product.service';
import { IProduct } from '@/shared/model/product.model';

import { IOrderItem, OrderItem } from '@/shared/model/order-item.model';
import OrderItemService from './order-item.service';
import { OrderItemStatus } from '@/shared/model/enumerations/order-item-status.model';

const validations: any = {
  orderItem: {
    notes: {},
    status: {
      required,
    },
    productTypeIdentifier: {},
    createdBy: {},
    createdDate: {},
    lastModifiedBy: {},
    lastModifiedDate: {},
    version: {},
    order: {
      required,
    },
  },
};

@Component({
  validations,
})
export default class OrderItemUpdate extends Vue {
  @Inject('orderItemService') private orderItemService: () => OrderItemService;
  @Inject('alertService') private alertService: () => AlertService;

  public orderItem: IOrderItem = new OrderItem();

  @Inject('productTypeService') private productTypeService: () => ProductTypeService;

  public productTypes: IProductType[] = [];

  @Inject('orderService') private orderService: () => OrderService;

  public orders: IOrder[] = [];

  @Inject('productService') private productService: () => ProductService;

  public products: IProduct[] = [];
  public orderItemStatusValues: string[] = Object.keys(OrderItemStatus);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.orderItemId) {
        vm.retrieveOrderItem(to.params.orderItemId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.orderItem.id) {
      this.orderItemService()
        .update(this.orderItem)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('slm2App.orderItem.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.orderItemService()
        .create(this.orderItem)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('slm2App.orderItem.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.orderItem[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.orderItem[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.orderItem[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.orderItem[field] = null;
    }
  }

  public retrieveOrderItem(orderItemId): void {
    this.orderItemService()
      .find(orderItemId)
      .then(res => {
        res.createdDate = new Date(res.createdDate);
        res.lastModifiedDate = new Date(res.lastModifiedDate);
        this.orderItem = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.productTypeService()
      .retrieve()
      .then(res => {
        this.productTypes = res.data;
      });
    this.orderService()
      .retrieve()
      .then(res => {
        this.orders = res.data;
      });
    this.productService()
      .retrieve()
      .then(res => {
        this.products = res.data;
      });
  }
}
