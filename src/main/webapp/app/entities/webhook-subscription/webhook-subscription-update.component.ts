import { Component, Vue, Inject } from 'vue-property-decorator';

import { required } from 'vuelidate/lib/validators';
import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import ShopService from '@/entities/shop/shop.service';
import { IShop } from '@/shared/model/shop.model';

import { IWebhookSubscription, WebhookSubscription } from '@/shared/model/webhook-subscription.model';
import WebhookSubscriptionService from './webhook-subscription.service';
import { WebhookType } from '@/shared/model/enumerations/webhook-type.model';

const validations: any = {
  webhookSubscription: {
    externalId: {
      required,
    },
    type: {
      required,
    },
    createdBy: {},
    createdDate: {},
    lastModifiedBy: {},
    lastModifiedDate: {},
    version: {},
    shop: {
      required,
    },
  },
};

@Component({
  validations,
})
export default class WebhookSubscriptionUpdate extends Vue {
  @Inject('webhookSubscriptionService') private webhookSubscriptionService: () => WebhookSubscriptionService;
  @Inject('alertService') private alertService: () => AlertService;

  public webhookSubscription: IWebhookSubscription = new WebhookSubscription();

  @Inject('shopService') private shopService: () => ShopService;

  public shops: IShop[] = [];
  public webhookTypeValues: string[] = Object.keys(WebhookType);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.webhookSubscriptionId) {
        vm.retrieveWebhookSubscription(to.params.webhookSubscriptionId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.webhookSubscription.id) {
      this.webhookSubscriptionService()
        .update(this.webhookSubscription)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('slm2App.webhookSubscription.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.webhookSubscriptionService()
        .create(this.webhookSubscription)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('slm2App.webhookSubscription.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.webhookSubscription[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.webhookSubscription[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.webhookSubscription[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.webhookSubscription[field] = null;
    }
  }

  public retrieveWebhookSubscription(webhookSubscriptionId): void {
    this.webhookSubscriptionService()
      .find(webhookSubscriptionId)
      .then(res => {
        res.createdDate = new Date(res.createdDate);
        res.lastModifiedDate = new Date(res.lastModifiedDate);
        this.webhookSubscription = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.shopService()
      .retrieve()
      .then(res => {
        this.shops = res.data;
      });
  }
}
