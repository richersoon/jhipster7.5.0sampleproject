import { Component, Vue, Inject } from 'vue-property-decorator';

import { IWebhookSubscription } from '@/shared/model/webhook-subscription.model';
import WebhookSubscriptionService from './webhook-subscription.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class WebhookSubscriptionDetails extends Vue {
  @Inject('webhookSubscriptionService') private webhookSubscriptionService: () => WebhookSubscriptionService;
  @Inject('alertService') private alertService: () => AlertService;

  public webhookSubscription: IWebhookSubscription = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.webhookSubscriptionId) {
        vm.retrieveWebhookSubscription(to.params.webhookSubscriptionId);
      }
    });
  }

  public retrieveWebhookSubscription(webhookSubscriptionId) {
    this.webhookSubscriptionService()
      .find(webhookSubscriptionId)
      .then(res => {
        this.webhookSubscription = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
