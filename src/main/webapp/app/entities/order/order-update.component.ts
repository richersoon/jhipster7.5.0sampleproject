import { Component, Vue, Inject } from 'vue-property-decorator';

import { required } from 'vuelidate/lib/validators';
import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import OrderItemService from '@/entities/order-item/order-item.service';
import { IOrderItem } from '@/shared/model/order-item.model';

import OrderNoteService from '@/entities/order-note/order-note.service';
import { IOrderNote } from '@/shared/model/order-note.model';

import EventMessageService from '@/entities/event-message/event-message.service';
import { IEventMessage } from '@/shared/model/event-message.model';

import OrderApprovalNotificationService from '@/entities/order-approval-notification/order-approval-notification.service';
import { IOrderApprovalNotification } from '@/shared/model/order-approval-notification.model';

import ShopService from '@/entities/shop/shop.service';
import { IShop } from '@/shared/model/shop.model';

import { IOrder, Order } from '@/shared/model/order.model';
import OrderService from './order.service';
import { ExternalType } from '@/shared/model/enumerations/external-type.model';
import { OrderStatus } from '@/shared/model/enumerations/order-status.model';

const validations: any = {
  order: {
    orderNo: {},
    externalType: {},
    firstname: {},
    lastname: {},
    email: {},
    language: {},
    totalPrice: {},
    paymentGateway: {},
    status: {},
    ip: {},
    street: {},
    city: {},
    province: {},
    country: {},
    zip: {},
    latitude: {},
    longitude: {},
    createdBy: {},
    createdDate: {},
    lastModifiedBy: {},
    lastModifiedDate: {},
    version: {},
    shop: {
      required,
    },
  },
};

@Component({
  validations,
})
export default class OrderUpdate extends Vue {
  @Inject('orderService') private orderService: () => OrderService;
  @Inject('alertService') private alertService: () => AlertService;

  public order: IOrder = new Order();

  @Inject('orderItemService') private orderItemService: () => OrderItemService;

  public orderItems: IOrderItem[] = [];

  @Inject('orderNoteService') private orderNoteService: () => OrderNoteService;

  public orderNotes: IOrderNote[] = [];

  @Inject('eventMessageService') private eventMessageService: () => EventMessageService;

  public eventMessages: IEventMessage[] = [];

  @Inject('orderApprovalNotificationService') private orderApprovalNotificationService: () => OrderApprovalNotificationService;

  public orderApprovalNotifications: IOrderApprovalNotification[] = [];

  @Inject('shopService') private shopService: () => ShopService;

  public shops: IShop[] = [];
  public externalTypeValues: string[] = Object.keys(ExternalType);
  public orderStatusValues: string[] = Object.keys(OrderStatus);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.orderId) {
        vm.retrieveOrder(to.params.orderId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.order.id) {
      this.orderService()
        .update(this.order)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('slm2App.order.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.orderService()
        .create(this.order)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('slm2App.order.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.order[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.order[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.order[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.order[field] = null;
    }
  }

  public retrieveOrder(orderId): void {
    this.orderService()
      .find(orderId)
      .then(res => {
        res.createdDate = new Date(res.createdDate);
        res.lastModifiedDate = new Date(res.lastModifiedDate);
        this.order = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.orderItemService()
      .retrieve()
      .then(res => {
        this.orderItems = res.data;
      });
    this.orderNoteService()
      .retrieve()
      .then(res => {
        this.orderNotes = res.data;
      });
    this.eventMessageService()
      .retrieve()
      .then(res => {
        this.eventMessages = res.data;
      });
    this.orderApprovalNotificationService()
      .retrieve()
      .then(res => {
        this.orderApprovalNotifications = res.data;
      });
    this.shopService()
      .retrieve()
      .then(res => {
        this.shops = res.data;
      });
  }
}
