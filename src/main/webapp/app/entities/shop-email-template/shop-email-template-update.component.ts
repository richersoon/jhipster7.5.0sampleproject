import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { required } from 'vuelidate/lib/validators';
import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import DefaultEmailTemplateService from '@/entities/default-email-template/default-email-template.service';
import { IDefaultEmailTemplate } from '@/shared/model/default-email-template.model';

import ShopService from '@/entities/shop/shop.service';
import { IShop } from '@/shared/model/shop.model';

import { IShopEmailTemplate, ShopEmailTemplate } from '@/shared/model/shop-email-template.model';
import ShopEmailTemplateService from './shop-email-template.service';

const validations: any = {
  shopEmailTemplate: {
    subject: {
      required,
    },
    header: {
      required,
    },
    body: {
      required,
    },
    footer: {
      required,
    },
    replyTo: {
      required,
    },
    createdBy: {},
    createdDate: {},
    lastModifiedBy: {},
    lastModifiedDate: {},
    version: {},
    defaultEmailTemplate: {
      required,
    },
    shop: {
      required,
    },
  },
};

@Component({
  validations,
})
export default class ShopEmailTemplateUpdate extends mixins(JhiDataUtils) {
  @Inject('shopEmailTemplateService') private shopEmailTemplateService: () => ShopEmailTemplateService;
  @Inject('alertService') private alertService: () => AlertService;

  public shopEmailTemplate: IShopEmailTemplate = new ShopEmailTemplate();

  @Inject('defaultEmailTemplateService') private defaultEmailTemplateService: () => DefaultEmailTemplateService;

  public defaultEmailTemplates: IDefaultEmailTemplate[] = [];

  @Inject('shopService') private shopService: () => ShopService;

  public shops: IShop[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.shopEmailTemplateId) {
        vm.retrieveShopEmailTemplate(to.params.shopEmailTemplateId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.shopEmailTemplate.id) {
      this.shopEmailTemplateService()
        .update(this.shopEmailTemplate)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('slm2App.shopEmailTemplate.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.shopEmailTemplateService()
        .create(this.shopEmailTemplate)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('slm2App.shopEmailTemplate.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.shopEmailTemplate[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.shopEmailTemplate[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.shopEmailTemplate[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.shopEmailTemplate[field] = null;
    }
  }

  public retrieveShopEmailTemplate(shopEmailTemplateId): void {
    this.shopEmailTemplateService()
      .find(shopEmailTemplateId)
      .then(res => {
        res.createdDate = new Date(res.createdDate);
        res.lastModifiedDate = new Date(res.lastModifiedDate);
        this.shopEmailTemplate = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.defaultEmailTemplateService()
      .retrieve()
      .then(res => {
        this.defaultEmailTemplates = res.data;
      });
    this.shopService()
      .retrieve()
      .then(res => {
        this.shops = res.data;
      });
  }
}
