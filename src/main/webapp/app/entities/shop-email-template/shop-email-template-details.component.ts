import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { IShopEmailTemplate } from '@/shared/model/shop-email-template.model';
import ShopEmailTemplateService from './shop-email-template.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class ShopEmailTemplateDetails extends mixins(JhiDataUtils) {
  @Inject('shopEmailTemplateService') private shopEmailTemplateService: () => ShopEmailTemplateService;
  @Inject('alertService') private alertService: () => AlertService;

  public shopEmailTemplate: IShopEmailTemplate = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.shopEmailTemplateId) {
        vm.retrieveShopEmailTemplate(to.params.shopEmailTemplateId);
      }
    });
  }

  public retrieveShopEmailTemplate(shopEmailTemplateId) {
    this.shopEmailTemplateService()
      .find(shopEmailTemplateId)
      .then(res => {
        this.shopEmailTemplate = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
