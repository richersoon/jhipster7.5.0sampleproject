import { Component, Vue, Inject } from 'vue-property-decorator';

import { IProductType } from '@/shared/model/product-type.model';
import ProductTypeService from './product-type.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class ProductTypeDetails extends Vue {
  @Inject('productTypeService') private productTypeService: () => ProductTypeService;
  @Inject('alertService') private alertService: () => AlertService;

  public productType: IProductType = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.productTypeId) {
        vm.retrieveProductType(to.params.productTypeId);
      }
    });
  }

  public retrieveProductType(productTypeId) {
    this.productTypeService()
      .find(productTypeId)
      .then(res => {
        this.productType = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
