import { Component, Vue, Inject } from 'vue-property-decorator';

import { IShopSmtp } from '@/shared/model/shop-smtp.model';
import ShopSmtpService from './shop-smtp.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class ShopSmtpDetails extends Vue {
  @Inject('shopSmtpService') private shopSmtpService: () => ShopSmtpService;
  @Inject('alertService') private alertService: () => AlertService;

  public shopSmtp: IShopSmtp = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.shopSmtpId) {
        vm.retrieveShopSmtp(to.params.shopSmtpId);
      }
    });
  }

  public retrieveShopSmtp(shopSmtpId) {
    this.shopSmtpService()
      .find(shopSmtpId)
      .then(res => {
        this.shopSmtp = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
