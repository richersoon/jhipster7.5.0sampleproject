import { Component, Vue, Inject } from 'vue-property-decorator';

import { required, numeric } from 'vuelidate/lib/validators';
import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import ShopService from '@/entities/shop/shop.service';
import { IShop } from '@/shared/model/shop.model';

import { IShopSmtp, ShopSmtp } from '@/shared/model/shop-smtp.model';
import ShopSmtpService from './shop-smtp.service';

const validations: any = {
  shopSmtp: {
    host: {
      required,
    },
    port: {
      required,
      numeric,
    },
    fromEmail: {
      required,
    },
    fromName: {
      required,
    },
    replyTo: {
      required,
    },
    username: {
      required,
    },
    password: {
      required,
    },
    createdBy: {},
    createdDate: {},
    lastModifiedBy: {},
    lastModifiedDate: {},
    version: {},
    shop: {
      required,
    },
  },
};

@Component({
  validations,
})
export default class ShopSmtpUpdate extends Vue {
  @Inject('shopSmtpService') private shopSmtpService: () => ShopSmtpService;
  @Inject('alertService') private alertService: () => AlertService;

  public shopSmtp: IShopSmtp = new ShopSmtp();

  @Inject('shopService') private shopService: () => ShopService;

  public shops: IShop[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.shopSmtpId) {
        vm.retrieveShopSmtp(to.params.shopSmtpId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.shopSmtp.id) {
      this.shopSmtpService()
        .update(this.shopSmtp)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('slm2App.shopSmtp.updated', { param: param.id });
          return this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.shopSmtpService()
        .create(this.shopSmtp)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('slm2App.shopSmtp.created', { param: param.id });
          this.$root.$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.shopSmtp[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.shopSmtp[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.shopSmtp[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.shopSmtp[field] = null;
    }
  }

  public retrieveShopSmtp(shopSmtpId): void {
    this.shopSmtpService()
      .find(shopSmtpId)
      .then(res => {
        res.createdDate = new Date(res.createdDate);
        res.lastModifiedDate = new Date(res.lastModifiedDate);
        this.shopSmtp = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.shopService()
      .retrieve()
      .then(res => {
        this.shops = res.data;
      });
  }
}
