import { Component, Vue, Inject } from 'vue-property-decorator';

import { IOrderNote } from '@/shared/model/order-note.model';
import OrderNoteService from './order-note.service';
import AlertService from '@/shared/alert/alert.service';

@Component
export default class OrderNoteDetails extends Vue {
  @Inject('orderNoteService') private orderNoteService: () => OrderNoteService;
  @Inject('alertService') private alertService: () => AlertService;

  public orderNote: IOrderNote = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.orderNoteId) {
        vm.retrieveOrderNote(to.params.orderNoteId);
      }
    });
  }

  public retrieveOrderNote(orderNoteId) {
    this.orderNoteService()
      .find(orderNoteId)
      .then(res => {
        this.orderNote = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
