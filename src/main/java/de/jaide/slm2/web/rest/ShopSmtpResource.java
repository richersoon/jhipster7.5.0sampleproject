package de.jaide.slm2.web.rest;

import de.jaide.slm2.repository.ShopSmtpRepository;
import de.jaide.slm2.service.ShopSmtpService;
import de.jaide.slm2.service.dto.ShopSmtpDTO;
import de.jaide.slm2.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link de.jaide.slm2.domain.ShopSmtp}.
 */
@RestController
@RequestMapping("/api")
public class ShopSmtpResource {

    private final Logger log = LoggerFactory.getLogger(ShopSmtpResource.class);

    private static final String ENTITY_NAME = "shopSmtp";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShopSmtpService shopSmtpService;

    private final ShopSmtpRepository shopSmtpRepository;

    public ShopSmtpResource(ShopSmtpService shopSmtpService, ShopSmtpRepository shopSmtpRepository) {
        this.shopSmtpService = shopSmtpService;
        this.shopSmtpRepository = shopSmtpRepository;
    }

    /**
     * {@code POST  /shop-smtps} : Create a new shopSmtp.
     *
     * @param shopSmtpDTO the shopSmtpDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new shopSmtpDTO, or with status {@code 400 (Bad Request)} if the shopSmtp has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/shop-smtps")
    public ResponseEntity<ShopSmtpDTO> createShopSmtp(@Valid @RequestBody ShopSmtpDTO shopSmtpDTO) throws URISyntaxException {
        log.debug("REST request to save ShopSmtp : {}", shopSmtpDTO);
        if (shopSmtpDTO.getId() != null) {
            throw new BadRequestAlertException("A new shopSmtp cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShopSmtpDTO result = shopSmtpService.save(shopSmtpDTO);
        return ResponseEntity
            .created(new URI("/api/shop-smtps/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /shop-smtps/:id} : Updates an existing shopSmtp.
     *
     * @param id the id of the shopSmtpDTO to save.
     * @param shopSmtpDTO the shopSmtpDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shopSmtpDTO,
     * or with status {@code 400 (Bad Request)} if the shopSmtpDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the shopSmtpDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/shop-smtps/{id}")
    public ResponseEntity<ShopSmtpDTO> updateShopSmtp(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody ShopSmtpDTO shopSmtpDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ShopSmtp : {}, {}", id, shopSmtpDTO);
        if (shopSmtpDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, shopSmtpDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!shopSmtpRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ShopSmtpDTO result = shopSmtpService.save(shopSmtpDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, shopSmtpDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /shop-smtps/:id} : Partial updates given fields of an existing shopSmtp, field will ignore if it is null
     *
     * @param id the id of the shopSmtpDTO to save.
     * @param shopSmtpDTO the shopSmtpDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shopSmtpDTO,
     * or with status {@code 400 (Bad Request)} if the shopSmtpDTO is not valid,
     * or with status {@code 404 (Not Found)} if the shopSmtpDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the shopSmtpDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/shop-smtps/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ShopSmtpDTO> partialUpdateShopSmtp(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody ShopSmtpDTO shopSmtpDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ShopSmtp partially : {}, {}", id, shopSmtpDTO);
        if (shopSmtpDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, shopSmtpDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!shopSmtpRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ShopSmtpDTO> result = shopSmtpService.partialUpdate(shopSmtpDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, shopSmtpDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /shop-smtps} : get all the shopSmtps.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shopSmtps in body.
     */
    @GetMapping("/shop-smtps")
    public ResponseEntity<List<ShopSmtpDTO>> getAllShopSmtps(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of ShopSmtps");
        Page<ShopSmtpDTO> page = shopSmtpService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /shop-smtps/:id} : get the "id" shopSmtp.
     *
     * @param id the id of the shopSmtpDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the shopSmtpDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/shop-smtps/{id}")
    public ResponseEntity<ShopSmtpDTO> getShopSmtp(@PathVariable Long id) {
        log.debug("REST request to get ShopSmtp : {}", id);
        Optional<ShopSmtpDTO> shopSmtpDTO = shopSmtpService.findOne(id);
        return ResponseUtil.wrapOrNotFound(shopSmtpDTO);
    }

    /**
     * {@code DELETE  /shop-smtps/:id} : delete the "id" shopSmtp.
     *
     * @param id the id of the shopSmtpDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/shop-smtps/{id}")
    public ResponseEntity<Void> deleteShopSmtp(@PathVariable Long id) {
        log.debug("REST request to delete ShopSmtp : {}", id);
        shopSmtpService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
