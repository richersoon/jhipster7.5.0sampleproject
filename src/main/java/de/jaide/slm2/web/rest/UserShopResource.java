package de.jaide.slm2.web.rest;

import de.jaide.slm2.repository.UserShopRepository;
import de.jaide.slm2.service.UserShopService;
import de.jaide.slm2.service.dto.UserShopDTO;
import de.jaide.slm2.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link de.jaide.slm2.domain.UserShop}.
 */
@RestController
@RequestMapping("/api")
public class UserShopResource {

    private final Logger log = LoggerFactory.getLogger(UserShopResource.class);

    private static final String ENTITY_NAME = "userShop";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserShopService userShopService;

    private final UserShopRepository userShopRepository;

    public UserShopResource(UserShopService userShopService, UserShopRepository userShopRepository) {
        this.userShopService = userShopService;
        this.userShopRepository = userShopRepository;
    }

    /**
     * {@code POST  /user-shops} : Create a new userShop.
     *
     * @param userShopDTO the userShopDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userShopDTO, or with status {@code 400 (Bad Request)} if the userShop has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-shops")
    public ResponseEntity<UserShopDTO> createUserShop(@Valid @RequestBody UserShopDTO userShopDTO) throws URISyntaxException {
        log.debug("REST request to save UserShop : {}", userShopDTO);
        if (userShopDTO.getId() != null) {
            throw new BadRequestAlertException("A new userShop cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserShopDTO result = userShopService.save(userShopDTO);
        return ResponseEntity
            .created(new URI("/api/user-shops/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-shops/:id} : Updates an existing userShop.
     *
     * @param id the id of the userShopDTO to save.
     * @param userShopDTO the userShopDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userShopDTO,
     * or with status {@code 400 (Bad Request)} if the userShopDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userShopDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-shops/{id}")
    public ResponseEntity<UserShopDTO> updateUserShop(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody UserShopDTO userShopDTO
    ) throws URISyntaxException {
        log.debug("REST request to update UserShop : {}, {}", id, userShopDTO);
        if (userShopDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, userShopDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!userShopRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        UserShopDTO result = userShopService.save(userShopDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, userShopDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /user-shops/:id} : Partial updates given fields of an existing userShop, field will ignore if it is null
     *
     * @param id the id of the userShopDTO to save.
     * @param userShopDTO the userShopDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userShopDTO,
     * or with status {@code 400 (Bad Request)} if the userShopDTO is not valid,
     * or with status {@code 404 (Not Found)} if the userShopDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the userShopDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/user-shops/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<UserShopDTO> partialUpdateUserShop(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody UserShopDTO userShopDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update UserShop partially : {}, {}", id, userShopDTO);
        if (userShopDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, userShopDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!userShopRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<UserShopDTO> result = userShopService.partialUpdate(userShopDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, userShopDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /user-shops} : get all the userShops.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userShops in body.
     */
    @GetMapping("/user-shops")
    public ResponseEntity<List<UserShopDTO>> getAllUserShops(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of UserShops");
        Page<UserShopDTO> page = userShopService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /user-shops/:id} : get the "id" userShop.
     *
     * @param id the id of the userShopDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userShopDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-shops/{id}")
    public ResponseEntity<UserShopDTO> getUserShop(@PathVariable Long id) {
        log.debug("REST request to get UserShop : {}", id);
        Optional<UserShopDTO> userShopDTO = userShopService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userShopDTO);
    }

    /**
     * {@code DELETE  /user-shops/:id} : delete the "id" userShop.
     *
     * @param id the id of the userShopDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-shops/{id}")
    public ResponseEntity<Void> deleteUserShop(@PathVariable Long id) {
        log.debug("REST request to delete UserShop : {}", id);
        userShopService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
