package de.jaide.slm2.web.rest;

import de.jaide.slm2.repository.OrderApprovalNotificationRepository;
import de.jaide.slm2.service.OrderApprovalNotificationService;
import de.jaide.slm2.service.dto.OrderApprovalNotificationDTO;
import de.jaide.slm2.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link de.jaide.slm2.domain.OrderApprovalNotification}.
 */
@RestController
@RequestMapping("/api")
public class OrderApprovalNotificationResource {

    private final Logger log = LoggerFactory.getLogger(OrderApprovalNotificationResource.class);

    private static final String ENTITY_NAME = "orderApprovalNotification";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrderApprovalNotificationService orderApprovalNotificationService;

    private final OrderApprovalNotificationRepository orderApprovalNotificationRepository;

    public OrderApprovalNotificationResource(
        OrderApprovalNotificationService orderApprovalNotificationService,
        OrderApprovalNotificationRepository orderApprovalNotificationRepository
    ) {
        this.orderApprovalNotificationService = orderApprovalNotificationService;
        this.orderApprovalNotificationRepository = orderApprovalNotificationRepository;
    }

    /**
     * {@code POST  /order-approval-notifications} : Create a new orderApprovalNotification.
     *
     * @param orderApprovalNotificationDTO the orderApprovalNotificationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new orderApprovalNotificationDTO, or with status {@code 400 (Bad Request)} if the orderApprovalNotification has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/order-approval-notifications")
    public ResponseEntity<OrderApprovalNotificationDTO> createOrderApprovalNotification(
        @RequestBody OrderApprovalNotificationDTO orderApprovalNotificationDTO
    ) throws URISyntaxException {
        log.debug("REST request to save OrderApprovalNotification : {}", orderApprovalNotificationDTO);
        if (orderApprovalNotificationDTO.getId() != null) {
            throw new BadRequestAlertException("A new orderApprovalNotification cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrderApprovalNotificationDTO result = orderApprovalNotificationService.save(orderApprovalNotificationDTO);
        return ResponseEntity
            .created(new URI("/api/order-approval-notifications/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /order-approval-notifications/:id} : Updates an existing orderApprovalNotification.
     *
     * @param id the id of the orderApprovalNotificationDTO to save.
     * @param orderApprovalNotificationDTO the orderApprovalNotificationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated orderApprovalNotificationDTO,
     * or with status {@code 400 (Bad Request)} if the orderApprovalNotificationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the orderApprovalNotificationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/order-approval-notifications/{id}")
    public ResponseEntity<OrderApprovalNotificationDTO> updateOrderApprovalNotification(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody OrderApprovalNotificationDTO orderApprovalNotificationDTO
    ) throws URISyntaxException {
        log.debug("REST request to update OrderApprovalNotification : {}, {}", id, orderApprovalNotificationDTO);
        if (orderApprovalNotificationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, orderApprovalNotificationDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!orderApprovalNotificationRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        OrderApprovalNotificationDTO result = orderApprovalNotificationService.save(orderApprovalNotificationDTO);
        return ResponseEntity
            .ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, orderApprovalNotificationDTO.getId().toString())
            )
            .body(result);
    }

    /**
     * {@code PATCH  /order-approval-notifications/:id} : Partial updates given fields of an existing orderApprovalNotification, field will ignore if it is null
     *
     * @param id the id of the orderApprovalNotificationDTO to save.
     * @param orderApprovalNotificationDTO the orderApprovalNotificationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated orderApprovalNotificationDTO,
     * or with status {@code 400 (Bad Request)} if the orderApprovalNotificationDTO is not valid,
     * or with status {@code 404 (Not Found)} if the orderApprovalNotificationDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the orderApprovalNotificationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/order-approval-notifications/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<OrderApprovalNotificationDTO> partialUpdateOrderApprovalNotification(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody OrderApprovalNotificationDTO orderApprovalNotificationDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update OrderApprovalNotification partially : {}, {}", id, orderApprovalNotificationDTO);
        if (orderApprovalNotificationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, orderApprovalNotificationDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!orderApprovalNotificationRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<OrderApprovalNotificationDTO> result = orderApprovalNotificationService.partialUpdate(orderApprovalNotificationDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, orderApprovalNotificationDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /order-approval-notifications} : get all the orderApprovalNotifications.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of orderApprovalNotifications in body.
     */
    @GetMapping("/order-approval-notifications")
    public ResponseEntity<List<OrderApprovalNotificationDTO>> getAllOrderApprovalNotifications(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get a page of OrderApprovalNotifications");
        Page<OrderApprovalNotificationDTO> page = orderApprovalNotificationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /order-approval-notifications/:id} : get the "id" orderApprovalNotification.
     *
     * @param id the id of the orderApprovalNotificationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the orderApprovalNotificationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/order-approval-notifications/{id}")
    public ResponseEntity<OrderApprovalNotificationDTO> getOrderApprovalNotification(@PathVariable Long id) {
        log.debug("REST request to get OrderApprovalNotification : {}", id);
        Optional<OrderApprovalNotificationDTO> orderApprovalNotificationDTO = orderApprovalNotificationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(orderApprovalNotificationDTO);
    }

    /**
     * {@code DELETE  /order-approval-notifications/:id} : delete the "id" orderApprovalNotification.
     *
     * @param id the id of the orderApprovalNotificationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/order-approval-notifications/{id}")
    public ResponseEntity<Void> deleteOrderApprovalNotification(@PathVariable Long id) {
        log.debug("REST request to delete OrderApprovalNotification : {}", id);
        orderApprovalNotificationService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
