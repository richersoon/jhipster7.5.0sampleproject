package de.jaide.slm2.web.rest;

import de.jaide.slm2.repository.UserCompanyRepository;
import de.jaide.slm2.service.UserCompanyService;
import de.jaide.slm2.service.dto.UserCompanyDTO;
import de.jaide.slm2.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link de.jaide.slm2.domain.UserCompany}.
 */
@RestController
@RequestMapping("/api")
public class UserCompanyResource {

    private final Logger log = LoggerFactory.getLogger(UserCompanyResource.class);

    private static final String ENTITY_NAME = "userCompany";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserCompanyService userCompanyService;

    private final UserCompanyRepository userCompanyRepository;

    public UserCompanyResource(UserCompanyService userCompanyService, UserCompanyRepository userCompanyRepository) {
        this.userCompanyService = userCompanyService;
        this.userCompanyRepository = userCompanyRepository;
    }

    /**
     * {@code POST  /user-companies} : Create a new userCompany.
     *
     * @param userCompanyDTO the userCompanyDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new userCompanyDTO, or with status {@code 400 (Bad Request)} if the userCompany has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/user-companies")
    public ResponseEntity<UserCompanyDTO> createUserCompany(@Valid @RequestBody UserCompanyDTO userCompanyDTO) throws URISyntaxException {
        log.debug("REST request to save UserCompany : {}", userCompanyDTO);
        if (userCompanyDTO.getId() != null) {
            throw new BadRequestAlertException("A new userCompany cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserCompanyDTO result = userCompanyService.save(userCompanyDTO);
        return ResponseEntity
            .created(new URI("/api/user-companies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /user-companies/:id} : Updates an existing userCompany.
     *
     * @param id the id of the userCompanyDTO to save.
     * @param userCompanyDTO the userCompanyDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userCompanyDTO,
     * or with status {@code 400 (Bad Request)} if the userCompanyDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the userCompanyDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/user-companies/{id}")
    public ResponseEntity<UserCompanyDTO> updateUserCompany(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody UserCompanyDTO userCompanyDTO
    ) throws URISyntaxException {
        log.debug("REST request to update UserCompany : {}, {}", id, userCompanyDTO);
        if (userCompanyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, userCompanyDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!userCompanyRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        UserCompanyDTO result = userCompanyService.save(userCompanyDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, userCompanyDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /user-companies/:id} : Partial updates given fields of an existing userCompany, field will ignore if it is null
     *
     * @param id the id of the userCompanyDTO to save.
     * @param userCompanyDTO the userCompanyDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated userCompanyDTO,
     * or with status {@code 400 (Bad Request)} if the userCompanyDTO is not valid,
     * or with status {@code 404 (Not Found)} if the userCompanyDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the userCompanyDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/user-companies/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<UserCompanyDTO> partialUpdateUserCompany(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody UserCompanyDTO userCompanyDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update UserCompany partially : {}, {}", id, userCompanyDTO);
        if (userCompanyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, userCompanyDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!userCompanyRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<UserCompanyDTO> result = userCompanyService.partialUpdate(userCompanyDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, userCompanyDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /user-companies} : get all the userCompanies.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of userCompanies in body.
     */
    @GetMapping("/user-companies")
    public ResponseEntity<List<UserCompanyDTO>> getAllUserCompanies(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of UserCompanies");
        Page<UserCompanyDTO> page = userCompanyService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /user-companies/:id} : get the "id" userCompany.
     *
     * @param id the id of the userCompanyDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the userCompanyDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/user-companies/{id}")
    public ResponseEntity<UserCompanyDTO> getUserCompany(@PathVariable Long id) {
        log.debug("REST request to get UserCompany : {}", id);
        Optional<UserCompanyDTO> userCompanyDTO = userCompanyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(userCompanyDTO);
    }

    /**
     * {@code DELETE  /user-companies/:id} : delete the "id" userCompany.
     *
     * @param id the id of the userCompanyDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/user-companies/{id}")
    public ResponseEntity<Void> deleteUserCompany(@PathVariable Long id) {
        log.debug("REST request to delete UserCompany : {}", id);
        userCompanyService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
