package de.jaide.slm2.web.rest;

import de.jaide.slm2.repository.ShopApprovalRuleRepository;
import de.jaide.slm2.service.ShopApprovalRuleService;
import de.jaide.slm2.service.dto.ShopApprovalRuleDTO;
import de.jaide.slm2.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link de.jaide.slm2.domain.ShopApprovalRule}.
 */
@RestController
@RequestMapping("/api")
public class ShopApprovalRuleResource {

    private final Logger log = LoggerFactory.getLogger(ShopApprovalRuleResource.class);

    private static final String ENTITY_NAME = "shopApprovalRule";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShopApprovalRuleService shopApprovalRuleService;

    private final ShopApprovalRuleRepository shopApprovalRuleRepository;

    public ShopApprovalRuleResource(
        ShopApprovalRuleService shopApprovalRuleService,
        ShopApprovalRuleRepository shopApprovalRuleRepository
    ) {
        this.shopApprovalRuleService = shopApprovalRuleService;
        this.shopApprovalRuleRepository = shopApprovalRuleRepository;
    }

    /**
     * {@code POST  /shop-approval-rules} : Create a new shopApprovalRule.
     *
     * @param shopApprovalRuleDTO the shopApprovalRuleDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new shopApprovalRuleDTO, or with status {@code 400 (Bad Request)} if the shopApprovalRule has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/shop-approval-rules")
    public ResponseEntity<ShopApprovalRuleDTO> createShopApprovalRule(@Valid @RequestBody ShopApprovalRuleDTO shopApprovalRuleDTO)
        throws URISyntaxException {
        log.debug("REST request to save ShopApprovalRule : {}", shopApprovalRuleDTO);
        if (shopApprovalRuleDTO.getId() != null) {
            throw new BadRequestAlertException("A new shopApprovalRule cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShopApprovalRuleDTO result = shopApprovalRuleService.save(shopApprovalRuleDTO);
        return ResponseEntity
            .created(new URI("/api/shop-approval-rules/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /shop-approval-rules/:id} : Updates an existing shopApprovalRule.
     *
     * @param id the id of the shopApprovalRuleDTO to save.
     * @param shopApprovalRuleDTO the shopApprovalRuleDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shopApprovalRuleDTO,
     * or with status {@code 400 (Bad Request)} if the shopApprovalRuleDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the shopApprovalRuleDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/shop-approval-rules/{id}")
    public ResponseEntity<ShopApprovalRuleDTO> updateShopApprovalRule(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody ShopApprovalRuleDTO shopApprovalRuleDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ShopApprovalRule : {}, {}", id, shopApprovalRuleDTO);
        if (shopApprovalRuleDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, shopApprovalRuleDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!shopApprovalRuleRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ShopApprovalRuleDTO result = shopApprovalRuleService.save(shopApprovalRuleDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, shopApprovalRuleDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /shop-approval-rules/:id} : Partial updates given fields of an existing shopApprovalRule, field will ignore if it is null
     *
     * @param id the id of the shopApprovalRuleDTO to save.
     * @param shopApprovalRuleDTO the shopApprovalRuleDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shopApprovalRuleDTO,
     * or with status {@code 400 (Bad Request)} if the shopApprovalRuleDTO is not valid,
     * or with status {@code 404 (Not Found)} if the shopApprovalRuleDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the shopApprovalRuleDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/shop-approval-rules/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ShopApprovalRuleDTO> partialUpdateShopApprovalRule(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody ShopApprovalRuleDTO shopApprovalRuleDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ShopApprovalRule partially : {}, {}", id, shopApprovalRuleDTO);
        if (shopApprovalRuleDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, shopApprovalRuleDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!shopApprovalRuleRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ShopApprovalRuleDTO> result = shopApprovalRuleService.partialUpdate(shopApprovalRuleDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, shopApprovalRuleDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /shop-approval-rules} : get all the shopApprovalRules.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shopApprovalRules in body.
     */
    @GetMapping("/shop-approval-rules")
    public ResponseEntity<List<ShopApprovalRuleDTO>> getAllShopApprovalRules(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get a page of ShopApprovalRules");
        Page<ShopApprovalRuleDTO> page = shopApprovalRuleService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /shop-approval-rules/:id} : get the "id" shopApprovalRule.
     *
     * @param id the id of the shopApprovalRuleDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the shopApprovalRuleDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/shop-approval-rules/{id}")
    public ResponseEntity<ShopApprovalRuleDTO> getShopApprovalRule(@PathVariable Long id) {
        log.debug("REST request to get ShopApprovalRule : {}", id);
        Optional<ShopApprovalRuleDTO> shopApprovalRuleDTO = shopApprovalRuleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(shopApprovalRuleDTO);
    }

    /**
     * {@code DELETE  /shop-approval-rules/:id} : delete the "id" shopApprovalRule.
     *
     * @param id the id of the shopApprovalRuleDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/shop-approval-rules/{id}")
    public ResponseEntity<Void> deleteShopApprovalRule(@PathVariable Long id) {
        log.debug("REST request to delete ShopApprovalRule : {}", id);
        shopApprovalRuleService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
