package de.jaide.slm2.web.rest;

import de.jaide.slm2.repository.OrderNoteRepository;
import de.jaide.slm2.service.OrderNoteService;
import de.jaide.slm2.service.dto.OrderNoteDTO;
import de.jaide.slm2.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link de.jaide.slm2.domain.OrderNote}.
 */
@RestController
@RequestMapping("/api")
public class OrderNoteResource {

    private final Logger log = LoggerFactory.getLogger(OrderNoteResource.class);

    private static final String ENTITY_NAME = "orderNote";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrderNoteService orderNoteService;

    private final OrderNoteRepository orderNoteRepository;

    public OrderNoteResource(OrderNoteService orderNoteService, OrderNoteRepository orderNoteRepository) {
        this.orderNoteService = orderNoteService;
        this.orderNoteRepository = orderNoteRepository;
    }

    /**
     * {@code POST  /order-notes} : Create a new orderNote.
     *
     * @param orderNoteDTO the orderNoteDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new orderNoteDTO, or with status {@code 400 (Bad Request)} if the orderNote has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/order-notes")
    public ResponseEntity<OrderNoteDTO> createOrderNote(@Valid @RequestBody OrderNoteDTO orderNoteDTO) throws URISyntaxException {
        log.debug("REST request to save OrderNote : {}", orderNoteDTO);
        if (orderNoteDTO.getId() != null) {
            throw new BadRequestAlertException("A new orderNote cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrderNoteDTO result = orderNoteService.save(orderNoteDTO);
        return ResponseEntity
            .created(new URI("/api/order-notes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /order-notes/:id} : Updates an existing orderNote.
     *
     * @param id the id of the orderNoteDTO to save.
     * @param orderNoteDTO the orderNoteDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated orderNoteDTO,
     * or with status {@code 400 (Bad Request)} if the orderNoteDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the orderNoteDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/order-notes/{id}")
    public ResponseEntity<OrderNoteDTO> updateOrderNote(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody OrderNoteDTO orderNoteDTO
    ) throws URISyntaxException {
        log.debug("REST request to update OrderNote : {}, {}", id, orderNoteDTO);
        if (orderNoteDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, orderNoteDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!orderNoteRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        OrderNoteDTO result = orderNoteService.save(orderNoteDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, orderNoteDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /order-notes/:id} : Partial updates given fields of an existing orderNote, field will ignore if it is null
     *
     * @param id the id of the orderNoteDTO to save.
     * @param orderNoteDTO the orderNoteDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated orderNoteDTO,
     * or with status {@code 400 (Bad Request)} if the orderNoteDTO is not valid,
     * or with status {@code 404 (Not Found)} if the orderNoteDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the orderNoteDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/order-notes/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<OrderNoteDTO> partialUpdateOrderNote(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody OrderNoteDTO orderNoteDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update OrderNote partially : {}, {}", id, orderNoteDTO);
        if (orderNoteDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, orderNoteDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!orderNoteRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<OrderNoteDTO> result = orderNoteService.partialUpdate(orderNoteDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, orderNoteDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /order-notes} : get all the orderNotes.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of orderNotes in body.
     */
    @GetMapping("/order-notes")
    public ResponseEntity<List<OrderNoteDTO>> getAllOrderNotes(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of OrderNotes");
        Page<OrderNoteDTO> page = orderNoteService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /order-notes/:id} : get the "id" orderNote.
     *
     * @param id the id of the orderNoteDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the orderNoteDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/order-notes/{id}")
    public ResponseEntity<OrderNoteDTO> getOrderNote(@PathVariable Long id) {
        log.debug("REST request to get OrderNote : {}", id);
        Optional<OrderNoteDTO> orderNoteDTO = orderNoteService.findOne(id);
        return ResponseUtil.wrapOrNotFound(orderNoteDTO);
    }

    /**
     * {@code DELETE  /order-notes/:id} : delete the "id" orderNote.
     *
     * @param id the id of the orderNoteDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/order-notes/{id}")
    public ResponseEntity<Void> deleteOrderNote(@PathVariable Long id) {
        log.debug("REST request to delete OrderNote : {}", id);
        orderNoteService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
