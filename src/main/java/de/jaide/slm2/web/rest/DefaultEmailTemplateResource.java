package de.jaide.slm2.web.rest;

import de.jaide.slm2.repository.DefaultEmailTemplateRepository;
import de.jaide.slm2.service.DefaultEmailTemplateService;
import de.jaide.slm2.service.dto.DefaultEmailTemplateDTO;
import de.jaide.slm2.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link de.jaide.slm2.domain.DefaultEmailTemplate}.
 */
@RestController
@RequestMapping("/api")
public class DefaultEmailTemplateResource {

    private final Logger log = LoggerFactory.getLogger(DefaultEmailTemplateResource.class);

    private static final String ENTITY_NAME = "defaultEmailTemplate";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DefaultEmailTemplateService defaultEmailTemplateService;

    private final DefaultEmailTemplateRepository defaultEmailTemplateRepository;

    public DefaultEmailTemplateResource(
        DefaultEmailTemplateService defaultEmailTemplateService,
        DefaultEmailTemplateRepository defaultEmailTemplateRepository
    ) {
        this.defaultEmailTemplateService = defaultEmailTemplateService;
        this.defaultEmailTemplateRepository = defaultEmailTemplateRepository;
    }

    /**
     * {@code POST  /default-email-templates} : Create a new defaultEmailTemplate.
     *
     * @param defaultEmailTemplateDTO the defaultEmailTemplateDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new defaultEmailTemplateDTO, or with status {@code 400 (Bad Request)} if the defaultEmailTemplate has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/default-email-templates")
    public ResponseEntity<DefaultEmailTemplateDTO> createDefaultEmailTemplate(
        @Valid @RequestBody DefaultEmailTemplateDTO defaultEmailTemplateDTO
    ) throws URISyntaxException {
        log.debug("REST request to save DefaultEmailTemplate : {}", defaultEmailTemplateDTO);
        if (defaultEmailTemplateDTO.getId() != null) {
            throw new BadRequestAlertException("A new defaultEmailTemplate cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DefaultEmailTemplateDTO result = defaultEmailTemplateService.save(defaultEmailTemplateDTO);
        return ResponseEntity
            .created(new URI("/api/default-email-templates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /default-email-templates/:id} : Updates an existing defaultEmailTemplate.
     *
     * @param id the id of the defaultEmailTemplateDTO to save.
     * @param defaultEmailTemplateDTO the defaultEmailTemplateDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated defaultEmailTemplateDTO,
     * or with status {@code 400 (Bad Request)} if the defaultEmailTemplateDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the defaultEmailTemplateDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/default-email-templates/{id}")
    public ResponseEntity<DefaultEmailTemplateDTO> updateDefaultEmailTemplate(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody DefaultEmailTemplateDTO defaultEmailTemplateDTO
    ) throws URISyntaxException {
        log.debug("REST request to update DefaultEmailTemplate : {}, {}", id, defaultEmailTemplateDTO);
        if (defaultEmailTemplateDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, defaultEmailTemplateDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!defaultEmailTemplateRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        DefaultEmailTemplateDTO result = defaultEmailTemplateService.save(defaultEmailTemplateDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, defaultEmailTemplateDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /default-email-templates/:id} : Partial updates given fields of an existing defaultEmailTemplate, field will ignore if it is null
     *
     * @param id the id of the defaultEmailTemplateDTO to save.
     * @param defaultEmailTemplateDTO the defaultEmailTemplateDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated defaultEmailTemplateDTO,
     * or with status {@code 400 (Bad Request)} if the defaultEmailTemplateDTO is not valid,
     * or with status {@code 404 (Not Found)} if the defaultEmailTemplateDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the defaultEmailTemplateDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/default-email-templates/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<DefaultEmailTemplateDTO> partialUpdateDefaultEmailTemplate(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody DefaultEmailTemplateDTO defaultEmailTemplateDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update DefaultEmailTemplate partially : {}, {}", id, defaultEmailTemplateDTO);
        if (defaultEmailTemplateDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, defaultEmailTemplateDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!defaultEmailTemplateRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<DefaultEmailTemplateDTO> result = defaultEmailTemplateService.partialUpdate(defaultEmailTemplateDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, defaultEmailTemplateDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /default-email-templates} : get all the defaultEmailTemplates.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of defaultEmailTemplates in body.
     */
    @GetMapping("/default-email-templates")
    public ResponseEntity<List<DefaultEmailTemplateDTO>> getAllDefaultEmailTemplates(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get a page of DefaultEmailTemplates");
        Page<DefaultEmailTemplateDTO> page = defaultEmailTemplateService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /default-email-templates/:id} : get the "id" defaultEmailTemplate.
     *
     * @param id the id of the defaultEmailTemplateDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the defaultEmailTemplateDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/default-email-templates/{id}")
    public ResponseEntity<DefaultEmailTemplateDTO> getDefaultEmailTemplate(@PathVariable Long id) {
        log.debug("REST request to get DefaultEmailTemplate : {}", id);
        Optional<DefaultEmailTemplateDTO> defaultEmailTemplateDTO = defaultEmailTemplateService.findOne(id);
        return ResponseUtil.wrapOrNotFound(defaultEmailTemplateDTO);
    }

    /**
     * {@code DELETE  /default-email-templates/:id} : delete the "id" defaultEmailTemplate.
     *
     * @param id the id of the defaultEmailTemplateDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/default-email-templates/{id}")
    public ResponseEntity<Void> deleteDefaultEmailTemplate(@PathVariable Long id) {
        log.debug("REST request to delete DefaultEmailTemplate : {}", id);
        defaultEmailTemplateService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
