package de.jaide.slm2.web.rest;

import de.jaide.slm2.repository.WebhookSubscriptionRepository;
import de.jaide.slm2.service.WebhookSubscriptionService;
import de.jaide.slm2.service.dto.WebhookSubscriptionDTO;
import de.jaide.slm2.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link de.jaide.slm2.domain.WebhookSubscription}.
 */
@RestController
@RequestMapping("/api")
public class WebhookSubscriptionResource {

    private final Logger log = LoggerFactory.getLogger(WebhookSubscriptionResource.class);

    private static final String ENTITY_NAME = "webhookSubscription";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final WebhookSubscriptionService webhookSubscriptionService;

    private final WebhookSubscriptionRepository webhookSubscriptionRepository;

    public WebhookSubscriptionResource(
        WebhookSubscriptionService webhookSubscriptionService,
        WebhookSubscriptionRepository webhookSubscriptionRepository
    ) {
        this.webhookSubscriptionService = webhookSubscriptionService;
        this.webhookSubscriptionRepository = webhookSubscriptionRepository;
    }

    /**
     * {@code POST  /webhook-subscriptions} : Create a new webhookSubscription.
     *
     * @param webhookSubscriptionDTO the webhookSubscriptionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new webhookSubscriptionDTO, or with status {@code 400 (Bad Request)} if the webhookSubscription has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/webhook-subscriptions")
    public ResponseEntity<WebhookSubscriptionDTO> createWebhookSubscription(
        @Valid @RequestBody WebhookSubscriptionDTO webhookSubscriptionDTO
    ) throws URISyntaxException {
        log.debug("REST request to save WebhookSubscription : {}", webhookSubscriptionDTO);
        if (webhookSubscriptionDTO.getId() != null) {
            throw new BadRequestAlertException("A new webhookSubscription cannot already have an ID", ENTITY_NAME, "idexists");
        }
        WebhookSubscriptionDTO result = webhookSubscriptionService.save(webhookSubscriptionDTO);
        return ResponseEntity
            .created(new URI("/api/webhook-subscriptions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /webhook-subscriptions/:id} : Updates an existing webhookSubscription.
     *
     * @param id the id of the webhookSubscriptionDTO to save.
     * @param webhookSubscriptionDTO the webhookSubscriptionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated webhookSubscriptionDTO,
     * or with status {@code 400 (Bad Request)} if the webhookSubscriptionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the webhookSubscriptionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/webhook-subscriptions/{id}")
    public ResponseEntity<WebhookSubscriptionDTO> updateWebhookSubscription(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody WebhookSubscriptionDTO webhookSubscriptionDTO
    ) throws URISyntaxException {
        log.debug("REST request to update WebhookSubscription : {}, {}", id, webhookSubscriptionDTO);
        if (webhookSubscriptionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, webhookSubscriptionDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!webhookSubscriptionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        WebhookSubscriptionDTO result = webhookSubscriptionService.save(webhookSubscriptionDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, webhookSubscriptionDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /webhook-subscriptions/:id} : Partial updates given fields of an existing webhookSubscription, field will ignore if it is null
     *
     * @param id the id of the webhookSubscriptionDTO to save.
     * @param webhookSubscriptionDTO the webhookSubscriptionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated webhookSubscriptionDTO,
     * or with status {@code 400 (Bad Request)} if the webhookSubscriptionDTO is not valid,
     * or with status {@code 404 (Not Found)} if the webhookSubscriptionDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the webhookSubscriptionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/webhook-subscriptions/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<WebhookSubscriptionDTO> partialUpdateWebhookSubscription(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody WebhookSubscriptionDTO webhookSubscriptionDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update WebhookSubscription partially : {}, {}", id, webhookSubscriptionDTO);
        if (webhookSubscriptionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, webhookSubscriptionDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!webhookSubscriptionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<WebhookSubscriptionDTO> result = webhookSubscriptionService.partialUpdate(webhookSubscriptionDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, webhookSubscriptionDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /webhook-subscriptions} : get all the webhookSubscriptions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of webhookSubscriptions in body.
     */
    @GetMapping("/webhook-subscriptions")
    public ResponseEntity<List<WebhookSubscriptionDTO>> getAllWebhookSubscriptions(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get a page of WebhookSubscriptions");
        Page<WebhookSubscriptionDTO> page = webhookSubscriptionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /webhook-subscriptions/:id} : get the "id" webhookSubscription.
     *
     * @param id the id of the webhookSubscriptionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the webhookSubscriptionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/webhook-subscriptions/{id}")
    public ResponseEntity<WebhookSubscriptionDTO> getWebhookSubscription(@PathVariable Long id) {
        log.debug("REST request to get WebhookSubscription : {}", id);
        Optional<WebhookSubscriptionDTO> webhookSubscriptionDTO = webhookSubscriptionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(webhookSubscriptionDTO);
    }

    /**
     * {@code DELETE  /webhook-subscriptions/:id} : delete the "id" webhookSubscription.
     *
     * @param id the id of the webhookSubscriptionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/webhook-subscriptions/{id}")
    public ResponseEntity<Void> deleteWebhookSubscription(@PathVariable Long id) {
        log.debug("REST request to delete WebhookSubscription : {}", id);
        webhookSubscriptionService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
