package de.jaide.slm2.web.rest;

import de.jaide.slm2.repository.ShopEmailTemplateRepository;
import de.jaide.slm2.service.ShopEmailTemplateService;
import de.jaide.slm2.service.dto.ShopEmailTemplateDTO;
import de.jaide.slm2.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link de.jaide.slm2.domain.ShopEmailTemplate}.
 */
@RestController
@RequestMapping("/api")
public class ShopEmailTemplateResource {

    private final Logger log = LoggerFactory.getLogger(ShopEmailTemplateResource.class);

    private static final String ENTITY_NAME = "shopEmailTemplate";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShopEmailTemplateService shopEmailTemplateService;

    private final ShopEmailTemplateRepository shopEmailTemplateRepository;

    public ShopEmailTemplateResource(
        ShopEmailTemplateService shopEmailTemplateService,
        ShopEmailTemplateRepository shopEmailTemplateRepository
    ) {
        this.shopEmailTemplateService = shopEmailTemplateService;
        this.shopEmailTemplateRepository = shopEmailTemplateRepository;
    }

    /**
     * {@code POST  /shop-email-templates} : Create a new shopEmailTemplate.
     *
     * @param shopEmailTemplateDTO the shopEmailTemplateDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new shopEmailTemplateDTO, or with status {@code 400 (Bad Request)} if the shopEmailTemplate has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/shop-email-templates")
    public ResponseEntity<ShopEmailTemplateDTO> createShopEmailTemplate(@Valid @RequestBody ShopEmailTemplateDTO shopEmailTemplateDTO)
        throws URISyntaxException {
        log.debug("REST request to save ShopEmailTemplate : {}", shopEmailTemplateDTO);
        if (shopEmailTemplateDTO.getId() != null) {
            throw new BadRequestAlertException("A new shopEmailTemplate cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ShopEmailTemplateDTO result = shopEmailTemplateService.save(shopEmailTemplateDTO);
        return ResponseEntity
            .created(new URI("/api/shop-email-templates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /shop-email-templates/:id} : Updates an existing shopEmailTemplate.
     *
     * @param id the id of the shopEmailTemplateDTO to save.
     * @param shopEmailTemplateDTO the shopEmailTemplateDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shopEmailTemplateDTO,
     * or with status {@code 400 (Bad Request)} if the shopEmailTemplateDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the shopEmailTemplateDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/shop-email-templates/{id}")
    public ResponseEntity<ShopEmailTemplateDTO> updateShopEmailTemplate(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody ShopEmailTemplateDTO shopEmailTemplateDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ShopEmailTemplate : {}, {}", id, shopEmailTemplateDTO);
        if (shopEmailTemplateDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, shopEmailTemplateDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!shopEmailTemplateRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ShopEmailTemplateDTO result = shopEmailTemplateService.save(shopEmailTemplateDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, shopEmailTemplateDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /shop-email-templates/:id} : Partial updates given fields of an existing shopEmailTemplate, field will ignore if it is null
     *
     * @param id the id of the shopEmailTemplateDTO to save.
     * @param shopEmailTemplateDTO the shopEmailTemplateDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shopEmailTemplateDTO,
     * or with status {@code 400 (Bad Request)} if the shopEmailTemplateDTO is not valid,
     * or with status {@code 404 (Not Found)} if the shopEmailTemplateDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the shopEmailTemplateDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/shop-email-templates/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ShopEmailTemplateDTO> partialUpdateShopEmailTemplate(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody ShopEmailTemplateDTO shopEmailTemplateDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ShopEmailTemplate partially : {}, {}", id, shopEmailTemplateDTO);
        if (shopEmailTemplateDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, shopEmailTemplateDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!shopEmailTemplateRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ShopEmailTemplateDTO> result = shopEmailTemplateService.partialUpdate(shopEmailTemplateDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, shopEmailTemplateDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /shop-email-templates} : get all the shopEmailTemplates.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shopEmailTemplates in body.
     */
    @GetMapping("/shop-email-templates")
    public ResponseEntity<List<ShopEmailTemplateDTO>> getAllShopEmailTemplates(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get a page of ShopEmailTemplates");
        Page<ShopEmailTemplateDTO> page = shopEmailTemplateService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /shop-email-templates/:id} : get the "id" shopEmailTemplate.
     *
     * @param id the id of the shopEmailTemplateDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the shopEmailTemplateDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/shop-email-templates/{id}")
    public ResponseEntity<ShopEmailTemplateDTO> getShopEmailTemplate(@PathVariable Long id) {
        log.debug("REST request to get ShopEmailTemplate : {}", id);
        Optional<ShopEmailTemplateDTO> shopEmailTemplateDTO = shopEmailTemplateService.findOne(id);
        return ResponseUtil.wrapOrNotFound(shopEmailTemplateDTO);
    }

    /**
     * {@code DELETE  /shop-email-templates/:id} : delete the "id" shopEmailTemplate.
     *
     * @param id the id of the shopEmailTemplateDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/shop-email-templates/{id}")
    public ResponseEntity<Void> deleteShopEmailTemplate(@PathVariable Long id) {
        log.debug("REST request to delete ShopEmailTemplate : {}", id);
        shopEmailTemplateService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
