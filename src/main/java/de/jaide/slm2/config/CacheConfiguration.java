package de.jaide.slm2.config;

import java.time.Duration;
import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;
import org.hibernate.cache.jcache.ConfigSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.info.GitProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.*;
import tech.jhipster.config.JHipsterProperties;
import tech.jhipster.config.cache.PrefixedKeyGenerator;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private GitProperties gitProperties;
    private BuildProperties buildProperties;
    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache = jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration =
            Eh107Configuration.fromEhcacheCacheConfiguration(
                CacheConfigurationBuilder
                    .newCacheConfigurationBuilder(Object.class, Object.class, ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                    .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                    .build()
            );
    }

    @Bean
    public HibernatePropertiesCustomizer hibernatePropertiesCustomizer(javax.cache.CacheManager cacheManager) {
        return hibernateProperties -> hibernateProperties.put(ConfigSettings.CACHE_MANAGER, cacheManager);
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, de.jaide.slm2.repository.UserRepository.USERS_BY_LOGIN_CACHE);
            createCache(cm, de.jaide.slm2.repository.UserRepository.USERS_BY_EMAIL_CACHE);
            createCache(cm, de.jaide.slm2.domain.User.class.getName());
            createCache(cm, de.jaide.slm2.domain.Authority.class.getName());
            createCache(cm, de.jaide.slm2.domain.User.class.getName() + ".authorities");
            createCache(cm, de.jaide.slm2.domain.Company.class.getName());
            createCache(cm, de.jaide.slm2.domain.Company.class.getName() + ".shops");
            createCache(cm, de.jaide.slm2.domain.Company.class.getName() + ".userCompanies");
            createCache(cm, de.jaide.slm2.domain.Shop.class.getName());
            createCache(cm, de.jaide.slm2.domain.Shop.class.getName() + ".userShops");
            createCache(cm, de.jaide.slm2.domain.Shop.class.getName() + ".productTypes");
            createCache(cm, de.jaide.slm2.domain.Shop.class.getName() + ".orders");
            createCache(cm, de.jaide.slm2.domain.Shop.class.getName() + ".shopEmailTemplates");
            createCache(cm, de.jaide.slm2.domain.Shop.class.getName() + ".shopApprovalRules");
            createCache(cm, de.jaide.slm2.domain.Shop.class.getName() + ".webhookSubscriptions");
            createCache(cm, de.jaide.slm2.domain.UserCompany.class.getName());
            createCache(cm, de.jaide.slm2.domain.UserShop.class.getName());
            createCache(cm, de.jaide.slm2.domain.ProductType.class.getName());
            createCache(cm, de.jaide.slm2.domain.ProductType.class.getName() + ".products");
            createCache(cm, de.jaide.slm2.domain.ProductType.class.getName() + ".orderItems");
            createCache(cm, de.jaide.slm2.domain.Product.class.getName());
            createCache(cm, de.jaide.slm2.domain.Product.class.getName() + ".orderItems");
            createCache(cm, de.jaide.slm2.domain.Order.class.getName());
            createCache(cm, de.jaide.slm2.domain.Order.class.getName() + ".orderItems");
            createCache(cm, de.jaide.slm2.domain.Order.class.getName() + ".orderNotes");
            createCache(cm, de.jaide.slm2.domain.Order.class.getName() + ".eventMessages");
            createCache(cm, de.jaide.slm2.domain.Order.class.getName() + ".orderApprovalNotifications");
            createCache(cm, de.jaide.slm2.domain.OrderItem.class.getName());
            createCache(cm, de.jaide.slm2.domain.OrderNote.class.getName());
            createCache(cm, de.jaide.slm2.domain.OrderApprovalNotification.class.getName());
            createCache(cm, de.jaide.slm2.domain.DefaultEmailTemplate.class.getName());
            createCache(cm, de.jaide.slm2.domain.DefaultEmailTemplate.class.getName() + ".shopEmailTemplates");
            createCache(cm, de.jaide.slm2.domain.ShopEmailTemplate.class.getName());
            createCache(cm, de.jaide.slm2.domain.ShopSmtp.class.getName());
            createCache(cm, de.jaide.slm2.domain.ShopApprovalRule.class.getName());
            createCache(cm, de.jaide.slm2.domain.WebhookSubscription.class.getName());
            createCache(cm, de.jaide.slm2.domain.EventMessage.class.getName());
            createCache(cm, de.jaide.slm2.domain.File.class.getName());
            createCache(cm, de.jaide.slm2.domain.File.class.getName() + ".companies");
            createCache(cm, de.jaide.slm2.domain.File.class.getName() + ".shops");
            // jhipster-needle-ehcache-add-entry
        };
    }

    private void createCache(javax.cache.CacheManager cm, String cacheName) {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache != null) {
            cache.clear();
        } else {
            cm.createCache(cacheName, jcacheConfiguration);
        }
    }

    @Autowired(required = false)
    public void setGitProperties(GitProperties gitProperties) {
        this.gitProperties = gitProperties;
    }

    @Autowired(required = false)
    public void setBuildProperties(BuildProperties buildProperties) {
        this.buildProperties = buildProperties;
    }

    @Bean
    public KeyGenerator keyGenerator() {
        return new PrefixedKeyGenerator(this.gitProperties, this.buildProperties);
    }
}
