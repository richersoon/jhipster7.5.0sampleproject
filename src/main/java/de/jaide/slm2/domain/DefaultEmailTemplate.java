package de.jaide.slm2.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import de.jaide.slm2.domain.enumeration.Language;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A DefaultEmailTemplate.
 */
@Entity
@Table(name = "default_email_template")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class DefaultEmailTemplate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "subject", nullable = false)
    private String subject;

    @Lob
    @Column(name = "header", nullable = false)
    private String header;

    @Lob
    @Column(name = "body", nullable = false)
    private String body;

    @Lob
    @Column(name = "footer", nullable = false)
    private String footer;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "language", nullable = false, unique = true)
    private Language language;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "last_modified_by")
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Column(name = "version")
    private Long version;

    @OneToMany(mappedBy = "defaultEmailTemplate")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "defaultEmailTemplate", "shop" }, allowSetters = true)
    private Set<ShopEmailTemplate> shopEmailTemplates = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public DefaultEmailTemplate id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubject() {
        return this.subject;
    }

    public DefaultEmailTemplate subject(String subject) {
        this.setSubject(subject);
        return this;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getHeader() {
        return this.header;
    }

    public DefaultEmailTemplate header(String header) {
        this.setHeader(header);
        return this;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getBody() {
        return this.body;
    }

    public DefaultEmailTemplate body(String body) {
        this.setBody(body);
        return this;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getFooter() {
        return this.footer;
    }

    public DefaultEmailTemplate footer(String footer) {
        this.setFooter(footer);
        return this;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public Language getLanguage() {
        return this.language;
    }

    public DefaultEmailTemplate language(Language language) {
        this.setLanguage(language);
        return this;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public DefaultEmailTemplate createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public DefaultEmailTemplate createdDate(Instant createdDate) {
        this.setCreatedDate(createdDate);
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    public DefaultEmailTemplate lastModifiedBy(String lastModifiedBy) {
        this.setLastModifiedBy(lastModifiedBy);
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return this.lastModifiedDate;
    }

    public DefaultEmailTemplate lastModifiedDate(Instant lastModifiedDate) {
        this.setLastModifiedDate(lastModifiedDate);
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Long getVersion() {
        return this.version;
    }

    public DefaultEmailTemplate version(Long version) {
        this.setVersion(version);
        return this;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Set<ShopEmailTemplate> getShopEmailTemplates() {
        return this.shopEmailTemplates;
    }

    public void setShopEmailTemplates(Set<ShopEmailTemplate> shopEmailTemplates) {
        if (this.shopEmailTemplates != null) {
            this.shopEmailTemplates.forEach(i -> i.setDefaultEmailTemplate(null));
        }
        if (shopEmailTemplates != null) {
            shopEmailTemplates.forEach(i -> i.setDefaultEmailTemplate(this));
        }
        this.shopEmailTemplates = shopEmailTemplates;
    }

    public DefaultEmailTemplate shopEmailTemplates(Set<ShopEmailTemplate> shopEmailTemplates) {
        this.setShopEmailTemplates(shopEmailTemplates);
        return this;
    }

    public DefaultEmailTemplate addShopEmailTemplate(ShopEmailTemplate shopEmailTemplate) {
        this.shopEmailTemplates.add(shopEmailTemplate);
        shopEmailTemplate.setDefaultEmailTemplate(this);
        return this;
    }

    public DefaultEmailTemplate removeShopEmailTemplate(ShopEmailTemplate shopEmailTemplate) {
        this.shopEmailTemplates.remove(shopEmailTemplate);
        shopEmailTemplate.setDefaultEmailTemplate(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DefaultEmailTemplate)) {
            return false;
        }
        return id != null && id.equals(((DefaultEmailTemplate) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DefaultEmailTemplate{" +
            "id=" + getId() +
            ", subject='" + getSubject() + "'" +
            ", header='" + getHeader() + "'" +
            ", body='" + getBody() + "'" +
            ", footer='" + getFooter() + "'" +
            ", language='" + getLanguage() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", version=" + getVersion() +
            "}";
    }
}
