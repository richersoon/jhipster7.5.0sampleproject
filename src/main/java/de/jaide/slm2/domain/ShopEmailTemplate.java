package de.jaide.slm2.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ShopEmailTemplate.
 */
@Entity
@Table(name = "shop_email_template")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ShopEmailTemplate implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "subject", nullable = false)
    private String subject;

    @Lob
    @Column(name = "header", nullable = false)
    private String header;

    @Lob
    @Column(name = "body", nullable = false)
    private String body;

    @Lob
    @Column(name = "footer", nullable = false)
    private String footer;

    @NotNull
    @Column(name = "reply_to", nullable = false)
    private String replyTo;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "last_modified_by")
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Column(name = "version")
    private Long version;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "shopEmailTemplates" }, allowSetters = true)
    private DefaultEmailTemplate defaultEmailTemplate;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(
        value = {
            "userShops",
            "productTypes",
            "orders",
            "shopEmailTemplates",
            "shopApprovalRules",
            "webhookSubscriptions",
            "shopSmtp",
            "company",
            "logoFile",
        },
        allowSetters = true
    )
    private Shop shop;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public ShopEmailTemplate id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubject() {
        return this.subject;
    }

    public ShopEmailTemplate subject(String subject) {
        this.setSubject(subject);
        return this;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getHeader() {
        return this.header;
    }

    public ShopEmailTemplate header(String header) {
        this.setHeader(header);
        return this;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getBody() {
        return this.body;
    }

    public ShopEmailTemplate body(String body) {
        this.setBody(body);
        return this;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getFooter() {
        return this.footer;
    }

    public ShopEmailTemplate footer(String footer) {
        this.setFooter(footer);
        return this;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public String getReplyTo() {
        return this.replyTo;
    }

    public ShopEmailTemplate replyTo(String replyTo) {
        this.setReplyTo(replyTo);
        return this;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public ShopEmailTemplate createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public ShopEmailTemplate createdDate(Instant createdDate) {
        this.setCreatedDate(createdDate);
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    public ShopEmailTemplate lastModifiedBy(String lastModifiedBy) {
        this.setLastModifiedBy(lastModifiedBy);
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return this.lastModifiedDate;
    }

    public ShopEmailTemplate lastModifiedDate(Instant lastModifiedDate) {
        this.setLastModifiedDate(lastModifiedDate);
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Long getVersion() {
        return this.version;
    }

    public ShopEmailTemplate version(Long version) {
        this.setVersion(version);
        return this;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public DefaultEmailTemplate getDefaultEmailTemplate() {
        return this.defaultEmailTemplate;
    }

    public void setDefaultEmailTemplate(DefaultEmailTemplate defaultEmailTemplate) {
        this.defaultEmailTemplate = defaultEmailTemplate;
    }

    public ShopEmailTemplate defaultEmailTemplate(DefaultEmailTemplate defaultEmailTemplate) {
        this.setDefaultEmailTemplate(defaultEmailTemplate);
        return this;
    }

    public Shop getShop() {
        return this.shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public ShopEmailTemplate shop(Shop shop) {
        this.setShop(shop);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ShopEmailTemplate)) {
            return false;
        }
        return id != null && id.equals(((ShopEmailTemplate) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ShopEmailTemplate{" +
            "id=" + getId() +
            ", subject='" + getSubject() + "'" +
            ", header='" + getHeader() + "'" +
            ", body='" + getBody() + "'" +
            ", footer='" + getFooter() + "'" +
            ", replyTo='" + getReplyTo() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", version=" + getVersion() +
            "}";
    }
}
