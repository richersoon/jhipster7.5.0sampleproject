package de.jaide.slm2.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import de.jaide.slm2.domain.enumeration.ExternalType;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Shop.
 */
@Entity
@Table(name = "shop")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Shop implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "url", nullable = false, unique = true)
    private String url;

    @Enumerated(EnumType.STRING)
    @Column(name = "external_type")
    private ExternalType externalType;

    @Column(name = "authorization_code", unique = true)
    private String authorizationCode;

    @Column(name = "generated_token", unique = true)
    private String generatedToken;

    @Column(name = "access_token", unique = true)
    private String accessToken;

    @Column(name = "consumer_key")
    private String consumerKey;

    @Column(name = "consumer_secret")
    private String consumerSecret;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "last_modified_by")
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Column(name = "version")
    private Long version;

    @OneToMany(mappedBy = "shop")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "user", "shop" }, allowSetters = true)
    private Set<UserShop> userShops = new HashSet<>();

    @OneToMany(mappedBy = "shop")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "products", "orderItems", "shop" }, allowSetters = true)
    private Set<ProductType> productTypes = new HashSet<>();

    @OneToMany(mappedBy = "shop")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(
        value = { "orderItems", "orderNotes", "eventMessages", "orderApprovalNotifications", "shop" },
        allowSetters = true
    )
    private Set<Order> orders = new HashSet<>();

    @OneToMany(mappedBy = "shop")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "defaultEmailTemplate", "shop" }, allowSetters = true)
    private Set<ShopEmailTemplate> shopEmailTemplates = new HashSet<>();

    @OneToMany(mappedBy = "shop")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "shop" }, allowSetters = true)
    private Set<ShopApprovalRule> shopApprovalRules = new HashSet<>();

    @OneToMany(mappedBy = "shop")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "shop" }, allowSetters = true)
    private Set<WebhookSubscription> webhookSubscriptions = new HashSet<>();

    @JsonIgnoreProperties(value = { "shop" }, allowSetters = true)
    @OneToOne(mappedBy = "shop")
    private ShopSmtp shopSmtp;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(value = { "shops", "userCompanies", "logoFile" }, allowSetters = true)
    private Company company;

    @ManyToOne
    @JsonIgnoreProperties(value = { "companies", "shops", "uploaderUser" }, allowSetters = true)
    private File logoFile;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Shop id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Shop name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return this.url;
    }

    public Shop url(String url) {
        this.setUrl(url);
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ExternalType getExternalType() {
        return this.externalType;
    }

    public Shop externalType(ExternalType externalType) {
        this.setExternalType(externalType);
        return this;
    }

    public void setExternalType(ExternalType externalType) {
        this.externalType = externalType;
    }

    public String getAuthorizationCode() {
        return this.authorizationCode;
    }

    public Shop authorizationCode(String authorizationCode) {
        this.setAuthorizationCode(authorizationCode);
        return this;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getGeneratedToken() {
        return this.generatedToken;
    }

    public Shop generatedToken(String generatedToken) {
        this.setGeneratedToken(generatedToken);
        return this;
    }

    public void setGeneratedToken(String generatedToken) {
        this.generatedToken = generatedToken;
    }

    public String getAccessToken() {
        return this.accessToken;
    }

    public Shop accessToken(String accessToken) {
        this.setAccessToken(accessToken);
        return this;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getConsumerKey() {
        return this.consumerKey;
    }

    public Shop consumerKey(String consumerKey) {
        this.setConsumerKey(consumerKey);
        return this;
    }

    public void setConsumerKey(String consumerKey) {
        this.consumerKey = consumerKey;
    }

    public String getConsumerSecret() {
        return this.consumerSecret;
    }

    public Shop consumerSecret(String consumerSecret) {
        this.setConsumerSecret(consumerSecret);
        return this;
    }

    public void setConsumerSecret(String consumerSecret) {
        this.consumerSecret = consumerSecret;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public Shop createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public Shop createdDate(Instant createdDate) {
        this.setCreatedDate(createdDate);
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    public Shop lastModifiedBy(String lastModifiedBy) {
        this.setLastModifiedBy(lastModifiedBy);
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return this.lastModifiedDate;
    }

    public Shop lastModifiedDate(Instant lastModifiedDate) {
        this.setLastModifiedDate(lastModifiedDate);
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Long getVersion() {
        return this.version;
    }

    public Shop version(Long version) {
        this.setVersion(version);
        return this;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Set<UserShop> getUserShops() {
        return this.userShops;
    }

    public void setUserShops(Set<UserShop> userShops) {
        if (this.userShops != null) {
            this.userShops.forEach(i -> i.setShop(null));
        }
        if (userShops != null) {
            userShops.forEach(i -> i.setShop(this));
        }
        this.userShops = userShops;
    }

    public Shop userShops(Set<UserShop> userShops) {
        this.setUserShops(userShops);
        return this;
    }

    public Shop addUserShop(UserShop userShop) {
        this.userShops.add(userShop);
        userShop.setShop(this);
        return this;
    }

    public Shop removeUserShop(UserShop userShop) {
        this.userShops.remove(userShop);
        userShop.setShop(null);
        return this;
    }

    public Set<ProductType> getProductTypes() {
        return this.productTypes;
    }

    public void setProductTypes(Set<ProductType> productTypes) {
        if (this.productTypes != null) {
            this.productTypes.forEach(i -> i.setShop(null));
        }
        if (productTypes != null) {
            productTypes.forEach(i -> i.setShop(this));
        }
        this.productTypes = productTypes;
    }

    public Shop productTypes(Set<ProductType> productTypes) {
        this.setProductTypes(productTypes);
        return this;
    }

    public Shop addProductType(ProductType productType) {
        this.productTypes.add(productType);
        productType.setShop(this);
        return this;
    }

    public Shop removeProductType(ProductType productType) {
        this.productTypes.remove(productType);
        productType.setShop(null);
        return this;
    }

    public Set<Order> getOrders() {
        return this.orders;
    }

    public void setOrders(Set<Order> orders) {
        if (this.orders != null) {
            this.orders.forEach(i -> i.setShop(null));
        }
        if (orders != null) {
            orders.forEach(i -> i.setShop(this));
        }
        this.orders = orders;
    }

    public Shop orders(Set<Order> orders) {
        this.setOrders(orders);
        return this;
    }

    public Shop addOrder(Order order) {
        this.orders.add(order);
        order.setShop(this);
        return this;
    }

    public Shop removeOrder(Order order) {
        this.orders.remove(order);
        order.setShop(null);
        return this;
    }

    public Set<ShopEmailTemplate> getShopEmailTemplates() {
        return this.shopEmailTemplates;
    }

    public void setShopEmailTemplates(Set<ShopEmailTemplate> shopEmailTemplates) {
        if (this.shopEmailTemplates != null) {
            this.shopEmailTemplates.forEach(i -> i.setShop(null));
        }
        if (shopEmailTemplates != null) {
            shopEmailTemplates.forEach(i -> i.setShop(this));
        }
        this.shopEmailTemplates = shopEmailTemplates;
    }

    public Shop shopEmailTemplates(Set<ShopEmailTemplate> shopEmailTemplates) {
        this.setShopEmailTemplates(shopEmailTemplates);
        return this;
    }

    public Shop addShopEmailTemplate(ShopEmailTemplate shopEmailTemplate) {
        this.shopEmailTemplates.add(shopEmailTemplate);
        shopEmailTemplate.setShop(this);
        return this;
    }

    public Shop removeShopEmailTemplate(ShopEmailTemplate shopEmailTemplate) {
        this.shopEmailTemplates.remove(shopEmailTemplate);
        shopEmailTemplate.setShop(null);
        return this;
    }

    public Set<ShopApprovalRule> getShopApprovalRules() {
        return this.shopApprovalRules;
    }

    public void setShopApprovalRules(Set<ShopApprovalRule> shopApprovalRules) {
        if (this.shopApprovalRules != null) {
            this.shopApprovalRules.forEach(i -> i.setShop(null));
        }
        if (shopApprovalRules != null) {
            shopApprovalRules.forEach(i -> i.setShop(this));
        }
        this.shopApprovalRules = shopApprovalRules;
    }

    public Shop shopApprovalRules(Set<ShopApprovalRule> shopApprovalRules) {
        this.setShopApprovalRules(shopApprovalRules);
        return this;
    }

    public Shop addShopApprovalRule(ShopApprovalRule shopApprovalRule) {
        this.shopApprovalRules.add(shopApprovalRule);
        shopApprovalRule.setShop(this);
        return this;
    }

    public Shop removeShopApprovalRule(ShopApprovalRule shopApprovalRule) {
        this.shopApprovalRules.remove(shopApprovalRule);
        shopApprovalRule.setShop(null);
        return this;
    }

    public Set<WebhookSubscription> getWebhookSubscriptions() {
        return this.webhookSubscriptions;
    }

    public void setWebhookSubscriptions(Set<WebhookSubscription> webhookSubscriptions) {
        if (this.webhookSubscriptions != null) {
            this.webhookSubscriptions.forEach(i -> i.setShop(null));
        }
        if (webhookSubscriptions != null) {
            webhookSubscriptions.forEach(i -> i.setShop(this));
        }
        this.webhookSubscriptions = webhookSubscriptions;
    }

    public Shop webhookSubscriptions(Set<WebhookSubscription> webhookSubscriptions) {
        this.setWebhookSubscriptions(webhookSubscriptions);
        return this;
    }

    public Shop addWebhookSubscription(WebhookSubscription webhookSubscription) {
        this.webhookSubscriptions.add(webhookSubscription);
        webhookSubscription.setShop(this);
        return this;
    }

    public Shop removeWebhookSubscription(WebhookSubscription webhookSubscription) {
        this.webhookSubscriptions.remove(webhookSubscription);
        webhookSubscription.setShop(null);
        return this;
    }

    public ShopSmtp getShopSmtp() {
        return this.shopSmtp;
    }

    public void setShopSmtp(ShopSmtp shopSmtp) {
        if (this.shopSmtp != null) {
            this.shopSmtp.setShop(null);
        }
        if (shopSmtp != null) {
            shopSmtp.setShop(this);
        }
        this.shopSmtp = shopSmtp;
    }

    public Shop shopSmtp(ShopSmtp shopSmtp) {
        this.setShopSmtp(shopSmtp);
        return this;
    }

    public Company getCompany() {
        return this.company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Shop company(Company company) {
        this.setCompany(company);
        return this;
    }

    public File getLogoFile() {
        return this.logoFile;
    }

    public void setLogoFile(File file) {
        this.logoFile = file;
    }

    public Shop logoFile(File file) {
        this.setLogoFile(file);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Shop)) {
            return false;
        }
        return id != null && id.equals(((Shop) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Shop{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", url='" + getUrl() + "'" +
            ", externalType='" + getExternalType() + "'" +
            ", authorizationCode='" + getAuthorizationCode() + "'" +
            ", generatedToken='" + getGeneratedToken() + "'" +
            ", accessToken='" + getAccessToken() + "'" +
            ", consumerKey='" + getConsumerKey() + "'" +
            ", consumerSecret='" + getConsumerSecret() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", version=" + getVersion() +
            "}";
    }
}
