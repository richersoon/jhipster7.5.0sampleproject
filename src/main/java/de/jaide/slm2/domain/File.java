package de.jaide.slm2.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A File.
 */
@Entity
@Table(name = "file")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class File implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "url", nullable = false)
    private String url;

    @NotNull
    @Column(name = "mime_type", nullable = false)
    private String mimeType;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "last_modified_by")
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Column(name = "version")
    private Long version;

    @OneToMany(mappedBy = "logoFile")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "shops", "userCompanies", "logoFile" }, allowSetters = true)
    private Set<Company> companies = new HashSet<>();

    @OneToMany(mappedBy = "logoFile")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(
        value = {
            "userShops",
            "productTypes",
            "orders",
            "shopEmailTemplates",
            "shopApprovalRules",
            "webhookSubscriptions",
            "shopSmtp",
            "company",
            "logoFile",
        },
        allowSetters = true
    )
    private Set<Shop> shops = new HashSet<>();

    @ManyToOne(optional = false)
    @NotNull
    private User uploaderUser;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public File id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public File name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return this.url;
    }

    public File url(String url) {
        this.setUrl(url);
        return this;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMimeType() {
        return this.mimeType;
    }

    public File mimeType(String mimeType) {
        this.setMimeType(mimeType);
        return this;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public File createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public File createdDate(Instant createdDate) {
        this.setCreatedDate(createdDate);
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    public File lastModifiedBy(String lastModifiedBy) {
        this.setLastModifiedBy(lastModifiedBy);
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return this.lastModifiedDate;
    }

    public File lastModifiedDate(Instant lastModifiedDate) {
        this.setLastModifiedDate(lastModifiedDate);
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Long getVersion() {
        return this.version;
    }

    public File version(Long version) {
        this.setVersion(version);
        return this;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Set<Company> getCompanies() {
        return this.companies;
    }

    public void setCompanies(Set<Company> companies) {
        if (this.companies != null) {
            this.companies.forEach(i -> i.setLogoFile(null));
        }
        if (companies != null) {
            companies.forEach(i -> i.setLogoFile(this));
        }
        this.companies = companies;
    }

    public File companies(Set<Company> companies) {
        this.setCompanies(companies);
        return this;
    }

    public File addCompany(Company company) {
        this.companies.add(company);
        company.setLogoFile(this);
        return this;
    }

    public File removeCompany(Company company) {
        this.companies.remove(company);
        company.setLogoFile(null);
        return this;
    }

    public Set<Shop> getShops() {
        return this.shops;
    }

    public void setShops(Set<Shop> shops) {
        if (this.shops != null) {
            this.shops.forEach(i -> i.setLogoFile(null));
        }
        if (shops != null) {
            shops.forEach(i -> i.setLogoFile(this));
        }
        this.shops = shops;
    }

    public File shops(Set<Shop> shops) {
        this.setShops(shops);
        return this;
    }

    public File addShop(Shop shop) {
        this.shops.add(shop);
        shop.setLogoFile(this);
        return this;
    }

    public File removeShop(Shop shop) {
        this.shops.remove(shop);
        shop.setLogoFile(null);
        return this;
    }

    public User getUploaderUser() {
        return this.uploaderUser;
    }

    public void setUploaderUser(User user) {
        this.uploaderUser = user;
    }

    public File uploaderUser(User user) {
        this.setUploaderUser(user);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof File)) {
            return false;
        }
        return id != null && id.equals(((File) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "File{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", url='" + getUrl() + "'" +
            ", mimeType='" + getMimeType() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", version=" + getVersion() +
            "}";
    }
}
