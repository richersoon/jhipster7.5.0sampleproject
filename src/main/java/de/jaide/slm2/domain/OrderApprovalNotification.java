package de.jaide.slm2.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import de.jaide.slm2.domain.enumeration.OrderApprovalStatus;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A OrderApprovalNotification.
 */
@Entity
@Table(name = "order_approval_notification")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class OrderApprovalNotification implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private OrderApprovalStatus status;

    @Column(name = "browser_ip_city")
    private String browserIpCity;

    @Column(name = "browser_ip_province")
    private String browserIpProvince;

    @Column(name = "browser_ip_country")
    private String browserIpCountry;

    @Column(name = "browser_ip_latitude")
    private String browserIpLatitude;

    @Column(name = "browser_ip_longitude")
    private String browserIpLongitude;

    @Column(name = "browser_ip_zip")
    private String browserIpZip;

    @Column(name = "distance_km", precision = 21, scale = 2)
    private BigDecimal distanceKm;

    @Column(name = "duration_seconds")
    private Integer durationSeconds;

    @Column(name = "note")
    private String note;

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "last_modified_by")
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Column(name = "version")
    private Long version;

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "orderItems", "orderNotes", "eventMessages", "orderApprovalNotifications", "shop" },
        allowSetters = true
    )
    private Order order;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public OrderApprovalNotification id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OrderApprovalStatus getStatus() {
        return this.status;
    }

    public OrderApprovalNotification status(OrderApprovalStatus status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(OrderApprovalStatus status) {
        this.status = status;
    }

    public String getBrowserIpCity() {
        return this.browserIpCity;
    }

    public OrderApprovalNotification browserIpCity(String browserIpCity) {
        this.setBrowserIpCity(browserIpCity);
        return this;
    }

    public void setBrowserIpCity(String browserIpCity) {
        this.browserIpCity = browserIpCity;
    }

    public String getBrowserIpProvince() {
        return this.browserIpProvince;
    }

    public OrderApprovalNotification browserIpProvince(String browserIpProvince) {
        this.setBrowserIpProvince(browserIpProvince);
        return this;
    }

    public void setBrowserIpProvince(String browserIpProvince) {
        this.browserIpProvince = browserIpProvince;
    }

    public String getBrowserIpCountry() {
        return this.browserIpCountry;
    }

    public OrderApprovalNotification browserIpCountry(String browserIpCountry) {
        this.setBrowserIpCountry(browserIpCountry);
        return this;
    }

    public void setBrowserIpCountry(String browserIpCountry) {
        this.browserIpCountry = browserIpCountry;
    }

    public String getBrowserIpLatitude() {
        return this.browserIpLatitude;
    }

    public OrderApprovalNotification browserIpLatitude(String browserIpLatitude) {
        this.setBrowserIpLatitude(browserIpLatitude);
        return this;
    }

    public void setBrowserIpLatitude(String browserIpLatitude) {
        this.browserIpLatitude = browserIpLatitude;
    }

    public String getBrowserIpLongitude() {
        return this.browserIpLongitude;
    }

    public OrderApprovalNotification browserIpLongitude(String browserIpLongitude) {
        this.setBrowserIpLongitude(browserIpLongitude);
        return this;
    }

    public void setBrowserIpLongitude(String browserIpLongitude) {
        this.browserIpLongitude = browserIpLongitude;
    }

    public String getBrowserIpZip() {
        return this.browserIpZip;
    }

    public OrderApprovalNotification browserIpZip(String browserIpZip) {
        this.setBrowserIpZip(browserIpZip);
        return this;
    }

    public void setBrowserIpZip(String browserIpZip) {
        this.browserIpZip = browserIpZip;
    }

    public BigDecimal getDistanceKm() {
        return this.distanceKm;
    }

    public OrderApprovalNotification distanceKm(BigDecimal distanceKm) {
        this.setDistanceKm(distanceKm);
        return this;
    }

    public void setDistanceKm(BigDecimal distanceKm) {
        this.distanceKm = distanceKm;
    }

    public Integer getDurationSeconds() {
        return this.durationSeconds;
    }

    public OrderApprovalNotification durationSeconds(Integer durationSeconds) {
        this.setDurationSeconds(durationSeconds);
        return this;
    }

    public void setDurationSeconds(Integer durationSeconds) {
        this.durationSeconds = durationSeconds;
    }

    public String getNote() {
        return this.note;
    }

    public OrderApprovalNotification note(String note) {
        this.setNote(note);
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public OrderApprovalNotification createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public OrderApprovalNotification createdDate(Instant createdDate) {
        this.setCreatedDate(createdDate);
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    public OrderApprovalNotification lastModifiedBy(String lastModifiedBy) {
        this.setLastModifiedBy(lastModifiedBy);
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return this.lastModifiedDate;
    }

    public OrderApprovalNotification lastModifiedDate(Instant lastModifiedDate) {
        this.setLastModifiedDate(lastModifiedDate);
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Long getVersion() {
        return this.version;
    }

    public OrderApprovalNotification version(Long version) {
        this.setVersion(version);
        return this;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Order getOrder() {
        return this.order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public OrderApprovalNotification order(Order order) {
        this.setOrder(order);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrderApprovalNotification)) {
            return false;
        }
        return id != null && id.equals(((OrderApprovalNotification) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OrderApprovalNotification{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            ", browserIpCity='" + getBrowserIpCity() + "'" +
            ", browserIpProvince='" + getBrowserIpProvince() + "'" +
            ", browserIpCountry='" + getBrowserIpCountry() + "'" +
            ", browserIpLatitude='" + getBrowserIpLatitude() + "'" +
            ", browserIpLongitude='" + getBrowserIpLongitude() + "'" +
            ", browserIpZip='" + getBrowserIpZip() + "'" +
            ", distanceKm=" + getDistanceKm() +
            ", durationSeconds=" + getDurationSeconds() +
            ", note='" + getNote() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", version=" + getVersion() +
            "}";
    }
}
