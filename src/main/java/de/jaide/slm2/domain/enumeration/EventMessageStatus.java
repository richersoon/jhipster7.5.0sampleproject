package de.jaide.slm2.domain.enumeration;

/**
 * The EventMessageStatus enumeration.
 */
public enum EventMessageStatus {
    PENDING,
    COMPLETED,
    ERROR,
}
