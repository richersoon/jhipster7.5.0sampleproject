package de.jaide.slm2.domain.enumeration;

/**
 * The Role enumeration.
 */
public enum Role {
    MANAGER,
    EMPLOYEE,
}
