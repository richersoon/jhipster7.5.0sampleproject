package de.jaide.slm2.domain.enumeration;

/**
 * The Language enumeration.
 */
public enum Language {
    EN("en"),
    DE("de");

    private final String value;

    Language(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
