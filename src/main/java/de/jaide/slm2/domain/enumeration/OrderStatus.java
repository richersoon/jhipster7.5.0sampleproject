package de.jaide.slm2.domain.enumeration;

/**
 * The OrderStatus enumeration.
 */
public enum OrderStatus {
    PROCESSING,
    OUT_OF_STOCK,
    IN_REVIEW,
    DECLINED,
    UNKNOWN_PRODUCT,
    FULFILLED,
    SENT,
    COMPLETED,
}
