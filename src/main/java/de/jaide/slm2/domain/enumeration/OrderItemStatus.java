package de.jaide.slm2.domain.enumeration;

/**
 * The OrderItemStatus enumeration.
 */
public enum OrderItemStatus {
    PROCESSING,
    OUT_OF_STOCK,
    IN_REVIEW,
    UNKNOWN_PRODUCT,
    FULFILLED,
    SENT,
    COMPLETED,
}
