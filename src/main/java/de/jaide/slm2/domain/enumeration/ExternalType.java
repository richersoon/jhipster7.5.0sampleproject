package de.jaide.slm2.domain.enumeration;

/**
 * The ExternalType enumeration.
 */
public enum ExternalType {
    SHOPIFY,
    WOOCOMMERCE,
    EBAY,
}
