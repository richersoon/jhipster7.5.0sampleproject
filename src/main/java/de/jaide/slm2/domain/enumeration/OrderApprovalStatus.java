package de.jaide.slm2.domain.enumeration;

/**
 * The OrderApprovalStatus enumeration.
 */
public enum OrderApprovalStatus {
    PENDING,
    COMPLETED,
    ERROR,
}
