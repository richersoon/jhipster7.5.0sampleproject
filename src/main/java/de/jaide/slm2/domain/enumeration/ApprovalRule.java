package de.jaide.slm2.domain.enumeration;

/**
 * The ApprovalRule enumeration.
 */
public enum ApprovalRule {
    QUANTITY,
    PRICE,
}
