package de.jaide.slm2.domain.enumeration;

/**
 * The WebhookType enumeration.
 */
public enum WebhookType {
    ORDER_PAID,
    APP_UNINSTALLED,
}
