package de.jaide.slm2.service;

import de.jaide.slm2.service.dto.WebhookSubscriptionDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link de.jaide.slm2.domain.WebhookSubscription}.
 */
public interface WebhookSubscriptionService {
    /**
     * Save a webhookSubscription.
     *
     * @param webhookSubscriptionDTO the entity to save.
     * @return the persisted entity.
     */
    WebhookSubscriptionDTO save(WebhookSubscriptionDTO webhookSubscriptionDTO);

    /**
     * Partially updates a webhookSubscription.
     *
     * @param webhookSubscriptionDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<WebhookSubscriptionDTO> partialUpdate(WebhookSubscriptionDTO webhookSubscriptionDTO);

    /**
     * Get all the webhookSubscriptions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<WebhookSubscriptionDTO> findAll(Pageable pageable);

    /**
     * Get the "id" webhookSubscription.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<WebhookSubscriptionDTO> findOne(Long id);

    /**
     * Delete the "id" webhookSubscription.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
