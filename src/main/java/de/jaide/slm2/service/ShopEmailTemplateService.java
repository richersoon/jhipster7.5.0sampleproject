package de.jaide.slm2.service;

import de.jaide.slm2.service.dto.ShopEmailTemplateDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link de.jaide.slm2.domain.ShopEmailTemplate}.
 */
public interface ShopEmailTemplateService {
    /**
     * Save a shopEmailTemplate.
     *
     * @param shopEmailTemplateDTO the entity to save.
     * @return the persisted entity.
     */
    ShopEmailTemplateDTO save(ShopEmailTemplateDTO shopEmailTemplateDTO);

    /**
     * Partially updates a shopEmailTemplate.
     *
     * @param shopEmailTemplateDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ShopEmailTemplateDTO> partialUpdate(ShopEmailTemplateDTO shopEmailTemplateDTO);

    /**
     * Get all the shopEmailTemplates.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ShopEmailTemplateDTO> findAll(Pageable pageable);

    /**
     * Get the "id" shopEmailTemplate.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ShopEmailTemplateDTO> findOne(Long id);

    /**
     * Delete the "id" shopEmailTemplate.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
