package de.jaide.slm2.service.impl;

import de.jaide.slm2.domain.EventMessage;
import de.jaide.slm2.repository.EventMessageRepository;
import de.jaide.slm2.service.EventMessageService;
import de.jaide.slm2.service.dto.EventMessageDTO;
import de.jaide.slm2.service.mapper.EventMessageMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link EventMessage}.
 */
@Service
@Transactional
public class EventMessageServiceImpl implements EventMessageService {

    private final Logger log = LoggerFactory.getLogger(EventMessageServiceImpl.class);

    private final EventMessageRepository eventMessageRepository;

    private final EventMessageMapper eventMessageMapper;

    public EventMessageServiceImpl(EventMessageRepository eventMessageRepository, EventMessageMapper eventMessageMapper) {
        this.eventMessageRepository = eventMessageRepository;
        this.eventMessageMapper = eventMessageMapper;
    }

    @Override
    public EventMessageDTO save(EventMessageDTO eventMessageDTO) {
        log.debug("Request to save EventMessage : {}", eventMessageDTO);
        EventMessage eventMessage = eventMessageMapper.toEntity(eventMessageDTO);
        eventMessage = eventMessageRepository.save(eventMessage);
        return eventMessageMapper.toDto(eventMessage);
    }

    @Override
    public Optional<EventMessageDTO> partialUpdate(EventMessageDTO eventMessageDTO) {
        log.debug("Request to partially update EventMessage : {}", eventMessageDTO);

        return eventMessageRepository
            .findById(eventMessageDTO.getId())
            .map(existingEventMessage -> {
                eventMessageMapper.partialUpdate(existingEventMessage, eventMessageDTO);

                return existingEventMessage;
            })
            .map(eventMessageRepository::save)
            .map(eventMessageMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EventMessageDTO> findAll(Pageable pageable) {
        log.debug("Request to get all EventMessages");
        return eventMessageRepository.findAll(pageable).map(eventMessageMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<EventMessageDTO> findOne(Long id) {
        log.debug("Request to get EventMessage : {}", id);
        return eventMessageRepository.findById(id).map(eventMessageMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete EventMessage : {}", id);
        eventMessageRepository.deleteById(id);
    }
}
