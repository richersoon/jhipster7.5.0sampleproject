package de.jaide.slm2.service.impl;

import de.jaide.slm2.domain.OrderApprovalNotification;
import de.jaide.slm2.repository.OrderApprovalNotificationRepository;
import de.jaide.slm2.service.OrderApprovalNotificationService;
import de.jaide.slm2.service.dto.OrderApprovalNotificationDTO;
import de.jaide.slm2.service.mapper.OrderApprovalNotificationMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link OrderApprovalNotification}.
 */
@Service
@Transactional
public class OrderApprovalNotificationServiceImpl implements OrderApprovalNotificationService {

    private final Logger log = LoggerFactory.getLogger(OrderApprovalNotificationServiceImpl.class);

    private final OrderApprovalNotificationRepository orderApprovalNotificationRepository;

    private final OrderApprovalNotificationMapper orderApprovalNotificationMapper;

    public OrderApprovalNotificationServiceImpl(
        OrderApprovalNotificationRepository orderApprovalNotificationRepository,
        OrderApprovalNotificationMapper orderApprovalNotificationMapper
    ) {
        this.orderApprovalNotificationRepository = orderApprovalNotificationRepository;
        this.orderApprovalNotificationMapper = orderApprovalNotificationMapper;
    }

    @Override
    public OrderApprovalNotificationDTO save(OrderApprovalNotificationDTO orderApprovalNotificationDTO) {
        log.debug("Request to save OrderApprovalNotification : {}", orderApprovalNotificationDTO);
        OrderApprovalNotification orderApprovalNotification = orderApprovalNotificationMapper.toEntity(orderApprovalNotificationDTO);
        orderApprovalNotification = orderApprovalNotificationRepository.save(orderApprovalNotification);
        return orderApprovalNotificationMapper.toDto(orderApprovalNotification);
    }

    @Override
    public Optional<OrderApprovalNotificationDTO> partialUpdate(OrderApprovalNotificationDTO orderApprovalNotificationDTO) {
        log.debug("Request to partially update OrderApprovalNotification : {}", orderApprovalNotificationDTO);

        return orderApprovalNotificationRepository
            .findById(orderApprovalNotificationDTO.getId())
            .map(existingOrderApprovalNotification -> {
                orderApprovalNotificationMapper.partialUpdate(existingOrderApprovalNotification, orderApprovalNotificationDTO);

                return existingOrderApprovalNotification;
            })
            .map(orderApprovalNotificationRepository::save)
            .map(orderApprovalNotificationMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<OrderApprovalNotificationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all OrderApprovalNotifications");
        return orderApprovalNotificationRepository.findAll(pageable).map(orderApprovalNotificationMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<OrderApprovalNotificationDTO> findOne(Long id) {
        log.debug("Request to get OrderApprovalNotification : {}", id);
        return orderApprovalNotificationRepository.findById(id).map(orderApprovalNotificationMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete OrderApprovalNotification : {}", id);
        orderApprovalNotificationRepository.deleteById(id);
    }
}
