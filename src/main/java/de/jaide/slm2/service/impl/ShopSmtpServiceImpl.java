package de.jaide.slm2.service.impl;

import de.jaide.slm2.domain.ShopSmtp;
import de.jaide.slm2.repository.ShopSmtpRepository;
import de.jaide.slm2.service.ShopSmtpService;
import de.jaide.slm2.service.dto.ShopSmtpDTO;
import de.jaide.slm2.service.mapper.ShopSmtpMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ShopSmtp}.
 */
@Service
@Transactional
public class ShopSmtpServiceImpl implements ShopSmtpService {

    private final Logger log = LoggerFactory.getLogger(ShopSmtpServiceImpl.class);

    private final ShopSmtpRepository shopSmtpRepository;

    private final ShopSmtpMapper shopSmtpMapper;

    public ShopSmtpServiceImpl(ShopSmtpRepository shopSmtpRepository, ShopSmtpMapper shopSmtpMapper) {
        this.shopSmtpRepository = shopSmtpRepository;
        this.shopSmtpMapper = shopSmtpMapper;
    }

    @Override
    public ShopSmtpDTO save(ShopSmtpDTO shopSmtpDTO) {
        log.debug("Request to save ShopSmtp : {}", shopSmtpDTO);
        ShopSmtp shopSmtp = shopSmtpMapper.toEntity(shopSmtpDTO);
        shopSmtp = shopSmtpRepository.save(shopSmtp);
        return shopSmtpMapper.toDto(shopSmtp);
    }

    @Override
    public Optional<ShopSmtpDTO> partialUpdate(ShopSmtpDTO shopSmtpDTO) {
        log.debug("Request to partially update ShopSmtp : {}", shopSmtpDTO);

        return shopSmtpRepository
            .findById(shopSmtpDTO.getId())
            .map(existingShopSmtp -> {
                shopSmtpMapper.partialUpdate(existingShopSmtp, shopSmtpDTO);

                return existingShopSmtp;
            })
            .map(shopSmtpRepository::save)
            .map(shopSmtpMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ShopSmtpDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ShopSmtps");
        return shopSmtpRepository.findAll(pageable).map(shopSmtpMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ShopSmtpDTO> findOne(Long id) {
        log.debug("Request to get ShopSmtp : {}", id);
        return shopSmtpRepository.findById(id).map(shopSmtpMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ShopSmtp : {}", id);
        shopSmtpRepository.deleteById(id);
    }
}
