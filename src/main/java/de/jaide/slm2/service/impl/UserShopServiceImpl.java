package de.jaide.slm2.service.impl;

import de.jaide.slm2.domain.UserShop;
import de.jaide.slm2.repository.UserShopRepository;
import de.jaide.slm2.service.UserShopService;
import de.jaide.slm2.service.dto.UserShopDTO;
import de.jaide.slm2.service.mapper.UserShopMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link UserShop}.
 */
@Service
@Transactional
public class UserShopServiceImpl implements UserShopService {

    private final Logger log = LoggerFactory.getLogger(UserShopServiceImpl.class);

    private final UserShopRepository userShopRepository;

    private final UserShopMapper userShopMapper;

    public UserShopServiceImpl(UserShopRepository userShopRepository, UserShopMapper userShopMapper) {
        this.userShopRepository = userShopRepository;
        this.userShopMapper = userShopMapper;
    }

    @Override
    public UserShopDTO save(UserShopDTO userShopDTO) {
        log.debug("Request to save UserShop : {}", userShopDTO);
        UserShop userShop = userShopMapper.toEntity(userShopDTO);
        userShop = userShopRepository.save(userShop);
        return userShopMapper.toDto(userShop);
    }

    @Override
    public Optional<UserShopDTO> partialUpdate(UserShopDTO userShopDTO) {
        log.debug("Request to partially update UserShop : {}", userShopDTO);

        return userShopRepository
            .findById(userShopDTO.getId())
            .map(existingUserShop -> {
                userShopMapper.partialUpdate(existingUserShop, userShopDTO);

                return existingUserShop;
            })
            .map(userShopRepository::save)
            .map(userShopMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserShopDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserShops");
        return userShopRepository.findAll(pageable).map(userShopMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<UserShopDTO> findOne(Long id) {
        log.debug("Request to get UserShop : {}", id);
        return userShopRepository.findById(id).map(userShopMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserShop : {}", id);
        userShopRepository.deleteById(id);
    }
}
