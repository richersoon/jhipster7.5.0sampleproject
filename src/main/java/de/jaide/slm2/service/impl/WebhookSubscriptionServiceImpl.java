package de.jaide.slm2.service.impl;

import de.jaide.slm2.domain.WebhookSubscription;
import de.jaide.slm2.repository.WebhookSubscriptionRepository;
import de.jaide.slm2.service.WebhookSubscriptionService;
import de.jaide.slm2.service.dto.WebhookSubscriptionDTO;
import de.jaide.slm2.service.mapper.WebhookSubscriptionMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link WebhookSubscription}.
 */
@Service
@Transactional
public class WebhookSubscriptionServiceImpl implements WebhookSubscriptionService {

    private final Logger log = LoggerFactory.getLogger(WebhookSubscriptionServiceImpl.class);

    private final WebhookSubscriptionRepository webhookSubscriptionRepository;

    private final WebhookSubscriptionMapper webhookSubscriptionMapper;

    public WebhookSubscriptionServiceImpl(
        WebhookSubscriptionRepository webhookSubscriptionRepository,
        WebhookSubscriptionMapper webhookSubscriptionMapper
    ) {
        this.webhookSubscriptionRepository = webhookSubscriptionRepository;
        this.webhookSubscriptionMapper = webhookSubscriptionMapper;
    }

    @Override
    public WebhookSubscriptionDTO save(WebhookSubscriptionDTO webhookSubscriptionDTO) {
        log.debug("Request to save WebhookSubscription : {}", webhookSubscriptionDTO);
        WebhookSubscription webhookSubscription = webhookSubscriptionMapper.toEntity(webhookSubscriptionDTO);
        webhookSubscription = webhookSubscriptionRepository.save(webhookSubscription);
        return webhookSubscriptionMapper.toDto(webhookSubscription);
    }

    @Override
    public Optional<WebhookSubscriptionDTO> partialUpdate(WebhookSubscriptionDTO webhookSubscriptionDTO) {
        log.debug("Request to partially update WebhookSubscription : {}", webhookSubscriptionDTO);

        return webhookSubscriptionRepository
            .findById(webhookSubscriptionDTO.getId())
            .map(existingWebhookSubscription -> {
                webhookSubscriptionMapper.partialUpdate(existingWebhookSubscription, webhookSubscriptionDTO);

                return existingWebhookSubscription;
            })
            .map(webhookSubscriptionRepository::save)
            .map(webhookSubscriptionMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<WebhookSubscriptionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all WebhookSubscriptions");
        return webhookSubscriptionRepository.findAll(pageable).map(webhookSubscriptionMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<WebhookSubscriptionDTO> findOne(Long id) {
        log.debug("Request to get WebhookSubscription : {}", id);
        return webhookSubscriptionRepository.findById(id).map(webhookSubscriptionMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete WebhookSubscription : {}", id);
        webhookSubscriptionRepository.deleteById(id);
    }
}
