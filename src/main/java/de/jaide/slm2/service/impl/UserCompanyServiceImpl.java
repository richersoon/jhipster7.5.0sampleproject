package de.jaide.slm2.service.impl;

import de.jaide.slm2.domain.UserCompany;
import de.jaide.slm2.repository.UserCompanyRepository;
import de.jaide.slm2.service.UserCompanyService;
import de.jaide.slm2.service.dto.UserCompanyDTO;
import de.jaide.slm2.service.mapper.UserCompanyMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link UserCompany}.
 */
@Service
@Transactional
public class UserCompanyServiceImpl implements UserCompanyService {

    private final Logger log = LoggerFactory.getLogger(UserCompanyServiceImpl.class);

    private final UserCompanyRepository userCompanyRepository;

    private final UserCompanyMapper userCompanyMapper;

    public UserCompanyServiceImpl(UserCompanyRepository userCompanyRepository, UserCompanyMapper userCompanyMapper) {
        this.userCompanyRepository = userCompanyRepository;
        this.userCompanyMapper = userCompanyMapper;
    }

    @Override
    public UserCompanyDTO save(UserCompanyDTO userCompanyDTO) {
        log.debug("Request to save UserCompany : {}", userCompanyDTO);
        UserCompany userCompany = userCompanyMapper.toEntity(userCompanyDTO);
        userCompany = userCompanyRepository.save(userCompany);
        return userCompanyMapper.toDto(userCompany);
    }

    @Override
    public Optional<UserCompanyDTO> partialUpdate(UserCompanyDTO userCompanyDTO) {
        log.debug("Request to partially update UserCompany : {}", userCompanyDTO);

        return userCompanyRepository
            .findById(userCompanyDTO.getId())
            .map(existingUserCompany -> {
                userCompanyMapper.partialUpdate(existingUserCompany, userCompanyDTO);

                return existingUserCompany;
            })
            .map(userCompanyRepository::save)
            .map(userCompanyMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserCompanyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all UserCompanies");
        return userCompanyRepository.findAll(pageable).map(userCompanyMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<UserCompanyDTO> findOne(Long id) {
        log.debug("Request to get UserCompany : {}", id);
        return userCompanyRepository.findById(id).map(userCompanyMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete UserCompany : {}", id);
        userCompanyRepository.deleteById(id);
    }
}
