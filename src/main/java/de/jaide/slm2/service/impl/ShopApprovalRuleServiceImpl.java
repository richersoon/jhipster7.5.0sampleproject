package de.jaide.slm2.service.impl;

import de.jaide.slm2.domain.ShopApprovalRule;
import de.jaide.slm2.repository.ShopApprovalRuleRepository;
import de.jaide.slm2.service.ShopApprovalRuleService;
import de.jaide.slm2.service.dto.ShopApprovalRuleDTO;
import de.jaide.slm2.service.mapper.ShopApprovalRuleMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ShopApprovalRule}.
 */
@Service
@Transactional
public class ShopApprovalRuleServiceImpl implements ShopApprovalRuleService {

    private final Logger log = LoggerFactory.getLogger(ShopApprovalRuleServiceImpl.class);

    private final ShopApprovalRuleRepository shopApprovalRuleRepository;

    private final ShopApprovalRuleMapper shopApprovalRuleMapper;

    public ShopApprovalRuleServiceImpl(
        ShopApprovalRuleRepository shopApprovalRuleRepository,
        ShopApprovalRuleMapper shopApprovalRuleMapper
    ) {
        this.shopApprovalRuleRepository = shopApprovalRuleRepository;
        this.shopApprovalRuleMapper = shopApprovalRuleMapper;
    }

    @Override
    public ShopApprovalRuleDTO save(ShopApprovalRuleDTO shopApprovalRuleDTO) {
        log.debug("Request to save ShopApprovalRule : {}", shopApprovalRuleDTO);
        ShopApprovalRule shopApprovalRule = shopApprovalRuleMapper.toEntity(shopApprovalRuleDTO);
        shopApprovalRule = shopApprovalRuleRepository.save(shopApprovalRule);
        return shopApprovalRuleMapper.toDto(shopApprovalRule);
    }

    @Override
    public Optional<ShopApprovalRuleDTO> partialUpdate(ShopApprovalRuleDTO shopApprovalRuleDTO) {
        log.debug("Request to partially update ShopApprovalRule : {}", shopApprovalRuleDTO);

        return shopApprovalRuleRepository
            .findById(shopApprovalRuleDTO.getId())
            .map(existingShopApprovalRule -> {
                shopApprovalRuleMapper.partialUpdate(existingShopApprovalRule, shopApprovalRuleDTO);

                return existingShopApprovalRule;
            })
            .map(shopApprovalRuleRepository::save)
            .map(shopApprovalRuleMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ShopApprovalRuleDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ShopApprovalRules");
        return shopApprovalRuleRepository.findAll(pageable).map(shopApprovalRuleMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ShopApprovalRuleDTO> findOne(Long id) {
        log.debug("Request to get ShopApprovalRule : {}", id);
        return shopApprovalRuleRepository.findById(id).map(shopApprovalRuleMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ShopApprovalRule : {}", id);
        shopApprovalRuleRepository.deleteById(id);
    }
}
