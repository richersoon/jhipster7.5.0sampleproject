package de.jaide.slm2.service.impl;

import de.jaide.slm2.domain.ShopEmailTemplate;
import de.jaide.slm2.repository.ShopEmailTemplateRepository;
import de.jaide.slm2.service.ShopEmailTemplateService;
import de.jaide.slm2.service.dto.ShopEmailTemplateDTO;
import de.jaide.slm2.service.mapper.ShopEmailTemplateMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ShopEmailTemplate}.
 */
@Service
@Transactional
public class ShopEmailTemplateServiceImpl implements ShopEmailTemplateService {

    private final Logger log = LoggerFactory.getLogger(ShopEmailTemplateServiceImpl.class);

    private final ShopEmailTemplateRepository shopEmailTemplateRepository;

    private final ShopEmailTemplateMapper shopEmailTemplateMapper;

    public ShopEmailTemplateServiceImpl(
        ShopEmailTemplateRepository shopEmailTemplateRepository,
        ShopEmailTemplateMapper shopEmailTemplateMapper
    ) {
        this.shopEmailTemplateRepository = shopEmailTemplateRepository;
        this.shopEmailTemplateMapper = shopEmailTemplateMapper;
    }

    @Override
    public ShopEmailTemplateDTO save(ShopEmailTemplateDTO shopEmailTemplateDTO) {
        log.debug("Request to save ShopEmailTemplate : {}", shopEmailTemplateDTO);
        ShopEmailTemplate shopEmailTemplate = shopEmailTemplateMapper.toEntity(shopEmailTemplateDTO);
        shopEmailTemplate = shopEmailTemplateRepository.save(shopEmailTemplate);
        return shopEmailTemplateMapper.toDto(shopEmailTemplate);
    }

    @Override
    public Optional<ShopEmailTemplateDTO> partialUpdate(ShopEmailTemplateDTO shopEmailTemplateDTO) {
        log.debug("Request to partially update ShopEmailTemplate : {}", shopEmailTemplateDTO);

        return shopEmailTemplateRepository
            .findById(shopEmailTemplateDTO.getId())
            .map(existingShopEmailTemplate -> {
                shopEmailTemplateMapper.partialUpdate(existingShopEmailTemplate, shopEmailTemplateDTO);

                return existingShopEmailTemplate;
            })
            .map(shopEmailTemplateRepository::save)
            .map(shopEmailTemplateMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ShopEmailTemplateDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ShopEmailTemplates");
        return shopEmailTemplateRepository.findAll(pageable).map(shopEmailTemplateMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ShopEmailTemplateDTO> findOne(Long id) {
        log.debug("Request to get ShopEmailTemplate : {}", id);
        return shopEmailTemplateRepository.findById(id).map(shopEmailTemplateMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ShopEmailTemplate : {}", id);
        shopEmailTemplateRepository.deleteById(id);
    }
}
