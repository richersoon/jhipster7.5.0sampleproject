package de.jaide.slm2.service.impl;

import de.jaide.slm2.domain.DefaultEmailTemplate;
import de.jaide.slm2.repository.DefaultEmailTemplateRepository;
import de.jaide.slm2.service.DefaultEmailTemplateService;
import de.jaide.slm2.service.dto.DefaultEmailTemplateDTO;
import de.jaide.slm2.service.mapper.DefaultEmailTemplateMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link DefaultEmailTemplate}.
 */
@Service
@Transactional
public class DefaultEmailTemplateServiceImpl implements DefaultEmailTemplateService {

    private final Logger log = LoggerFactory.getLogger(DefaultEmailTemplateServiceImpl.class);

    private final DefaultEmailTemplateRepository defaultEmailTemplateRepository;

    private final DefaultEmailTemplateMapper defaultEmailTemplateMapper;

    public DefaultEmailTemplateServiceImpl(
        DefaultEmailTemplateRepository defaultEmailTemplateRepository,
        DefaultEmailTemplateMapper defaultEmailTemplateMapper
    ) {
        this.defaultEmailTemplateRepository = defaultEmailTemplateRepository;
        this.defaultEmailTemplateMapper = defaultEmailTemplateMapper;
    }

    @Override
    public DefaultEmailTemplateDTO save(DefaultEmailTemplateDTO defaultEmailTemplateDTO) {
        log.debug("Request to save DefaultEmailTemplate : {}", defaultEmailTemplateDTO);
        DefaultEmailTemplate defaultEmailTemplate = defaultEmailTemplateMapper.toEntity(defaultEmailTemplateDTO);
        defaultEmailTemplate = defaultEmailTemplateRepository.save(defaultEmailTemplate);
        return defaultEmailTemplateMapper.toDto(defaultEmailTemplate);
    }

    @Override
    public Optional<DefaultEmailTemplateDTO> partialUpdate(DefaultEmailTemplateDTO defaultEmailTemplateDTO) {
        log.debug("Request to partially update DefaultEmailTemplate : {}", defaultEmailTemplateDTO);

        return defaultEmailTemplateRepository
            .findById(defaultEmailTemplateDTO.getId())
            .map(existingDefaultEmailTemplate -> {
                defaultEmailTemplateMapper.partialUpdate(existingDefaultEmailTemplate, defaultEmailTemplateDTO);

                return existingDefaultEmailTemplate;
            })
            .map(defaultEmailTemplateRepository::save)
            .map(defaultEmailTemplateMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<DefaultEmailTemplateDTO> findAll(Pageable pageable) {
        log.debug("Request to get all DefaultEmailTemplates");
        return defaultEmailTemplateRepository.findAll(pageable).map(defaultEmailTemplateMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DefaultEmailTemplateDTO> findOne(Long id) {
        log.debug("Request to get DefaultEmailTemplate : {}", id);
        return defaultEmailTemplateRepository.findById(id).map(defaultEmailTemplateMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete DefaultEmailTemplate : {}", id);
        defaultEmailTemplateRepository.deleteById(id);
    }
}
