package de.jaide.slm2.service.impl;

import de.jaide.slm2.domain.OrderNote;
import de.jaide.slm2.repository.OrderNoteRepository;
import de.jaide.slm2.service.OrderNoteService;
import de.jaide.slm2.service.dto.OrderNoteDTO;
import de.jaide.slm2.service.mapper.OrderNoteMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link OrderNote}.
 */
@Service
@Transactional
public class OrderNoteServiceImpl implements OrderNoteService {

    private final Logger log = LoggerFactory.getLogger(OrderNoteServiceImpl.class);

    private final OrderNoteRepository orderNoteRepository;

    private final OrderNoteMapper orderNoteMapper;

    public OrderNoteServiceImpl(OrderNoteRepository orderNoteRepository, OrderNoteMapper orderNoteMapper) {
        this.orderNoteRepository = orderNoteRepository;
        this.orderNoteMapper = orderNoteMapper;
    }

    @Override
    public OrderNoteDTO save(OrderNoteDTO orderNoteDTO) {
        log.debug("Request to save OrderNote : {}", orderNoteDTO);
        OrderNote orderNote = orderNoteMapper.toEntity(orderNoteDTO);
        orderNote = orderNoteRepository.save(orderNote);
        return orderNoteMapper.toDto(orderNote);
    }

    @Override
    public Optional<OrderNoteDTO> partialUpdate(OrderNoteDTO orderNoteDTO) {
        log.debug("Request to partially update OrderNote : {}", orderNoteDTO);

        return orderNoteRepository
            .findById(orderNoteDTO.getId())
            .map(existingOrderNote -> {
                orderNoteMapper.partialUpdate(existingOrderNote, orderNoteDTO);

                return existingOrderNote;
            })
            .map(orderNoteRepository::save)
            .map(orderNoteMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<OrderNoteDTO> findAll(Pageable pageable) {
        log.debug("Request to get all OrderNotes");
        return orderNoteRepository.findAll(pageable).map(orderNoteMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<OrderNoteDTO> findOne(Long id) {
        log.debug("Request to get OrderNote : {}", id);
        return orderNoteRepository.findById(id).map(orderNoteMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete OrderNote : {}", id);
        orderNoteRepository.deleteById(id);
    }
}
