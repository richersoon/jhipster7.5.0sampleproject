package de.jaide.slm2.service.impl;

import de.jaide.slm2.domain.Shop;
import de.jaide.slm2.repository.ShopRepository;
import de.jaide.slm2.service.ShopService;
import de.jaide.slm2.service.dto.ShopDTO;
import de.jaide.slm2.service.mapper.ShopMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Shop}.
 */
@Service
@Transactional
public class ShopServiceImpl implements ShopService {

    private final Logger log = LoggerFactory.getLogger(ShopServiceImpl.class);

    private final ShopRepository shopRepository;

    private final ShopMapper shopMapper;

    public ShopServiceImpl(ShopRepository shopRepository, ShopMapper shopMapper) {
        this.shopRepository = shopRepository;
        this.shopMapper = shopMapper;
    }

    @Override
    public ShopDTO save(ShopDTO shopDTO) {
        log.debug("Request to save Shop : {}", shopDTO);
        Shop shop = shopMapper.toEntity(shopDTO);
        shop = shopRepository.save(shop);
        return shopMapper.toDto(shop);
    }

    @Override
    public Optional<ShopDTO> partialUpdate(ShopDTO shopDTO) {
        log.debug("Request to partially update Shop : {}", shopDTO);

        return shopRepository
            .findById(shopDTO.getId())
            .map(existingShop -> {
                shopMapper.partialUpdate(existingShop, shopDTO);

                return existingShop;
            })
            .map(shopRepository::save)
            .map(shopMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ShopDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Shops");
        return shopRepository.findAll(pageable).map(shopMapper::toDto);
    }

    /**
     *  Get all the shops where ShopSmtp is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<ShopDTO> findAllWhereShopSmtpIsNull() {
        log.debug("Request to get all shops where ShopSmtp is null");
        return StreamSupport
            .stream(shopRepository.findAll().spliterator(), false)
            .filter(shop -> shop.getShopSmtp() == null)
            .map(shopMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ShopDTO> findOne(Long id) {
        log.debug("Request to get Shop : {}", id);
        return shopRepository.findById(id).map(shopMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Shop : {}", id);
        shopRepository.deleteById(id);
    }
}
