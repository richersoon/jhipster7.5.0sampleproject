package de.jaide.slm2.service;

import de.jaide.slm2.service.dto.ProductTypeDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link de.jaide.slm2.domain.ProductType}.
 */
public interface ProductTypeService {
    /**
     * Save a productType.
     *
     * @param productTypeDTO the entity to save.
     * @return the persisted entity.
     */
    ProductTypeDTO save(ProductTypeDTO productTypeDTO);

    /**
     * Partially updates a productType.
     *
     * @param productTypeDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ProductTypeDTO> partialUpdate(ProductTypeDTO productTypeDTO);

    /**
     * Get all the productTypes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProductTypeDTO> findAll(Pageable pageable);

    /**
     * Get the "id" productType.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProductTypeDTO> findOne(Long id);

    /**
     * Delete the "id" productType.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
