package de.jaide.slm2.service;

import de.jaide.slm2.service.dto.DefaultEmailTemplateDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link de.jaide.slm2.domain.DefaultEmailTemplate}.
 */
public interface DefaultEmailTemplateService {
    /**
     * Save a defaultEmailTemplate.
     *
     * @param defaultEmailTemplateDTO the entity to save.
     * @return the persisted entity.
     */
    DefaultEmailTemplateDTO save(DefaultEmailTemplateDTO defaultEmailTemplateDTO);

    /**
     * Partially updates a defaultEmailTemplate.
     *
     * @param defaultEmailTemplateDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<DefaultEmailTemplateDTO> partialUpdate(DefaultEmailTemplateDTO defaultEmailTemplateDTO);

    /**
     * Get all the defaultEmailTemplates.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<DefaultEmailTemplateDTO> findAll(Pageable pageable);

    /**
     * Get the "id" defaultEmailTemplate.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DefaultEmailTemplateDTO> findOne(Long id);

    /**
     * Delete the "id" defaultEmailTemplate.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
