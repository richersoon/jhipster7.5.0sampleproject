package de.jaide.slm2.service.dto;

import de.jaide.slm2.domain.enumeration.OrderApprovalStatus;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link de.jaide.slm2.domain.OrderApprovalNotification} entity.
 */
public class OrderApprovalNotificationDTO implements Serializable {

    private Long id;

    private OrderApprovalStatus status;

    private String browserIpCity;

    private String browserIpProvince;

    private String browserIpCountry;

    private String browserIpLatitude;

    private String browserIpLongitude;

    private String browserIpZip;

    private BigDecimal distanceKm;

    private Integer durationSeconds;

    private String note;

    private String createdBy;

    private Instant createdDate;

    private String lastModifiedBy;

    private Instant lastModifiedDate;

    private Long version;

    private OrderDTO order;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OrderApprovalStatus getStatus() {
        return status;
    }

    public void setStatus(OrderApprovalStatus status) {
        this.status = status;
    }

    public String getBrowserIpCity() {
        return browserIpCity;
    }

    public void setBrowserIpCity(String browserIpCity) {
        this.browserIpCity = browserIpCity;
    }

    public String getBrowserIpProvince() {
        return browserIpProvince;
    }

    public void setBrowserIpProvince(String browserIpProvince) {
        this.browserIpProvince = browserIpProvince;
    }

    public String getBrowserIpCountry() {
        return browserIpCountry;
    }

    public void setBrowserIpCountry(String browserIpCountry) {
        this.browserIpCountry = browserIpCountry;
    }

    public String getBrowserIpLatitude() {
        return browserIpLatitude;
    }

    public void setBrowserIpLatitude(String browserIpLatitude) {
        this.browserIpLatitude = browserIpLatitude;
    }

    public String getBrowserIpLongitude() {
        return browserIpLongitude;
    }

    public void setBrowserIpLongitude(String browserIpLongitude) {
        this.browserIpLongitude = browserIpLongitude;
    }

    public String getBrowserIpZip() {
        return browserIpZip;
    }

    public void setBrowserIpZip(String browserIpZip) {
        this.browserIpZip = browserIpZip;
    }

    public BigDecimal getDistanceKm() {
        return distanceKm;
    }

    public void setDistanceKm(BigDecimal distanceKm) {
        this.distanceKm = distanceKm;
    }

    public Integer getDurationSeconds() {
        return durationSeconds;
    }

    public void setDurationSeconds(Integer durationSeconds) {
        this.durationSeconds = durationSeconds;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public OrderDTO getOrder() {
        return order;
    }

    public void setOrder(OrderDTO order) {
        this.order = order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrderApprovalNotificationDTO)) {
            return false;
        }

        OrderApprovalNotificationDTO orderApprovalNotificationDTO = (OrderApprovalNotificationDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, orderApprovalNotificationDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OrderApprovalNotificationDTO{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            ", browserIpCity='" + getBrowserIpCity() + "'" +
            ", browserIpProvince='" + getBrowserIpProvince() + "'" +
            ", browserIpCountry='" + getBrowserIpCountry() + "'" +
            ", browserIpLatitude='" + getBrowserIpLatitude() + "'" +
            ", browserIpLongitude='" + getBrowserIpLongitude() + "'" +
            ", browserIpZip='" + getBrowserIpZip() + "'" +
            ", distanceKm=" + getDistanceKm() +
            ", durationSeconds=" + getDurationSeconds() +
            ", note='" + getNote() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", version=" + getVersion() +
            ", order=" + getOrder() +
            "}";
    }
}
