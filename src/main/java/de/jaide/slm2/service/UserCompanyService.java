package de.jaide.slm2.service;

import de.jaide.slm2.service.dto.UserCompanyDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link de.jaide.slm2.domain.UserCompany}.
 */
public interface UserCompanyService {
    /**
     * Save a userCompany.
     *
     * @param userCompanyDTO the entity to save.
     * @return the persisted entity.
     */
    UserCompanyDTO save(UserCompanyDTO userCompanyDTO);

    /**
     * Partially updates a userCompany.
     *
     * @param userCompanyDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<UserCompanyDTO> partialUpdate(UserCompanyDTO userCompanyDTO);

    /**
     * Get all the userCompanies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UserCompanyDTO> findAll(Pageable pageable);

    /**
     * Get the "id" userCompany.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UserCompanyDTO> findOne(Long id);

    /**
     * Delete the "id" userCompany.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
