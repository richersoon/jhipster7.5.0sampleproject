package de.jaide.slm2.service.mapper;

import de.jaide.slm2.domain.ShopEmailTemplate;
import de.jaide.slm2.service.dto.ShopEmailTemplateDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ShopEmailTemplate} and its DTO {@link ShopEmailTemplateDTO}.
 */
@Mapper(componentModel = "spring", uses = { DefaultEmailTemplateMapper.class, ShopMapper.class })
public interface ShopEmailTemplateMapper extends EntityMapper<ShopEmailTemplateDTO, ShopEmailTemplate> {
    @Mapping(target = "defaultEmailTemplate", source = "defaultEmailTemplate", qualifiedByName = "id")
    @Mapping(target = "shop", source = "shop", qualifiedByName = "name")
    ShopEmailTemplateDTO toDto(ShopEmailTemplate s);
}
