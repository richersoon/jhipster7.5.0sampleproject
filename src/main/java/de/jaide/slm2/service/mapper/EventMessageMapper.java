package de.jaide.slm2.service.mapper;

import de.jaide.slm2.domain.EventMessage;
import de.jaide.slm2.service.dto.EventMessageDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link EventMessage} and its DTO {@link EventMessageDTO}.
 */
@Mapper(componentModel = "spring", uses = { OrderMapper.class })
public interface EventMessageMapper extends EntityMapper<EventMessageDTO, EventMessage> {
    @Mapping(target = "order", source = "order", qualifiedByName = "orderNo")
    EventMessageDTO toDto(EventMessage s);
}
