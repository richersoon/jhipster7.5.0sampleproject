package de.jaide.slm2.service.mapper;

import de.jaide.slm2.domain.ShopApprovalRule;
import de.jaide.slm2.service.dto.ShopApprovalRuleDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ShopApprovalRule} and its DTO {@link ShopApprovalRuleDTO}.
 */
@Mapper(componentModel = "spring", uses = { ShopMapper.class })
public interface ShopApprovalRuleMapper extends EntityMapper<ShopApprovalRuleDTO, ShopApprovalRule> {
    @Mapping(target = "shop", source = "shop", qualifiedByName = "name")
    ShopApprovalRuleDTO toDto(ShopApprovalRule s);
}
