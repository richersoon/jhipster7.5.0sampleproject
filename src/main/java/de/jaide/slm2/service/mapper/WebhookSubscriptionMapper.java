package de.jaide.slm2.service.mapper;

import de.jaide.slm2.domain.WebhookSubscription;
import de.jaide.slm2.service.dto.WebhookSubscriptionDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link WebhookSubscription} and its DTO {@link WebhookSubscriptionDTO}.
 */
@Mapper(componentModel = "spring", uses = { ShopMapper.class })
public interface WebhookSubscriptionMapper extends EntityMapper<WebhookSubscriptionDTO, WebhookSubscription> {
    @Mapping(target = "shop", source = "shop", qualifiedByName = "name")
    WebhookSubscriptionDTO toDto(WebhookSubscription s);
}
