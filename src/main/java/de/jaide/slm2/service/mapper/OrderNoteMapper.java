package de.jaide.slm2.service.mapper;

import de.jaide.slm2.domain.OrderNote;
import de.jaide.slm2.service.dto.OrderNoteDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link OrderNote} and its DTO {@link OrderNoteDTO}.
 */
@Mapper(componentModel = "spring", uses = { OrderMapper.class })
public interface OrderNoteMapper extends EntityMapper<OrderNoteDTO, OrderNote> {
    @Mapping(target = "order", source = "order", qualifiedByName = "orderNo")
    OrderNoteDTO toDto(OrderNote s);
}
