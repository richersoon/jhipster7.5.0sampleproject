package de.jaide.slm2.service.mapper;

import de.jaide.slm2.domain.Order;
import de.jaide.slm2.service.dto.OrderDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Order} and its DTO {@link OrderDTO}.
 */
@Mapper(componentModel = "spring", uses = { ShopMapper.class })
public interface OrderMapper extends EntityMapper<OrderDTO, Order> {
    @Mapping(target = "shop", source = "shop", qualifiedByName = "name")
    OrderDTO toDto(Order s);

    @Named("orderNo")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "orderNo", source = "orderNo")
    OrderDTO toDtoOrderNo(Order order);
}
