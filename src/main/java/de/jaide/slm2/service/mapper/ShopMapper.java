package de.jaide.slm2.service.mapper;

import de.jaide.slm2.domain.Shop;
import de.jaide.slm2.service.dto.ShopDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Shop} and its DTO {@link ShopDTO}.
 */
@Mapper(componentModel = "spring", uses = { CompanyMapper.class, FileMapper.class })
public interface ShopMapper extends EntityMapper<ShopDTO, Shop> {
    @Mapping(target = "company", source = "company", qualifiedByName = "name")
    @Mapping(target = "logoFile", source = "logoFile", qualifiedByName = "name")
    ShopDTO toDto(Shop s);

    @Named("name")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    ShopDTO toDtoName(Shop shop);
}
