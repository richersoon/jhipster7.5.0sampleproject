package de.jaide.slm2.service.mapper;

import de.jaide.slm2.domain.ShopSmtp;
import de.jaide.slm2.service.dto.ShopSmtpDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ShopSmtp} and its DTO {@link ShopSmtpDTO}.
 */
@Mapper(componentModel = "spring", uses = { ShopMapper.class })
public interface ShopSmtpMapper extends EntityMapper<ShopSmtpDTO, ShopSmtp> {
    @Mapping(target = "shop", source = "shop", qualifiedByName = "name")
    ShopSmtpDTO toDto(ShopSmtp s);
}
