package de.jaide.slm2.service.mapper;

import de.jaide.slm2.domain.UserShop;
import de.jaide.slm2.service.dto.UserShopDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserShop} and its DTO {@link UserShopDTO}.
 */
@Mapper(componentModel = "spring", uses = { UserMapper.class, ShopMapper.class })
public interface UserShopMapper extends EntityMapper<UserShopDTO, UserShop> {
    @Mapping(target = "user", source = "user", qualifiedByName = "login")
    @Mapping(target = "shop", source = "shop", qualifiedByName = "name")
    UserShopDTO toDto(UserShop s);
}
