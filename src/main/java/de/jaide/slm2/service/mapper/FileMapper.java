package de.jaide.slm2.service.mapper;

import de.jaide.slm2.domain.File;
import de.jaide.slm2.service.dto.FileDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link File} and its DTO {@link FileDTO}.
 */
@Mapper(componentModel = "spring", uses = { UserMapper.class })
public interface FileMapper extends EntityMapper<FileDTO, File> {
    @Mapping(target = "uploaderUser", source = "uploaderUser", qualifiedByName = "login")
    FileDTO toDto(File s);

    @Named("name")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    FileDTO toDtoName(File file);
}
