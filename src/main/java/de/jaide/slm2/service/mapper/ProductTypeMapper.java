package de.jaide.slm2.service.mapper;

import de.jaide.slm2.domain.ProductType;
import de.jaide.slm2.service.dto.ProductTypeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProductType} and its DTO {@link ProductTypeDTO}.
 */
@Mapper(componentModel = "spring", uses = { ShopMapper.class })
public interface ProductTypeMapper extends EntityMapper<ProductTypeDTO, ProductType> {
    @Mapping(target = "shop", source = "shop", qualifiedByName = "name")
    ProductTypeDTO toDto(ProductType s);

    @Named("saneName")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "saneName", source = "saneName")
    ProductTypeDTO toDtoSaneName(ProductType productType);
}
