package de.jaide.slm2.service.mapper;

import de.jaide.slm2.domain.Company;
import de.jaide.slm2.service.dto.CompanyDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Company} and its DTO {@link CompanyDTO}.
 */
@Mapper(componentModel = "spring", uses = { FileMapper.class })
public interface CompanyMapper extends EntityMapper<CompanyDTO, Company> {
    @Mapping(target = "logoFile", source = "logoFile", qualifiedByName = "name")
    CompanyDTO toDto(Company s);

    @Named("name")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    CompanyDTO toDtoName(Company company);
}
