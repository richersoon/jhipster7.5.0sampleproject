package de.jaide.slm2.service.mapper;

import de.jaide.slm2.domain.UserCompany;
import de.jaide.slm2.service.dto.UserCompanyDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link UserCompany} and its DTO {@link UserCompanyDTO}.
 */
@Mapper(componentModel = "spring", uses = { UserMapper.class, CompanyMapper.class })
public interface UserCompanyMapper extends EntityMapper<UserCompanyDTO, UserCompany> {
    @Mapping(target = "user", source = "user", qualifiedByName = "login")
    @Mapping(target = "company", source = "company", qualifiedByName = "name")
    UserCompanyDTO toDto(UserCompany s);
}
