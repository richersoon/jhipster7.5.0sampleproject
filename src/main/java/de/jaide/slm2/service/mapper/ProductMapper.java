package de.jaide.slm2.service.mapper;

import de.jaide.slm2.domain.Product;
import de.jaide.slm2.service.dto.ProductDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Product} and its DTO {@link ProductDTO}.
 */
@Mapper(componentModel = "spring", uses = { ProductTypeMapper.class })
public interface ProductMapper extends EntityMapper<ProductDTO, Product> {
    @Mapping(target = "productType", source = "productType", qualifiedByName = "saneName")
    ProductDTO toDto(Product s);

    @Named("product")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "product", source = "product")
    ProductDTO toDtoProduct(Product product);
}
