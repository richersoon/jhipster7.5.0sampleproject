package de.jaide.slm2.service.mapper;

import de.jaide.slm2.domain.DefaultEmailTemplate;
import de.jaide.slm2.service.dto.DefaultEmailTemplateDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link DefaultEmailTemplate} and its DTO {@link DefaultEmailTemplateDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface DefaultEmailTemplateMapper extends EntityMapper<DefaultEmailTemplateDTO, DefaultEmailTemplate> {
    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    DefaultEmailTemplateDTO toDtoId(DefaultEmailTemplate defaultEmailTemplate);
}
