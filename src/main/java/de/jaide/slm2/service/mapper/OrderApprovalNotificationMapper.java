package de.jaide.slm2.service.mapper;

import de.jaide.slm2.domain.OrderApprovalNotification;
import de.jaide.slm2.service.dto.OrderApprovalNotificationDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link OrderApprovalNotification} and its DTO {@link OrderApprovalNotificationDTO}.
 */
@Mapper(componentModel = "spring", uses = { OrderMapper.class })
public interface OrderApprovalNotificationMapper extends EntityMapper<OrderApprovalNotificationDTO, OrderApprovalNotification> {
    @Mapping(target = "order", source = "order", qualifiedByName = "orderNo")
    OrderApprovalNotificationDTO toDto(OrderApprovalNotification s);
}
