package de.jaide.slm2.service.mapper;

import de.jaide.slm2.domain.OrderItem;
import de.jaide.slm2.service.dto.OrderItemDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link OrderItem} and its DTO {@link OrderItemDTO}.
 */
@Mapper(componentModel = "spring", uses = { ProductTypeMapper.class, OrderMapper.class, ProductMapper.class })
public interface OrderItemMapper extends EntityMapper<OrderItemDTO, OrderItem> {
    @Mapping(target = "productType", source = "productType", qualifiedByName = "saneName")
    @Mapping(target = "order", source = "order", qualifiedByName = "orderNo")
    @Mapping(target = "product", source = "product", qualifiedByName = "product")
    OrderItemDTO toDto(OrderItem s);
}
