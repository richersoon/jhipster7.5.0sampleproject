package de.jaide.slm2.service;

import de.jaide.slm2.service.dto.OrderApprovalNotificationDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link de.jaide.slm2.domain.OrderApprovalNotification}.
 */
public interface OrderApprovalNotificationService {
    /**
     * Save a orderApprovalNotification.
     *
     * @param orderApprovalNotificationDTO the entity to save.
     * @return the persisted entity.
     */
    OrderApprovalNotificationDTO save(OrderApprovalNotificationDTO orderApprovalNotificationDTO);

    /**
     * Partially updates a orderApprovalNotification.
     *
     * @param orderApprovalNotificationDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<OrderApprovalNotificationDTO> partialUpdate(OrderApprovalNotificationDTO orderApprovalNotificationDTO);

    /**
     * Get all the orderApprovalNotifications.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<OrderApprovalNotificationDTO> findAll(Pageable pageable);

    /**
     * Get the "id" orderApprovalNotification.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OrderApprovalNotificationDTO> findOne(Long id);

    /**
     * Delete the "id" orderApprovalNotification.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
