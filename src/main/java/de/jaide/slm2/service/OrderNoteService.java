package de.jaide.slm2.service;

import de.jaide.slm2.service.dto.OrderNoteDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link de.jaide.slm2.domain.OrderNote}.
 */
public interface OrderNoteService {
    /**
     * Save a orderNote.
     *
     * @param orderNoteDTO the entity to save.
     * @return the persisted entity.
     */
    OrderNoteDTO save(OrderNoteDTO orderNoteDTO);

    /**
     * Partially updates a orderNote.
     *
     * @param orderNoteDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<OrderNoteDTO> partialUpdate(OrderNoteDTO orderNoteDTO);

    /**
     * Get all the orderNotes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<OrderNoteDTO> findAll(Pageable pageable);

    /**
     * Get the "id" orderNote.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OrderNoteDTO> findOne(Long id);

    /**
     * Delete the "id" orderNote.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
