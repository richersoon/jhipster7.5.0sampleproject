package de.jaide.slm2.service;

import de.jaide.slm2.service.dto.ShopApprovalRuleDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link de.jaide.slm2.domain.ShopApprovalRule}.
 */
public interface ShopApprovalRuleService {
    /**
     * Save a shopApprovalRule.
     *
     * @param shopApprovalRuleDTO the entity to save.
     * @return the persisted entity.
     */
    ShopApprovalRuleDTO save(ShopApprovalRuleDTO shopApprovalRuleDTO);

    /**
     * Partially updates a shopApprovalRule.
     *
     * @param shopApprovalRuleDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ShopApprovalRuleDTO> partialUpdate(ShopApprovalRuleDTO shopApprovalRuleDTO);

    /**
     * Get all the shopApprovalRules.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ShopApprovalRuleDTO> findAll(Pageable pageable);

    /**
     * Get the "id" shopApprovalRule.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ShopApprovalRuleDTO> findOne(Long id);

    /**
     * Delete the "id" shopApprovalRule.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
