package de.jaide.slm2.service;

import de.jaide.slm2.service.dto.ShopSmtpDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link de.jaide.slm2.domain.ShopSmtp}.
 */
public interface ShopSmtpService {
    /**
     * Save a shopSmtp.
     *
     * @param shopSmtpDTO the entity to save.
     * @return the persisted entity.
     */
    ShopSmtpDTO save(ShopSmtpDTO shopSmtpDTO);

    /**
     * Partially updates a shopSmtp.
     *
     * @param shopSmtpDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ShopSmtpDTO> partialUpdate(ShopSmtpDTO shopSmtpDTO);

    /**
     * Get all the shopSmtps.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ShopSmtpDTO> findAll(Pageable pageable);

    /**
     * Get the "id" shopSmtp.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ShopSmtpDTO> findOne(Long id);

    /**
     * Delete the "id" shopSmtp.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
