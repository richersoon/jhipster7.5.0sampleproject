package de.jaide.slm2.service;

import de.jaide.slm2.service.dto.EventMessageDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link de.jaide.slm2.domain.EventMessage}.
 */
public interface EventMessageService {
    /**
     * Save a eventMessage.
     *
     * @param eventMessageDTO the entity to save.
     * @return the persisted entity.
     */
    EventMessageDTO save(EventMessageDTO eventMessageDTO);

    /**
     * Partially updates a eventMessage.
     *
     * @param eventMessageDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<EventMessageDTO> partialUpdate(EventMessageDTO eventMessageDTO);

    /**
     * Get all the eventMessages.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EventMessageDTO> findAll(Pageable pageable);

    /**
     * Get the "id" eventMessage.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EventMessageDTO> findOne(Long id);

    /**
     * Delete the "id" eventMessage.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
