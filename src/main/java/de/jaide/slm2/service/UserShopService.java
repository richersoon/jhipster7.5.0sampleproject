package de.jaide.slm2.service;

import de.jaide.slm2.service.dto.UserShopDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link de.jaide.slm2.domain.UserShop}.
 */
public interface UserShopService {
    /**
     * Save a userShop.
     *
     * @param userShopDTO the entity to save.
     * @return the persisted entity.
     */
    UserShopDTO save(UserShopDTO userShopDTO);

    /**
     * Partially updates a userShop.
     *
     * @param userShopDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<UserShopDTO> partialUpdate(UserShopDTO userShopDTO);

    /**
     * Get all the userShops.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<UserShopDTO> findAll(Pageable pageable);

    /**
     * Get the "id" userShop.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<UserShopDTO> findOne(Long id);

    /**
     * Delete the "id" userShop.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
