package de.jaide.slm2.repository;

import de.jaide.slm2.domain.OrderApprovalNotification;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the OrderApprovalNotification entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderApprovalNotificationRepository extends JpaRepository<OrderApprovalNotification, Long> {}
