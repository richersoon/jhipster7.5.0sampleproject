package de.jaide.slm2.repository;

import de.jaide.slm2.domain.UserShop;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the UserShop entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserShopRepository extends JpaRepository<UserShop, Long> {
    @Query("select userShop from UserShop userShop where userShop.user.login = ?#{principal.username}")
    List<UserShop> findByUserIsCurrentUser();
}
