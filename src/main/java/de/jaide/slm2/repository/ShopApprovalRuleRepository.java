package de.jaide.slm2.repository;

import de.jaide.slm2.domain.ShopApprovalRule;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ShopApprovalRule entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShopApprovalRuleRepository extends JpaRepository<ShopApprovalRule, Long> {}
