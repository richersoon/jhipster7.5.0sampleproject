package de.jaide.slm2.repository;

import de.jaide.slm2.domain.File;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the File entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FileRepository extends JpaRepository<File, Long> {
    @Query("select file from File file where file.uploaderUser.login = ?#{principal.username}")
    List<File> findByUploaderUserIsCurrentUser();
}
