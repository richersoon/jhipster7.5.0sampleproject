package de.jaide.slm2.repository;

import de.jaide.slm2.domain.DefaultEmailTemplate;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the DefaultEmailTemplate entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DefaultEmailTemplateRepository extends JpaRepository<DefaultEmailTemplate, Long> {}
