package de.jaide.slm2.repository;

import de.jaide.slm2.domain.WebhookSubscription;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the WebhookSubscription entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WebhookSubscriptionRepository extends JpaRepository<WebhookSubscription, Long> {}
