package de.jaide.slm2.repository;

import de.jaide.slm2.domain.EventMessage;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the EventMessage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EventMessageRepository extends JpaRepository<EventMessage, Long> {}
