package de.jaide.slm2.repository;

import de.jaide.slm2.domain.ShopEmailTemplate;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ShopEmailTemplate entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShopEmailTemplateRepository extends JpaRepository<ShopEmailTemplate, Long> {}
