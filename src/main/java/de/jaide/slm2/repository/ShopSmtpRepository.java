package de.jaide.slm2.repository;

import de.jaide.slm2.domain.ShopSmtp;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ShopSmtp entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShopSmtpRepository extends JpaRepository<ShopSmtp, Long> {}
