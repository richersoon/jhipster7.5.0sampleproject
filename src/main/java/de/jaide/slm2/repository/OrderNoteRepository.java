package de.jaide.slm2.repository;

import de.jaide.slm2.domain.OrderNote;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the OrderNote entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderNoteRepository extends JpaRepository<OrderNote, Long> {}
