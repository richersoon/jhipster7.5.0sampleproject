package de.jaide.slm2.repository;

import de.jaide.slm2.domain.UserCompany;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the UserCompany entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserCompanyRepository extends JpaRepository<UserCompany, Long> {
    @Query("select userCompany from UserCompany userCompany where userCompany.user.login = ?#{principal.username}")
    List<UserCompany> findByUserIsCurrentUser();
}
