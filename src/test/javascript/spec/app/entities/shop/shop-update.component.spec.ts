/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';
import { ToastPlugin } from 'bootstrap-vue';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import * as config from '@/shared/config/config';
import ShopUpdateComponent from '@/entities/shop/shop-update.vue';
import ShopClass from '@/entities/shop/shop-update.component';
import ShopService from '@/entities/shop/shop.service';

import UserShopService from '@/entities/user-shop/user-shop.service';

import ProductTypeService from '@/entities/product-type/product-type.service';

import OrderService from '@/entities/order/order.service';

import ShopEmailTemplateService from '@/entities/shop-email-template/shop-email-template.service';

import ShopApprovalRuleService from '@/entities/shop-approval-rule/shop-approval-rule.service';

import WebhookSubscriptionService from '@/entities/webhook-subscription/webhook-subscription.service';

import ShopSmtpService from '@/entities/shop-smtp/shop-smtp.service';

import CompanyService from '@/entities/company/company.service';

import FileService from '@/entities/file/file.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.use(ToastPlugin);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('Shop Management Update Component', () => {
    let wrapper: Wrapper<ShopClass>;
    let comp: ShopClass;
    let shopServiceStub: SinonStubbedInstance<ShopService>;

    beforeEach(() => {
      shopServiceStub = sinon.createStubInstance<ShopService>(ShopService);

      wrapper = shallowMount<ShopClass>(ShopUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          shopService: () => shopServiceStub,
          alertService: () => new AlertService(),

          userShopService: () =>
            sinon.createStubInstance<UserShopService>(UserShopService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          productTypeService: () =>
            sinon.createStubInstance<ProductTypeService>(ProductTypeService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          orderService: () =>
            sinon.createStubInstance<OrderService>(OrderService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          shopEmailTemplateService: () =>
            sinon.createStubInstance<ShopEmailTemplateService>(ShopEmailTemplateService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          shopApprovalRuleService: () =>
            sinon.createStubInstance<ShopApprovalRuleService>(ShopApprovalRuleService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          webhookSubscriptionService: () =>
            sinon.createStubInstance<WebhookSubscriptionService>(WebhookSubscriptionService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          shopSmtpService: () =>
            sinon.createStubInstance<ShopSmtpService>(ShopSmtpService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          companyService: () =>
            sinon.createStubInstance<CompanyService>(CompanyService, {
              retrieve: sinon.stub().resolves({}),
            } as any),

          fileService: () =>
            sinon.createStubInstance<FileService>(FileService, {
              retrieve: sinon.stub().resolves({}),
            } as any),
        },
      });
      comp = wrapper.vm;
    });

    describe('load', () => {
      it('Should convert date from string', () => {
        // GIVEN
        const date = new Date('2019-10-15T11:42:02Z');

        // WHEN
        const convertedDate = comp.convertDateTimeFromServer(date);

        // THEN
        expect(convertedDate).toEqual(dayjs(date).format(DATE_TIME_LONG_FORMAT));
      });

      it('Should not convert date if date is not present', () => {
        expect(comp.convertDateTimeFromServer(null)).toBeNull();
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.shop = entity;
        shopServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(shopServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.shop = entity;
        shopServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(shopServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundShop = { id: 123 };
        shopServiceStub.find.resolves(foundShop);
        shopServiceStub.retrieve.resolves([foundShop]);

        // WHEN
        comp.beforeRouteEnter({ params: { shopId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.shop).toBe(foundShop);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
