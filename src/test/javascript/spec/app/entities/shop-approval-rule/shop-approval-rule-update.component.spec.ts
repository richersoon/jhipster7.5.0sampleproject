/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';
import { ToastPlugin } from 'bootstrap-vue';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import * as config from '@/shared/config/config';
import ShopApprovalRuleUpdateComponent from '@/entities/shop-approval-rule/shop-approval-rule-update.vue';
import ShopApprovalRuleClass from '@/entities/shop-approval-rule/shop-approval-rule-update.component';
import ShopApprovalRuleService from '@/entities/shop-approval-rule/shop-approval-rule.service';

import ShopService from '@/entities/shop/shop.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.use(ToastPlugin);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('ShopApprovalRule Management Update Component', () => {
    let wrapper: Wrapper<ShopApprovalRuleClass>;
    let comp: ShopApprovalRuleClass;
    let shopApprovalRuleServiceStub: SinonStubbedInstance<ShopApprovalRuleService>;

    beforeEach(() => {
      shopApprovalRuleServiceStub = sinon.createStubInstance<ShopApprovalRuleService>(ShopApprovalRuleService);

      wrapper = shallowMount<ShopApprovalRuleClass>(ShopApprovalRuleUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          shopApprovalRuleService: () => shopApprovalRuleServiceStub,
          alertService: () => new AlertService(),

          shopService: () =>
            sinon.createStubInstance<ShopService>(ShopService, {
              retrieve: sinon.stub().resolves({}),
            } as any),
        },
      });
      comp = wrapper.vm;
    });

    describe('load', () => {
      it('Should convert date from string', () => {
        // GIVEN
        const date = new Date('2019-10-15T11:42:02Z');

        // WHEN
        const convertedDate = comp.convertDateTimeFromServer(date);

        // THEN
        expect(convertedDate).toEqual(dayjs(date).format(DATE_TIME_LONG_FORMAT));
      });

      it('Should not convert date if date is not present', () => {
        expect(comp.convertDateTimeFromServer(null)).toBeNull();
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.shopApprovalRule = entity;
        shopApprovalRuleServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(shopApprovalRuleServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.shopApprovalRule = entity;
        shopApprovalRuleServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(shopApprovalRuleServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundShopApprovalRule = { id: 123 };
        shopApprovalRuleServiceStub.find.resolves(foundShopApprovalRule);
        shopApprovalRuleServiceStub.retrieve.resolves([foundShopApprovalRule]);

        // WHEN
        comp.beforeRouteEnter({ params: { shopApprovalRuleId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.shopApprovalRule).toBe(foundShopApprovalRule);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
