/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import ShopApprovalRuleDetailComponent from '@/entities/shop-approval-rule/shop-approval-rule-details.vue';
import ShopApprovalRuleClass from '@/entities/shop-approval-rule/shop-approval-rule-details.component';
import ShopApprovalRuleService from '@/entities/shop-approval-rule/shop-approval-rule.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('ShopApprovalRule Management Detail Component', () => {
    let wrapper: Wrapper<ShopApprovalRuleClass>;
    let comp: ShopApprovalRuleClass;
    let shopApprovalRuleServiceStub: SinonStubbedInstance<ShopApprovalRuleService>;

    beforeEach(() => {
      shopApprovalRuleServiceStub = sinon.createStubInstance<ShopApprovalRuleService>(ShopApprovalRuleService);

      wrapper = shallowMount<ShopApprovalRuleClass>(ShopApprovalRuleDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { shopApprovalRuleService: () => shopApprovalRuleServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundShopApprovalRule = { id: 123 };
        shopApprovalRuleServiceStub.find.resolves(foundShopApprovalRule);

        // WHEN
        comp.retrieveShopApprovalRule(123);
        await comp.$nextTick();

        // THEN
        expect(comp.shopApprovalRule).toBe(foundShopApprovalRule);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundShopApprovalRule = { id: 123 };
        shopApprovalRuleServiceStub.find.resolves(foundShopApprovalRule);

        // WHEN
        comp.beforeRouteEnter({ params: { shopApprovalRuleId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.shopApprovalRule).toBe(foundShopApprovalRule);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
