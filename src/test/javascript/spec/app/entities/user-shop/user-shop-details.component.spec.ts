/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import UserShopDetailComponent from '@/entities/user-shop/user-shop-details.vue';
import UserShopClass from '@/entities/user-shop/user-shop-details.component';
import UserShopService from '@/entities/user-shop/user-shop.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('UserShop Management Detail Component', () => {
    let wrapper: Wrapper<UserShopClass>;
    let comp: UserShopClass;
    let userShopServiceStub: SinonStubbedInstance<UserShopService>;

    beforeEach(() => {
      userShopServiceStub = sinon.createStubInstance<UserShopService>(UserShopService);

      wrapper = shallowMount<UserShopClass>(UserShopDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { userShopService: () => userShopServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundUserShop = { id: 123 };
        userShopServiceStub.find.resolves(foundUserShop);

        // WHEN
        comp.retrieveUserShop(123);
        await comp.$nextTick();

        // THEN
        expect(comp.userShop).toBe(foundUserShop);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundUserShop = { id: 123 };
        userShopServiceStub.find.resolves(foundUserShop);

        // WHEN
        comp.beforeRouteEnter({ params: { userShopId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.userShop).toBe(foundUserShop);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
