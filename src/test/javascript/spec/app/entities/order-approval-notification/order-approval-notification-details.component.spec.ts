/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import OrderApprovalNotificationDetailComponent from '@/entities/order-approval-notification/order-approval-notification-details.vue';
import OrderApprovalNotificationClass from '@/entities/order-approval-notification/order-approval-notification-details.component';
import OrderApprovalNotificationService from '@/entities/order-approval-notification/order-approval-notification.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('OrderApprovalNotification Management Detail Component', () => {
    let wrapper: Wrapper<OrderApprovalNotificationClass>;
    let comp: OrderApprovalNotificationClass;
    let orderApprovalNotificationServiceStub: SinonStubbedInstance<OrderApprovalNotificationService>;

    beforeEach(() => {
      orderApprovalNotificationServiceStub = sinon.createStubInstance<OrderApprovalNotificationService>(OrderApprovalNotificationService);

      wrapper = shallowMount<OrderApprovalNotificationClass>(OrderApprovalNotificationDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { orderApprovalNotificationService: () => orderApprovalNotificationServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundOrderApprovalNotification = { id: 123 };
        orderApprovalNotificationServiceStub.find.resolves(foundOrderApprovalNotification);

        // WHEN
        comp.retrieveOrderApprovalNotification(123);
        await comp.$nextTick();

        // THEN
        expect(comp.orderApprovalNotification).toBe(foundOrderApprovalNotification);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundOrderApprovalNotification = { id: 123 };
        orderApprovalNotificationServiceStub.find.resolves(foundOrderApprovalNotification);

        // WHEN
        comp.beforeRouteEnter({ params: { orderApprovalNotificationId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.orderApprovalNotification).toBe(foundOrderApprovalNotification);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
