/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';
import { ToastPlugin } from 'bootstrap-vue';

import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import * as config from '@/shared/config/config';
import OrderApprovalNotificationUpdateComponent from '@/entities/order-approval-notification/order-approval-notification-update.vue';
import OrderApprovalNotificationClass from '@/entities/order-approval-notification/order-approval-notification-update.component';
import OrderApprovalNotificationService from '@/entities/order-approval-notification/order-approval-notification.service';

import OrderService from '@/entities/order/order.service';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.use(ToastPlugin);
localVue.component('font-awesome-icon', {});
localVue.component('b-input-group', {});
localVue.component('b-input-group-prepend', {});
localVue.component('b-form-datepicker', {});
localVue.component('b-form-input', {});

describe('Component Tests', () => {
  describe('OrderApprovalNotification Management Update Component', () => {
    let wrapper: Wrapper<OrderApprovalNotificationClass>;
    let comp: OrderApprovalNotificationClass;
    let orderApprovalNotificationServiceStub: SinonStubbedInstance<OrderApprovalNotificationService>;

    beforeEach(() => {
      orderApprovalNotificationServiceStub = sinon.createStubInstance<OrderApprovalNotificationService>(OrderApprovalNotificationService);

      wrapper = shallowMount<OrderApprovalNotificationClass>(OrderApprovalNotificationUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          orderApprovalNotificationService: () => orderApprovalNotificationServiceStub,
          alertService: () => new AlertService(),

          orderService: () =>
            sinon.createStubInstance<OrderService>(OrderService, {
              retrieve: sinon.stub().resolves({}),
            } as any),
        },
      });
      comp = wrapper.vm;
    });

    describe('load', () => {
      it('Should convert date from string', () => {
        // GIVEN
        const date = new Date('2019-10-15T11:42:02Z');

        // WHEN
        const convertedDate = comp.convertDateTimeFromServer(date);

        // THEN
        expect(convertedDate).toEqual(dayjs(date).format(DATE_TIME_LONG_FORMAT));
      });

      it('Should not convert date if date is not present', () => {
        expect(comp.convertDateTimeFromServer(null)).toBeNull();
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: 123 };
        comp.orderApprovalNotification = entity;
        orderApprovalNotificationServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(orderApprovalNotificationServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.orderApprovalNotification = entity;
        orderApprovalNotificationServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(orderApprovalNotificationServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundOrderApprovalNotification = { id: 123 };
        orderApprovalNotificationServiceStub.find.resolves(foundOrderApprovalNotification);
        orderApprovalNotificationServiceStub.retrieve.resolves([foundOrderApprovalNotification]);

        // WHEN
        comp.beforeRouteEnter({ params: { orderApprovalNotificationId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.orderApprovalNotification).toBe(foundOrderApprovalNotification);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
