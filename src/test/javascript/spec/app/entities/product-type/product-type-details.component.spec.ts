/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import ProductTypeDetailComponent from '@/entities/product-type/product-type-details.vue';
import ProductTypeClass from '@/entities/product-type/product-type-details.component';
import ProductTypeService from '@/entities/product-type/product-type.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('ProductType Management Detail Component', () => {
    let wrapper: Wrapper<ProductTypeClass>;
    let comp: ProductTypeClass;
    let productTypeServiceStub: SinonStubbedInstance<ProductTypeService>;

    beforeEach(() => {
      productTypeServiceStub = sinon.createStubInstance<ProductTypeService>(ProductTypeService);

      wrapper = shallowMount<ProductTypeClass>(ProductTypeDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { productTypeService: () => productTypeServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundProductType = { id: 123 };
        productTypeServiceStub.find.resolves(foundProductType);

        // WHEN
        comp.retrieveProductType(123);
        await comp.$nextTick();

        // THEN
        expect(comp.productType).toBe(foundProductType);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundProductType = { id: 123 };
        productTypeServiceStub.find.resolves(foundProductType);

        // WHEN
        comp.beforeRouteEnter({ params: { productTypeId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.productType).toBe(foundProductType);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
