/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import EventMessageDetailComponent from '@/entities/event-message/event-message-details.vue';
import EventMessageClass from '@/entities/event-message/event-message-details.component';
import EventMessageService from '@/entities/event-message/event-message.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('EventMessage Management Detail Component', () => {
    let wrapper: Wrapper<EventMessageClass>;
    let comp: EventMessageClass;
    let eventMessageServiceStub: SinonStubbedInstance<EventMessageService>;

    beforeEach(() => {
      eventMessageServiceStub = sinon.createStubInstance<EventMessageService>(EventMessageService);

      wrapper = shallowMount<EventMessageClass>(EventMessageDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { eventMessageService: () => eventMessageServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundEventMessage = { id: 123 };
        eventMessageServiceStub.find.resolves(foundEventMessage);

        // WHEN
        comp.retrieveEventMessage(123);
        await comp.$nextTick();

        // THEN
        expect(comp.eventMessage).toBe(foundEventMessage);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundEventMessage = { id: 123 };
        eventMessageServiceStub.find.resolves(foundEventMessage);

        // WHEN
        comp.beforeRouteEnter({ params: { eventMessageId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.eventMessage).toBe(foundEventMessage);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
