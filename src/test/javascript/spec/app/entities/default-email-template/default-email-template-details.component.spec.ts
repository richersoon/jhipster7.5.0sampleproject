/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import DefaultEmailTemplateDetailComponent from '@/entities/default-email-template/default-email-template-details.vue';
import DefaultEmailTemplateClass from '@/entities/default-email-template/default-email-template-details.component';
import DefaultEmailTemplateService from '@/entities/default-email-template/default-email-template.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('DefaultEmailTemplate Management Detail Component', () => {
    let wrapper: Wrapper<DefaultEmailTemplateClass>;
    let comp: DefaultEmailTemplateClass;
    let defaultEmailTemplateServiceStub: SinonStubbedInstance<DefaultEmailTemplateService>;

    beforeEach(() => {
      defaultEmailTemplateServiceStub = sinon.createStubInstance<DefaultEmailTemplateService>(DefaultEmailTemplateService);

      wrapper = shallowMount<DefaultEmailTemplateClass>(DefaultEmailTemplateDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { defaultEmailTemplateService: () => defaultEmailTemplateServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundDefaultEmailTemplate = { id: 123 };
        defaultEmailTemplateServiceStub.find.resolves(foundDefaultEmailTemplate);

        // WHEN
        comp.retrieveDefaultEmailTemplate(123);
        await comp.$nextTick();

        // THEN
        expect(comp.defaultEmailTemplate).toBe(foundDefaultEmailTemplate);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundDefaultEmailTemplate = { id: 123 };
        defaultEmailTemplateServiceStub.find.resolves(foundDefaultEmailTemplate);

        // WHEN
        comp.beforeRouteEnter({ params: { defaultEmailTemplateId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.defaultEmailTemplate).toBe(foundDefaultEmailTemplate);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
