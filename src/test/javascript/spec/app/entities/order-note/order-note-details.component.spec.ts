/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import OrderNoteDetailComponent from '@/entities/order-note/order-note-details.vue';
import OrderNoteClass from '@/entities/order-note/order-note-details.component';
import OrderNoteService from '@/entities/order-note/order-note.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('OrderNote Management Detail Component', () => {
    let wrapper: Wrapper<OrderNoteClass>;
    let comp: OrderNoteClass;
    let orderNoteServiceStub: SinonStubbedInstance<OrderNoteService>;

    beforeEach(() => {
      orderNoteServiceStub = sinon.createStubInstance<OrderNoteService>(OrderNoteService);

      wrapper = shallowMount<OrderNoteClass>(OrderNoteDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { orderNoteService: () => orderNoteServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundOrderNote = { id: 123 };
        orderNoteServiceStub.find.resolves(foundOrderNote);

        // WHEN
        comp.retrieveOrderNote(123);
        await comp.$nextTick();

        // THEN
        expect(comp.orderNote).toBe(foundOrderNote);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundOrderNote = { id: 123 };
        orderNoteServiceStub.find.resolves(foundOrderNote);

        // WHEN
        comp.beforeRouteEnter({ params: { orderNoteId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.orderNote).toBe(foundOrderNote);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
