/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import UserCompanyDetailComponent from '@/entities/user-company/user-company-details.vue';
import UserCompanyClass from '@/entities/user-company/user-company-details.component';
import UserCompanyService from '@/entities/user-company/user-company.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('UserCompany Management Detail Component', () => {
    let wrapper: Wrapper<UserCompanyClass>;
    let comp: UserCompanyClass;
    let userCompanyServiceStub: SinonStubbedInstance<UserCompanyService>;

    beforeEach(() => {
      userCompanyServiceStub = sinon.createStubInstance<UserCompanyService>(UserCompanyService);

      wrapper = shallowMount<UserCompanyClass>(UserCompanyDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { userCompanyService: () => userCompanyServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundUserCompany = { id: 123 };
        userCompanyServiceStub.find.resolves(foundUserCompany);

        // WHEN
        comp.retrieveUserCompany(123);
        await comp.$nextTick();

        // THEN
        expect(comp.userCompany).toBe(foundUserCompany);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundUserCompany = { id: 123 };
        userCompanyServiceStub.find.resolves(foundUserCompany);

        // WHEN
        comp.beforeRouteEnter({ params: { userCompanyId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.userCompany).toBe(foundUserCompany);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
