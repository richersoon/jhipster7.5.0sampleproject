/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import VueRouter from 'vue-router';

import * as config from '@/shared/config/config';
import ShopSmtpDetailComponent from '@/entities/shop-smtp/shop-smtp-details.vue';
import ShopSmtpClass from '@/entities/shop-smtp/shop-smtp-details.component';
import ShopSmtpService from '@/entities/shop-smtp/shop-smtp.service';
import router from '@/router';
import AlertService from '@/shared/alert/alert.service';

const localVue = createLocalVue();
localVue.use(VueRouter);

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('ShopSmtp Management Detail Component', () => {
    let wrapper: Wrapper<ShopSmtpClass>;
    let comp: ShopSmtpClass;
    let shopSmtpServiceStub: SinonStubbedInstance<ShopSmtpService>;

    beforeEach(() => {
      shopSmtpServiceStub = sinon.createStubInstance<ShopSmtpService>(ShopSmtpService);

      wrapper = shallowMount<ShopSmtpClass>(ShopSmtpDetailComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: { shopSmtpService: () => shopSmtpServiceStub, alertService: () => new AlertService() },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundShopSmtp = { id: 123 };
        shopSmtpServiceStub.find.resolves(foundShopSmtp);

        // WHEN
        comp.retrieveShopSmtp(123);
        await comp.$nextTick();

        // THEN
        expect(comp.shopSmtp).toBe(foundShopSmtp);
      });
    });

    describe('Before route enter', () => {
      it('Should retrieve data', async () => {
        // GIVEN
        const foundShopSmtp = { id: 123 };
        shopSmtpServiceStub.find.resolves(foundShopSmtp);

        // WHEN
        comp.beforeRouteEnter({ params: { shopSmtpId: 123 } }, null, cb => cb(comp));
        await comp.$nextTick();

        // THEN
        expect(comp.shopSmtp).toBe(foundShopSmtp);
      });
    });

    describe('Previous state', () => {
      it('Should go previous state', async () => {
        comp.previousState();
        await comp.$nextTick();

        expect(comp.$router.currentRoute.fullPath).toContain('/');
      });
    });
  });
});
