package de.jaide.slm2.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import de.jaide.slm2.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class OrderApprovalNotificationDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrderApprovalNotificationDTO.class);
        OrderApprovalNotificationDTO orderApprovalNotificationDTO1 = new OrderApprovalNotificationDTO();
        orderApprovalNotificationDTO1.setId(1L);
        OrderApprovalNotificationDTO orderApprovalNotificationDTO2 = new OrderApprovalNotificationDTO();
        assertThat(orderApprovalNotificationDTO1).isNotEqualTo(orderApprovalNotificationDTO2);
        orderApprovalNotificationDTO2.setId(orderApprovalNotificationDTO1.getId());
        assertThat(orderApprovalNotificationDTO1).isEqualTo(orderApprovalNotificationDTO2);
        orderApprovalNotificationDTO2.setId(2L);
        assertThat(orderApprovalNotificationDTO1).isNotEqualTo(orderApprovalNotificationDTO2);
        orderApprovalNotificationDTO1.setId(null);
        assertThat(orderApprovalNotificationDTO1).isNotEqualTo(orderApprovalNotificationDTO2);
    }
}
