package de.jaide.slm2.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import de.jaide.slm2.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DefaultEmailTemplateDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(DefaultEmailTemplateDTO.class);
        DefaultEmailTemplateDTO defaultEmailTemplateDTO1 = new DefaultEmailTemplateDTO();
        defaultEmailTemplateDTO1.setId(1L);
        DefaultEmailTemplateDTO defaultEmailTemplateDTO2 = new DefaultEmailTemplateDTO();
        assertThat(defaultEmailTemplateDTO1).isNotEqualTo(defaultEmailTemplateDTO2);
        defaultEmailTemplateDTO2.setId(defaultEmailTemplateDTO1.getId());
        assertThat(defaultEmailTemplateDTO1).isEqualTo(defaultEmailTemplateDTO2);
        defaultEmailTemplateDTO2.setId(2L);
        assertThat(defaultEmailTemplateDTO1).isNotEqualTo(defaultEmailTemplateDTO2);
        defaultEmailTemplateDTO1.setId(null);
        assertThat(defaultEmailTemplateDTO1).isNotEqualTo(defaultEmailTemplateDTO2);
    }
}
