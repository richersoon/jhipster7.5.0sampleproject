package de.jaide.slm2.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import de.jaide.slm2.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ShopEmailTemplateDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShopEmailTemplateDTO.class);
        ShopEmailTemplateDTO shopEmailTemplateDTO1 = new ShopEmailTemplateDTO();
        shopEmailTemplateDTO1.setId(1L);
        ShopEmailTemplateDTO shopEmailTemplateDTO2 = new ShopEmailTemplateDTO();
        assertThat(shopEmailTemplateDTO1).isNotEqualTo(shopEmailTemplateDTO2);
        shopEmailTemplateDTO2.setId(shopEmailTemplateDTO1.getId());
        assertThat(shopEmailTemplateDTO1).isEqualTo(shopEmailTemplateDTO2);
        shopEmailTemplateDTO2.setId(2L);
        assertThat(shopEmailTemplateDTO1).isNotEqualTo(shopEmailTemplateDTO2);
        shopEmailTemplateDTO1.setId(null);
        assertThat(shopEmailTemplateDTO1).isNotEqualTo(shopEmailTemplateDTO2);
    }
}
