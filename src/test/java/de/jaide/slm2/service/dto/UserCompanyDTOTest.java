package de.jaide.slm2.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import de.jaide.slm2.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class UserCompanyDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserCompanyDTO.class);
        UserCompanyDTO userCompanyDTO1 = new UserCompanyDTO();
        userCompanyDTO1.setId(1L);
        UserCompanyDTO userCompanyDTO2 = new UserCompanyDTO();
        assertThat(userCompanyDTO1).isNotEqualTo(userCompanyDTO2);
        userCompanyDTO2.setId(userCompanyDTO1.getId());
        assertThat(userCompanyDTO1).isEqualTo(userCompanyDTO2);
        userCompanyDTO2.setId(2L);
        assertThat(userCompanyDTO1).isNotEqualTo(userCompanyDTO2);
        userCompanyDTO1.setId(null);
        assertThat(userCompanyDTO1).isNotEqualTo(userCompanyDTO2);
    }
}
