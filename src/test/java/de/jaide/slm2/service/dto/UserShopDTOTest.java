package de.jaide.slm2.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import de.jaide.slm2.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class UserShopDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserShopDTO.class);
        UserShopDTO userShopDTO1 = new UserShopDTO();
        userShopDTO1.setId(1L);
        UserShopDTO userShopDTO2 = new UserShopDTO();
        assertThat(userShopDTO1).isNotEqualTo(userShopDTO2);
        userShopDTO2.setId(userShopDTO1.getId());
        assertThat(userShopDTO1).isEqualTo(userShopDTO2);
        userShopDTO2.setId(2L);
        assertThat(userShopDTO1).isNotEqualTo(userShopDTO2);
        userShopDTO1.setId(null);
        assertThat(userShopDTO1).isNotEqualTo(userShopDTO2);
    }
}
