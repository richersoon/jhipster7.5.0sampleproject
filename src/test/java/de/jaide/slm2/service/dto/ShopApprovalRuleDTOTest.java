package de.jaide.slm2.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import de.jaide.slm2.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ShopApprovalRuleDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShopApprovalRuleDTO.class);
        ShopApprovalRuleDTO shopApprovalRuleDTO1 = new ShopApprovalRuleDTO();
        shopApprovalRuleDTO1.setId(1L);
        ShopApprovalRuleDTO shopApprovalRuleDTO2 = new ShopApprovalRuleDTO();
        assertThat(shopApprovalRuleDTO1).isNotEqualTo(shopApprovalRuleDTO2);
        shopApprovalRuleDTO2.setId(shopApprovalRuleDTO1.getId());
        assertThat(shopApprovalRuleDTO1).isEqualTo(shopApprovalRuleDTO2);
        shopApprovalRuleDTO2.setId(2L);
        assertThat(shopApprovalRuleDTO1).isNotEqualTo(shopApprovalRuleDTO2);
        shopApprovalRuleDTO1.setId(null);
        assertThat(shopApprovalRuleDTO1).isNotEqualTo(shopApprovalRuleDTO2);
    }
}
