package de.jaide.slm2.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import de.jaide.slm2.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class OrderNoteDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrderNoteDTO.class);
        OrderNoteDTO orderNoteDTO1 = new OrderNoteDTO();
        orderNoteDTO1.setId(1L);
        OrderNoteDTO orderNoteDTO2 = new OrderNoteDTO();
        assertThat(orderNoteDTO1).isNotEqualTo(orderNoteDTO2);
        orderNoteDTO2.setId(orderNoteDTO1.getId());
        assertThat(orderNoteDTO1).isEqualTo(orderNoteDTO2);
        orderNoteDTO2.setId(2L);
        assertThat(orderNoteDTO1).isNotEqualTo(orderNoteDTO2);
        orderNoteDTO1.setId(null);
        assertThat(orderNoteDTO1).isNotEqualTo(orderNoteDTO2);
    }
}
