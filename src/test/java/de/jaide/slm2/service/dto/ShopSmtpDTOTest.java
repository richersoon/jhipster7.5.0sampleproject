package de.jaide.slm2.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import de.jaide.slm2.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ShopSmtpDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShopSmtpDTO.class);
        ShopSmtpDTO shopSmtpDTO1 = new ShopSmtpDTO();
        shopSmtpDTO1.setId(1L);
        ShopSmtpDTO shopSmtpDTO2 = new ShopSmtpDTO();
        assertThat(shopSmtpDTO1).isNotEqualTo(shopSmtpDTO2);
        shopSmtpDTO2.setId(shopSmtpDTO1.getId());
        assertThat(shopSmtpDTO1).isEqualTo(shopSmtpDTO2);
        shopSmtpDTO2.setId(2L);
        assertThat(shopSmtpDTO1).isNotEqualTo(shopSmtpDTO2);
        shopSmtpDTO1.setId(null);
        assertThat(shopSmtpDTO1).isNotEqualTo(shopSmtpDTO2);
    }
}
