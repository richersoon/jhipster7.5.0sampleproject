package de.jaide.slm2.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import de.jaide.slm2.IntegrationTest;
import de.jaide.slm2.domain.DefaultEmailTemplate;
import de.jaide.slm2.domain.enumeration.Language;
import de.jaide.slm2.repository.DefaultEmailTemplateRepository;
import de.jaide.slm2.service.dto.DefaultEmailTemplateDTO;
import de.jaide.slm2.service.mapper.DefaultEmailTemplateMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link DefaultEmailTemplateResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class DefaultEmailTemplateResourceIT {

    private static final String DEFAULT_SUBJECT = "AAAAAAAAAA";
    private static final String UPDATED_SUBJECT = "BBBBBBBBBB";

    private static final String DEFAULT_HEADER = "AAAAAAAAAA";
    private static final String UPDATED_HEADER = "BBBBBBBBBB";

    private static final String DEFAULT_BODY = "AAAAAAAAAA";
    private static final String UPDATED_BODY = "BBBBBBBBBB";

    private static final String DEFAULT_FOOTER = "AAAAAAAAAA";
    private static final String UPDATED_FOOTER = "BBBBBBBBBB";

    private static final Language DEFAULT_LANGUAGE = Language.EN;
    private static final Language UPDATED_LANGUAGE = Language.DE;

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_VERSION = 1L;
    private static final Long UPDATED_VERSION = 2L;

    private static final String ENTITY_API_URL = "/api/default-email-templates";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private DefaultEmailTemplateRepository defaultEmailTemplateRepository;

    @Autowired
    private DefaultEmailTemplateMapper defaultEmailTemplateMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDefaultEmailTemplateMockMvc;

    private DefaultEmailTemplate defaultEmailTemplate;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DefaultEmailTemplate createEntity(EntityManager em) {
        DefaultEmailTemplate defaultEmailTemplate = new DefaultEmailTemplate()
            .subject(DEFAULT_SUBJECT)
            .header(DEFAULT_HEADER)
            .body(DEFAULT_BODY)
            .footer(DEFAULT_FOOTER)
            .language(DEFAULT_LANGUAGE)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .version(DEFAULT_VERSION);
        return defaultEmailTemplate;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DefaultEmailTemplate createUpdatedEntity(EntityManager em) {
        DefaultEmailTemplate defaultEmailTemplate = new DefaultEmailTemplate()
            .subject(UPDATED_SUBJECT)
            .header(UPDATED_HEADER)
            .body(UPDATED_BODY)
            .footer(UPDATED_FOOTER)
            .language(UPDATED_LANGUAGE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);
        return defaultEmailTemplate;
    }

    @BeforeEach
    public void initTest() {
        defaultEmailTemplate = createEntity(em);
    }

    @Test
    @Transactional
    void createDefaultEmailTemplate() throws Exception {
        int databaseSizeBeforeCreate = defaultEmailTemplateRepository.findAll().size();
        // Create the DefaultEmailTemplate
        DefaultEmailTemplateDTO defaultEmailTemplateDTO = defaultEmailTemplateMapper.toDto(defaultEmailTemplate);
        restDefaultEmailTemplateMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(defaultEmailTemplateDTO))
            )
            .andExpect(status().isCreated());

        // Validate the DefaultEmailTemplate in the database
        List<DefaultEmailTemplate> defaultEmailTemplateList = defaultEmailTemplateRepository.findAll();
        assertThat(defaultEmailTemplateList).hasSize(databaseSizeBeforeCreate + 1);
        DefaultEmailTemplate testDefaultEmailTemplate = defaultEmailTemplateList.get(defaultEmailTemplateList.size() - 1);
        assertThat(testDefaultEmailTemplate.getSubject()).isEqualTo(DEFAULT_SUBJECT);
        assertThat(testDefaultEmailTemplate.getHeader()).isEqualTo(DEFAULT_HEADER);
        assertThat(testDefaultEmailTemplate.getBody()).isEqualTo(DEFAULT_BODY);
        assertThat(testDefaultEmailTemplate.getFooter()).isEqualTo(DEFAULT_FOOTER);
        assertThat(testDefaultEmailTemplate.getLanguage()).isEqualTo(DEFAULT_LANGUAGE);
        assertThat(testDefaultEmailTemplate.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testDefaultEmailTemplate.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testDefaultEmailTemplate.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testDefaultEmailTemplate.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testDefaultEmailTemplate.getVersion()).isEqualTo(DEFAULT_VERSION);
    }

    @Test
    @Transactional
    void createDefaultEmailTemplateWithExistingId() throws Exception {
        // Create the DefaultEmailTemplate with an existing ID
        defaultEmailTemplate.setId(1L);
        DefaultEmailTemplateDTO defaultEmailTemplateDTO = defaultEmailTemplateMapper.toDto(defaultEmailTemplate);

        int databaseSizeBeforeCreate = defaultEmailTemplateRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restDefaultEmailTemplateMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(defaultEmailTemplateDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DefaultEmailTemplate in the database
        List<DefaultEmailTemplate> defaultEmailTemplateList = defaultEmailTemplateRepository.findAll();
        assertThat(defaultEmailTemplateList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkSubjectIsRequired() throws Exception {
        int databaseSizeBeforeTest = defaultEmailTemplateRepository.findAll().size();
        // set the field null
        defaultEmailTemplate.setSubject(null);

        // Create the DefaultEmailTemplate, which fails.
        DefaultEmailTemplateDTO defaultEmailTemplateDTO = defaultEmailTemplateMapper.toDto(defaultEmailTemplate);

        restDefaultEmailTemplateMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(defaultEmailTemplateDTO))
            )
            .andExpect(status().isBadRequest());

        List<DefaultEmailTemplate> defaultEmailTemplateList = defaultEmailTemplateRepository.findAll();
        assertThat(defaultEmailTemplateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkLanguageIsRequired() throws Exception {
        int databaseSizeBeforeTest = defaultEmailTemplateRepository.findAll().size();
        // set the field null
        defaultEmailTemplate.setLanguage(null);

        // Create the DefaultEmailTemplate, which fails.
        DefaultEmailTemplateDTO defaultEmailTemplateDTO = defaultEmailTemplateMapper.toDto(defaultEmailTemplate);

        restDefaultEmailTemplateMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(defaultEmailTemplateDTO))
            )
            .andExpect(status().isBadRequest());

        List<DefaultEmailTemplate> defaultEmailTemplateList = defaultEmailTemplateRepository.findAll();
        assertThat(defaultEmailTemplateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllDefaultEmailTemplates() throws Exception {
        // Initialize the database
        defaultEmailTemplateRepository.saveAndFlush(defaultEmailTemplate);

        // Get all the defaultEmailTemplateList
        restDefaultEmailTemplateMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(defaultEmailTemplate.getId().intValue())))
            .andExpect(jsonPath("$.[*].subject").value(hasItem(DEFAULT_SUBJECT)))
            .andExpect(jsonPath("$.[*].header").value(hasItem(DEFAULT_HEADER.toString())))
            .andExpect(jsonPath("$.[*].body").value(hasItem(DEFAULT_BODY.toString())))
            .andExpect(jsonPath("$.[*].footer").value(hasItem(DEFAULT_FOOTER.toString())))
            .andExpect(jsonPath("$.[*].language").value(hasItem(DEFAULT_LANGUAGE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.intValue())));
    }

    @Test
    @Transactional
    void getDefaultEmailTemplate() throws Exception {
        // Initialize the database
        defaultEmailTemplateRepository.saveAndFlush(defaultEmailTemplate);

        // Get the defaultEmailTemplate
        restDefaultEmailTemplateMockMvc
            .perform(get(ENTITY_API_URL_ID, defaultEmailTemplate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(defaultEmailTemplate.getId().intValue()))
            .andExpect(jsonPath("$.subject").value(DEFAULT_SUBJECT))
            .andExpect(jsonPath("$.header").value(DEFAULT_HEADER.toString()))
            .andExpect(jsonPath("$.body").value(DEFAULT_BODY.toString()))
            .andExpect(jsonPath("$.footer").value(DEFAULT_FOOTER.toString()))
            .andExpect(jsonPath("$.language").value(DEFAULT_LANGUAGE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION.intValue()));
    }

    @Test
    @Transactional
    void getNonExistingDefaultEmailTemplate() throws Exception {
        // Get the defaultEmailTemplate
        restDefaultEmailTemplateMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewDefaultEmailTemplate() throws Exception {
        // Initialize the database
        defaultEmailTemplateRepository.saveAndFlush(defaultEmailTemplate);

        int databaseSizeBeforeUpdate = defaultEmailTemplateRepository.findAll().size();

        // Update the defaultEmailTemplate
        DefaultEmailTemplate updatedDefaultEmailTemplate = defaultEmailTemplateRepository.findById(defaultEmailTemplate.getId()).get();
        // Disconnect from session so that the updates on updatedDefaultEmailTemplate are not directly saved in db
        em.detach(updatedDefaultEmailTemplate);
        updatedDefaultEmailTemplate
            .subject(UPDATED_SUBJECT)
            .header(UPDATED_HEADER)
            .body(UPDATED_BODY)
            .footer(UPDATED_FOOTER)
            .language(UPDATED_LANGUAGE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);
        DefaultEmailTemplateDTO defaultEmailTemplateDTO = defaultEmailTemplateMapper.toDto(updatedDefaultEmailTemplate);

        restDefaultEmailTemplateMockMvc
            .perform(
                put(ENTITY_API_URL_ID, defaultEmailTemplateDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(defaultEmailTemplateDTO))
            )
            .andExpect(status().isOk());

        // Validate the DefaultEmailTemplate in the database
        List<DefaultEmailTemplate> defaultEmailTemplateList = defaultEmailTemplateRepository.findAll();
        assertThat(defaultEmailTemplateList).hasSize(databaseSizeBeforeUpdate);
        DefaultEmailTemplate testDefaultEmailTemplate = defaultEmailTemplateList.get(defaultEmailTemplateList.size() - 1);
        assertThat(testDefaultEmailTemplate.getSubject()).isEqualTo(UPDATED_SUBJECT);
        assertThat(testDefaultEmailTemplate.getHeader()).isEqualTo(UPDATED_HEADER);
        assertThat(testDefaultEmailTemplate.getBody()).isEqualTo(UPDATED_BODY);
        assertThat(testDefaultEmailTemplate.getFooter()).isEqualTo(UPDATED_FOOTER);
        assertThat(testDefaultEmailTemplate.getLanguage()).isEqualTo(UPDATED_LANGUAGE);
        assertThat(testDefaultEmailTemplate.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testDefaultEmailTemplate.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testDefaultEmailTemplate.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testDefaultEmailTemplate.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testDefaultEmailTemplate.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void putNonExistingDefaultEmailTemplate() throws Exception {
        int databaseSizeBeforeUpdate = defaultEmailTemplateRepository.findAll().size();
        defaultEmailTemplate.setId(count.incrementAndGet());

        // Create the DefaultEmailTemplate
        DefaultEmailTemplateDTO defaultEmailTemplateDTO = defaultEmailTemplateMapper.toDto(defaultEmailTemplate);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDefaultEmailTemplateMockMvc
            .perform(
                put(ENTITY_API_URL_ID, defaultEmailTemplateDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(defaultEmailTemplateDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DefaultEmailTemplate in the database
        List<DefaultEmailTemplate> defaultEmailTemplateList = defaultEmailTemplateRepository.findAll();
        assertThat(defaultEmailTemplateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchDefaultEmailTemplate() throws Exception {
        int databaseSizeBeforeUpdate = defaultEmailTemplateRepository.findAll().size();
        defaultEmailTemplate.setId(count.incrementAndGet());

        // Create the DefaultEmailTemplate
        DefaultEmailTemplateDTO defaultEmailTemplateDTO = defaultEmailTemplateMapper.toDto(defaultEmailTemplate);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDefaultEmailTemplateMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(defaultEmailTemplateDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DefaultEmailTemplate in the database
        List<DefaultEmailTemplate> defaultEmailTemplateList = defaultEmailTemplateRepository.findAll();
        assertThat(defaultEmailTemplateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamDefaultEmailTemplate() throws Exception {
        int databaseSizeBeforeUpdate = defaultEmailTemplateRepository.findAll().size();
        defaultEmailTemplate.setId(count.incrementAndGet());

        // Create the DefaultEmailTemplate
        DefaultEmailTemplateDTO defaultEmailTemplateDTO = defaultEmailTemplateMapper.toDto(defaultEmailTemplate);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDefaultEmailTemplateMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(defaultEmailTemplateDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the DefaultEmailTemplate in the database
        List<DefaultEmailTemplate> defaultEmailTemplateList = defaultEmailTemplateRepository.findAll();
        assertThat(defaultEmailTemplateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateDefaultEmailTemplateWithPatch() throws Exception {
        // Initialize the database
        defaultEmailTemplateRepository.saveAndFlush(defaultEmailTemplate);

        int databaseSizeBeforeUpdate = defaultEmailTemplateRepository.findAll().size();

        // Update the defaultEmailTemplate using partial update
        DefaultEmailTemplate partialUpdatedDefaultEmailTemplate = new DefaultEmailTemplate();
        partialUpdatedDefaultEmailTemplate.setId(defaultEmailTemplate.getId());

        partialUpdatedDefaultEmailTemplate.header(UPDATED_HEADER).body(UPDATED_BODY).footer(UPDATED_FOOTER).version(UPDATED_VERSION);

        restDefaultEmailTemplateMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDefaultEmailTemplate.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDefaultEmailTemplate))
            )
            .andExpect(status().isOk());

        // Validate the DefaultEmailTemplate in the database
        List<DefaultEmailTemplate> defaultEmailTemplateList = defaultEmailTemplateRepository.findAll();
        assertThat(defaultEmailTemplateList).hasSize(databaseSizeBeforeUpdate);
        DefaultEmailTemplate testDefaultEmailTemplate = defaultEmailTemplateList.get(defaultEmailTemplateList.size() - 1);
        assertThat(testDefaultEmailTemplate.getSubject()).isEqualTo(DEFAULT_SUBJECT);
        assertThat(testDefaultEmailTemplate.getHeader()).isEqualTo(UPDATED_HEADER);
        assertThat(testDefaultEmailTemplate.getBody()).isEqualTo(UPDATED_BODY);
        assertThat(testDefaultEmailTemplate.getFooter()).isEqualTo(UPDATED_FOOTER);
        assertThat(testDefaultEmailTemplate.getLanguage()).isEqualTo(DEFAULT_LANGUAGE);
        assertThat(testDefaultEmailTemplate.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testDefaultEmailTemplate.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testDefaultEmailTemplate.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testDefaultEmailTemplate.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testDefaultEmailTemplate.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void fullUpdateDefaultEmailTemplateWithPatch() throws Exception {
        // Initialize the database
        defaultEmailTemplateRepository.saveAndFlush(defaultEmailTemplate);

        int databaseSizeBeforeUpdate = defaultEmailTemplateRepository.findAll().size();

        // Update the defaultEmailTemplate using partial update
        DefaultEmailTemplate partialUpdatedDefaultEmailTemplate = new DefaultEmailTemplate();
        partialUpdatedDefaultEmailTemplate.setId(defaultEmailTemplate.getId());

        partialUpdatedDefaultEmailTemplate
            .subject(UPDATED_SUBJECT)
            .header(UPDATED_HEADER)
            .body(UPDATED_BODY)
            .footer(UPDATED_FOOTER)
            .language(UPDATED_LANGUAGE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);

        restDefaultEmailTemplateMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedDefaultEmailTemplate.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDefaultEmailTemplate))
            )
            .andExpect(status().isOk());

        // Validate the DefaultEmailTemplate in the database
        List<DefaultEmailTemplate> defaultEmailTemplateList = defaultEmailTemplateRepository.findAll();
        assertThat(defaultEmailTemplateList).hasSize(databaseSizeBeforeUpdate);
        DefaultEmailTemplate testDefaultEmailTemplate = defaultEmailTemplateList.get(defaultEmailTemplateList.size() - 1);
        assertThat(testDefaultEmailTemplate.getSubject()).isEqualTo(UPDATED_SUBJECT);
        assertThat(testDefaultEmailTemplate.getHeader()).isEqualTo(UPDATED_HEADER);
        assertThat(testDefaultEmailTemplate.getBody()).isEqualTo(UPDATED_BODY);
        assertThat(testDefaultEmailTemplate.getFooter()).isEqualTo(UPDATED_FOOTER);
        assertThat(testDefaultEmailTemplate.getLanguage()).isEqualTo(UPDATED_LANGUAGE);
        assertThat(testDefaultEmailTemplate.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testDefaultEmailTemplate.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testDefaultEmailTemplate.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testDefaultEmailTemplate.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testDefaultEmailTemplate.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void patchNonExistingDefaultEmailTemplate() throws Exception {
        int databaseSizeBeforeUpdate = defaultEmailTemplateRepository.findAll().size();
        defaultEmailTemplate.setId(count.incrementAndGet());

        // Create the DefaultEmailTemplate
        DefaultEmailTemplateDTO defaultEmailTemplateDTO = defaultEmailTemplateMapper.toDto(defaultEmailTemplate);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDefaultEmailTemplateMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, defaultEmailTemplateDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(defaultEmailTemplateDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DefaultEmailTemplate in the database
        List<DefaultEmailTemplate> defaultEmailTemplateList = defaultEmailTemplateRepository.findAll();
        assertThat(defaultEmailTemplateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchDefaultEmailTemplate() throws Exception {
        int databaseSizeBeforeUpdate = defaultEmailTemplateRepository.findAll().size();
        defaultEmailTemplate.setId(count.incrementAndGet());

        // Create the DefaultEmailTemplate
        DefaultEmailTemplateDTO defaultEmailTemplateDTO = defaultEmailTemplateMapper.toDto(defaultEmailTemplate);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDefaultEmailTemplateMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(defaultEmailTemplateDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the DefaultEmailTemplate in the database
        List<DefaultEmailTemplate> defaultEmailTemplateList = defaultEmailTemplateRepository.findAll();
        assertThat(defaultEmailTemplateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamDefaultEmailTemplate() throws Exception {
        int databaseSizeBeforeUpdate = defaultEmailTemplateRepository.findAll().size();
        defaultEmailTemplate.setId(count.incrementAndGet());

        // Create the DefaultEmailTemplate
        DefaultEmailTemplateDTO defaultEmailTemplateDTO = defaultEmailTemplateMapper.toDto(defaultEmailTemplate);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restDefaultEmailTemplateMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(defaultEmailTemplateDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the DefaultEmailTemplate in the database
        List<DefaultEmailTemplate> defaultEmailTemplateList = defaultEmailTemplateRepository.findAll();
        assertThat(defaultEmailTemplateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteDefaultEmailTemplate() throws Exception {
        // Initialize the database
        defaultEmailTemplateRepository.saveAndFlush(defaultEmailTemplate);

        int databaseSizeBeforeDelete = defaultEmailTemplateRepository.findAll().size();

        // Delete the defaultEmailTemplate
        restDefaultEmailTemplateMockMvc
            .perform(delete(ENTITY_API_URL_ID, defaultEmailTemplate.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DefaultEmailTemplate> defaultEmailTemplateList = defaultEmailTemplateRepository.findAll();
        assertThat(defaultEmailTemplateList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
