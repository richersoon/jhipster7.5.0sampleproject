package de.jaide.slm2.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import de.jaide.slm2.IntegrationTest;
import de.jaide.slm2.domain.Shop;
import de.jaide.slm2.domain.User;
import de.jaide.slm2.domain.UserShop;
import de.jaide.slm2.domain.enumeration.Role;
import de.jaide.slm2.repository.UserShopRepository;
import de.jaide.slm2.service.dto.UserShopDTO;
import de.jaide.slm2.service.mapper.UserShopMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link UserShopResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class UserShopResourceIT {

    private static final Role DEFAULT_ROLE = Role.MANAGER;
    private static final Role UPDATED_ROLE = Role.EMPLOYEE;

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_VERSION = 1L;
    private static final Long UPDATED_VERSION = 2L;

    private static final String ENTITY_API_URL = "/api/user-shops";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private UserShopRepository userShopRepository;

    @Autowired
    private UserShopMapper userShopMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restUserShopMockMvc;

    private UserShop userShop;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserShop createEntity(EntityManager em) {
        UserShop userShop = new UserShop()
            .role(DEFAULT_ROLE)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .version(DEFAULT_VERSION);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        userShop.setUser(user);
        // Add required entity
        Shop shop;
        if (TestUtil.findAll(em, Shop.class).isEmpty()) {
            shop = ShopResourceIT.createEntity(em);
            em.persist(shop);
            em.flush();
        } else {
            shop = TestUtil.findAll(em, Shop.class).get(0);
        }
        userShop.setShop(shop);
        return userShop;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserShop createUpdatedEntity(EntityManager em) {
        UserShop userShop = new UserShop()
            .role(UPDATED_ROLE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        userShop.setUser(user);
        // Add required entity
        Shop shop;
        if (TestUtil.findAll(em, Shop.class).isEmpty()) {
            shop = ShopResourceIT.createUpdatedEntity(em);
            em.persist(shop);
            em.flush();
        } else {
            shop = TestUtil.findAll(em, Shop.class).get(0);
        }
        userShop.setShop(shop);
        return userShop;
    }

    @BeforeEach
    public void initTest() {
        userShop = createEntity(em);
    }

    @Test
    @Transactional
    void createUserShop() throws Exception {
        int databaseSizeBeforeCreate = userShopRepository.findAll().size();
        // Create the UserShop
        UserShopDTO userShopDTO = userShopMapper.toDto(userShop);
        restUserShopMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(userShopDTO)))
            .andExpect(status().isCreated());

        // Validate the UserShop in the database
        List<UserShop> userShopList = userShopRepository.findAll();
        assertThat(userShopList).hasSize(databaseSizeBeforeCreate + 1);
        UserShop testUserShop = userShopList.get(userShopList.size() - 1);
        assertThat(testUserShop.getRole()).isEqualTo(DEFAULT_ROLE);
        assertThat(testUserShop.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testUserShop.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testUserShop.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testUserShop.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testUserShop.getVersion()).isEqualTo(DEFAULT_VERSION);
    }

    @Test
    @Transactional
    void createUserShopWithExistingId() throws Exception {
        // Create the UserShop with an existing ID
        userShop.setId(1L);
        UserShopDTO userShopDTO = userShopMapper.toDto(userShop);

        int databaseSizeBeforeCreate = userShopRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserShopMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(userShopDTO)))
            .andExpect(status().isBadRequest());

        // Validate the UserShop in the database
        List<UserShop> userShopList = userShopRepository.findAll();
        assertThat(userShopList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkRoleIsRequired() throws Exception {
        int databaseSizeBeforeTest = userShopRepository.findAll().size();
        // set the field null
        userShop.setRole(null);

        // Create the UserShop, which fails.
        UserShopDTO userShopDTO = userShopMapper.toDto(userShop);

        restUserShopMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(userShopDTO)))
            .andExpect(status().isBadRequest());

        List<UserShop> userShopList = userShopRepository.findAll();
        assertThat(userShopList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllUserShops() throws Exception {
        // Initialize the database
        userShopRepository.saveAndFlush(userShop);

        // Get all the userShopList
        restUserShopMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userShop.getId().intValue())))
            .andExpect(jsonPath("$.[*].role").value(hasItem(DEFAULT_ROLE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.intValue())));
    }

    @Test
    @Transactional
    void getUserShop() throws Exception {
        // Initialize the database
        userShopRepository.saveAndFlush(userShop);

        // Get the userShop
        restUserShopMockMvc
            .perform(get(ENTITY_API_URL_ID, userShop.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(userShop.getId().intValue()))
            .andExpect(jsonPath("$.role").value(DEFAULT_ROLE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION.intValue()));
    }

    @Test
    @Transactional
    void getNonExistingUserShop() throws Exception {
        // Get the userShop
        restUserShopMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewUserShop() throws Exception {
        // Initialize the database
        userShopRepository.saveAndFlush(userShop);

        int databaseSizeBeforeUpdate = userShopRepository.findAll().size();

        // Update the userShop
        UserShop updatedUserShop = userShopRepository.findById(userShop.getId()).get();
        // Disconnect from session so that the updates on updatedUserShop are not directly saved in db
        em.detach(updatedUserShop);
        updatedUserShop
            .role(UPDATED_ROLE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);
        UserShopDTO userShopDTO = userShopMapper.toDto(updatedUserShop);

        restUserShopMockMvc
            .perform(
                put(ENTITY_API_URL_ID, userShopDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userShopDTO))
            )
            .andExpect(status().isOk());

        // Validate the UserShop in the database
        List<UserShop> userShopList = userShopRepository.findAll();
        assertThat(userShopList).hasSize(databaseSizeBeforeUpdate);
        UserShop testUserShop = userShopList.get(userShopList.size() - 1);
        assertThat(testUserShop.getRole()).isEqualTo(UPDATED_ROLE);
        assertThat(testUserShop.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testUserShop.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testUserShop.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testUserShop.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testUserShop.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void putNonExistingUserShop() throws Exception {
        int databaseSizeBeforeUpdate = userShopRepository.findAll().size();
        userShop.setId(count.incrementAndGet());

        // Create the UserShop
        UserShopDTO userShopDTO = userShopMapper.toDto(userShop);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserShopMockMvc
            .perform(
                put(ENTITY_API_URL_ID, userShopDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userShopDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserShop in the database
        List<UserShop> userShopList = userShopRepository.findAll();
        assertThat(userShopList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchUserShop() throws Exception {
        int databaseSizeBeforeUpdate = userShopRepository.findAll().size();
        userShop.setId(count.incrementAndGet());

        // Create the UserShop
        UserShopDTO userShopDTO = userShopMapper.toDto(userShop);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserShopMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userShopDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserShop in the database
        List<UserShop> userShopList = userShopRepository.findAll();
        assertThat(userShopList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamUserShop() throws Exception {
        int databaseSizeBeforeUpdate = userShopRepository.findAll().size();
        userShop.setId(count.incrementAndGet());

        // Create the UserShop
        UserShopDTO userShopDTO = userShopMapper.toDto(userShop);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserShopMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(userShopDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the UserShop in the database
        List<UserShop> userShopList = userShopRepository.findAll();
        assertThat(userShopList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateUserShopWithPatch() throws Exception {
        // Initialize the database
        userShopRepository.saveAndFlush(userShop);

        int databaseSizeBeforeUpdate = userShopRepository.findAll().size();

        // Update the userShop using partial update
        UserShop partialUpdatedUserShop = new UserShop();
        partialUpdatedUserShop.setId(userShop.getId());

        partialUpdatedUserShop
            .role(UPDATED_ROLE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .version(UPDATED_VERSION);

        restUserShopMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedUserShop.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedUserShop))
            )
            .andExpect(status().isOk());

        // Validate the UserShop in the database
        List<UserShop> userShopList = userShopRepository.findAll();
        assertThat(userShopList).hasSize(databaseSizeBeforeUpdate);
        UserShop testUserShop = userShopList.get(userShopList.size() - 1);
        assertThat(testUserShop.getRole()).isEqualTo(UPDATED_ROLE);
        assertThat(testUserShop.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testUserShop.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testUserShop.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testUserShop.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testUserShop.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void fullUpdateUserShopWithPatch() throws Exception {
        // Initialize the database
        userShopRepository.saveAndFlush(userShop);

        int databaseSizeBeforeUpdate = userShopRepository.findAll().size();

        // Update the userShop using partial update
        UserShop partialUpdatedUserShop = new UserShop();
        partialUpdatedUserShop.setId(userShop.getId());

        partialUpdatedUserShop
            .role(UPDATED_ROLE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);

        restUserShopMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedUserShop.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedUserShop))
            )
            .andExpect(status().isOk());

        // Validate the UserShop in the database
        List<UserShop> userShopList = userShopRepository.findAll();
        assertThat(userShopList).hasSize(databaseSizeBeforeUpdate);
        UserShop testUserShop = userShopList.get(userShopList.size() - 1);
        assertThat(testUserShop.getRole()).isEqualTo(UPDATED_ROLE);
        assertThat(testUserShop.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testUserShop.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testUserShop.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testUserShop.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testUserShop.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void patchNonExistingUserShop() throws Exception {
        int databaseSizeBeforeUpdate = userShopRepository.findAll().size();
        userShop.setId(count.incrementAndGet());

        // Create the UserShop
        UserShopDTO userShopDTO = userShopMapper.toDto(userShop);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserShopMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, userShopDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(userShopDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserShop in the database
        List<UserShop> userShopList = userShopRepository.findAll();
        assertThat(userShopList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchUserShop() throws Exception {
        int databaseSizeBeforeUpdate = userShopRepository.findAll().size();
        userShop.setId(count.incrementAndGet());

        // Create the UserShop
        UserShopDTO userShopDTO = userShopMapper.toDto(userShop);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserShopMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(userShopDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserShop in the database
        List<UserShop> userShopList = userShopRepository.findAll();
        assertThat(userShopList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamUserShop() throws Exception {
        int databaseSizeBeforeUpdate = userShopRepository.findAll().size();
        userShop.setId(count.incrementAndGet());

        // Create the UserShop
        UserShopDTO userShopDTO = userShopMapper.toDto(userShop);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserShopMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(userShopDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the UserShop in the database
        List<UserShop> userShopList = userShopRepository.findAll();
        assertThat(userShopList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteUserShop() throws Exception {
        // Initialize the database
        userShopRepository.saveAndFlush(userShop);

        int databaseSizeBeforeDelete = userShopRepository.findAll().size();

        // Delete the userShop
        restUserShopMockMvc
            .perform(delete(ENTITY_API_URL_ID, userShop.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserShop> userShopList = userShopRepository.findAll();
        assertThat(userShopList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
