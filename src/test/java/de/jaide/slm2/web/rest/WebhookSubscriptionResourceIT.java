package de.jaide.slm2.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import de.jaide.slm2.IntegrationTest;
import de.jaide.slm2.domain.Shop;
import de.jaide.slm2.domain.WebhookSubscription;
import de.jaide.slm2.domain.enumeration.WebhookType;
import de.jaide.slm2.repository.WebhookSubscriptionRepository;
import de.jaide.slm2.service.dto.WebhookSubscriptionDTO;
import de.jaide.slm2.service.mapper.WebhookSubscriptionMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link WebhookSubscriptionResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class WebhookSubscriptionResourceIT {

    private static final String DEFAULT_EXTERNAL_ID = "AAAAAAAAAA";
    private static final String UPDATED_EXTERNAL_ID = "BBBBBBBBBB";

    private static final WebhookType DEFAULT_TYPE = WebhookType.ORDER_PAID;
    private static final WebhookType UPDATED_TYPE = WebhookType.APP_UNINSTALLED;

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_VERSION = 1L;
    private static final Long UPDATED_VERSION = 2L;

    private static final String ENTITY_API_URL = "/api/webhook-subscriptions";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private WebhookSubscriptionRepository webhookSubscriptionRepository;

    @Autowired
    private WebhookSubscriptionMapper webhookSubscriptionMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restWebhookSubscriptionMockMvc;

    private WebhookSubscription webhookSubscription;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WebhookSubscription createEntity(EntityManager em) {
        WebhookSubscription webhookSubscription = new WebhookSubscription()
            .externalId(DEFAULT_EXTERNAL_ID)
            .type(DEFAULT_TYPE)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .version(DEFAULT_VERSION);
        // Add required entity
        Shop shop;
        if (TestUtil.findAll(em, Shop.class).isEmpty()) {
            shop = ShopResourceIT.createEntity(em);
            em.persist(shop);
            em.flush();
        } else {
            shop = TestUtil.findAll(em, Shop.class).get(0);
        }
        webhookSubscription.setShop(shop);
        return webhookSubscription;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static WebhookSubscription createUpdatedEntity(EntityManager em) {
        WebhookSubscription webhookSubscription = new WebhookSubscription()
            .externalId(UPDATED_EXTERNAL_ID)
            .type(UPDATED_TYPE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);
        // Add required entity
        Shop shop;
        if (TestUtil.findAll(em, Shop.class).isEmpty()) {
            shop = ShopResourceIT.createUpdatedEntity(em);
            em.persist(shop);
            em.flush();
        } else {
            shop = TestUtil.findAll(em, Shop.class).get(0);
        }
        webhookSubscription.setShop(shop);
        return webhookSubscription;
    }

    @BeforeEach
    public void initTest() {
        webhookSubscription = createEntity(em);
    }

    @Test
    @Transactional
    void createWebhookSubscription() throws Exception {
        int databaseSizeBeforeCreate = webhookSubscriptionRepository.findAll().size();
        // Create the WebhookSubscription
        WebhookSubscriptionDTO webhookSubscriptionDTO = webhookSubscriptionMapper.toDto(webhookSubscription);
        restWebhookSubscriptionMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(webhookSubscriptionDTO))
            )
            .andExpect(status().isCreated());

        // Validate the WebhookSubscription in the database
        List<WebhookSubscription> webhookSubscriptionList = webhookSubscriptionRepository.findAll();
        assertThat(webhookSubscriptionList).hasSize(databaseSizeBeforeCreate + 1);
        WebhookSubscription testWebhookSubscription = webhookSubscriptionList.get(webhookSubscriptionList.size() - 1);
        assertThat(testWebhookSubscription.getExternalId()).isEqualTo(DEFAULT_EXTERNAL_ID);
        assertThat(testWebhookSubscription.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testWebhookSubscription.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testWebhookSubscription.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testWebhookSubscription.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testWebhookSubscription.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testWebhookSubscription.getVersion()).isEqualTo(DEFAULT_VERSION);
    }

    @Test
    @Transactional
    void createWebhookSubscriptionWithExistingId() throws Exception {
        // Create the WebhookSubscription with an existing ID
        webhookSubscription.setId(1L);
        WebhookSubscriptionDTO webhookSubscriptionDTO = webhookSubscriptionMapper.toDto(webhookSubscription);

        int databaseSizeBeforeCreate = webhookSubscriptionRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restWebhookSubscriptionMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(webhookSubscriptionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the WebhookSubscription in the database
        List<WebhookSubscription> webhookSubscriptionList = webhookSubscriptionRepository.findAll();
        assertThat(webhookSubscriptionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkExternalIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = webhookSubscriptionRepository.findAll().size();
        // set the field null
        webhookSubscription.setExternalId(null);

        // Create the WebhookSubscription, which fails.
        WebhookSubscriptionDTO webhookSubscriptionDTO = webhookSubscriptionMapper.toDto(webhookSubscription);

        restWebhookSubscriptionMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(webhookSubscriptionDTO))
            )
            .andExpect(status().isBadRequest());

        List<WebhookSubscription> webhookSubscriptionList = webhookSubscriptionRepository.findAll();
        assertThat(webhookSubscriptionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = webhookSubscriptionRepository.findAll().size();
        // set the field null
        webhookSubscription.setType(null);

        // Create the WebhookSubscription, which fails.
        WebhookSubscriptionDTO webhookSubscriptionDTO = webhookSubscriptionMapper.toDto(webhookSubscription);

        restWebhookSubscriptionMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(webhookSubscriptionDTO))
            )
            .andExpect(status().isBadRequest());

        List<WebhookSubscription> webhookSubscriptionList = webhookSubscriptionRepository.findAll();
        assertThat(webhookSubscriptionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllWebhookSubscriptions() throws Exception {
        // Initialize the database
        webhookSubscriptionRepository.saveAndFlush(webhookSubscription);

        // Get all the webhookSubscriptionList
        restWebhookSubscriptionMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(webhookSubscription.getId().intValue())))
            .andExpect(jsonPath("$.[*].externalId").value(hasItem(DEFAULT_EXTERNAL_ID)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.intValue())));
    }

    @Test
    @Transactional
    void getWebhookSubscription() throws Exception {
        // Initialize the database
        webhookSubscriptionRepository.saveAndFlush(webhookSubscription);

        // Get the webhookSubscription
        restWebhookSubscriptionMockMvc
            .perform(get(ENTITY_API_URL_ID, webhookSubscription.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(webhookSubscription.getId().intValue()))
            .andExpect(jsonPath("$.externalId").value(DEFAULT_EXTERNAL_ID))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION.intValue()));
    }

    @Test
    @Transactional
    void getNonExistingWebhookSubscription() throws Exception {
        // Get the webhookSubscription
        restWebhookSubscriptionMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewWebhookSubscription() throws Exception {
        // Initialize the database
        webhookSubscriptionRepository.saveAndFlush(webhookSubscription);

        int databaseSizeBeforeUpdate = webhookSubscriptionRepository.findAll().size();

        // Update the webhookSubscription
        WebhookSubscription updatedWebhookSubscription = webhookSubscriptionRepository.findById(webhookSubscription.getId()).get();
        // Disconnect from session so that the updates on updatedWebhookSubscription are not directly saved in db
        em.detach(updatedWebhookSubscription);
        updatedWebhookSubscription
            .externalId(UPDATED_EXTERNAL_ID)
            .type(UPDATED_TYPE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);
        WebhookSubscriptionDTO webhookSubscriptionDTO = webhookSubscriptionMapper.toDto(updatedWebhookSubscription);

        restWebhookSubscriptionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, webhookSubscriptionDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(webhookSubscriptionDTO))
            )
            .andExpect(status().isOk());

        // Validate the WebhookSubscription in the database
        List<WebhookSubscription> webhookSubscriptionList = webhookSubscriptionRepository.findAll();
        assertThat(webhookSubscriptionList).hasSize(databaseSizeBeforeUpdate);
        WebhookSubscription testWebhookSubscription = webhookSubscriptionList.get(webhookSubscriptionList.size() - 1);
        assertThat(testWebhookSubscription.getExternalId()).isEqualTo(UPDATED_EXTERNAL_ID);
        assertThat(testWebhookSubscription.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testWebhookSubscription.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testWebhookSubscription.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testWebhookSubscription.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testWebhookSubscription.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testWebhookSubscription.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void putNonExistingWebhookSubscription() throws Exception {
        int databaseSizeBeforeUpdate = webhookSubscriptionRepository.findAll().size();
        webhookSubscription.setId(count.incrementAndGet());

        // Create the WebhookSubscription
        WebhookSubscriptionDTO webhookSubscriptionDTO = webhookSubscriptionMapper.toDto(webhookSubscription);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restWebhookSubscriptionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, webhookSubscriptionDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(webhookSubscriptionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the WebhookSubscription in the database
        List<WebhookSubscription> webhookSubscriptionList = webhookSubscriptionRepository.findAll();
        assertThat(webhookSubscriptionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchWebhookSubscription() throws Exception {
        int databaseSizeBeforeUpdate = webhookSubscriptionRepository.findAll().size();
        webhookSubscription.setId(count.incrementAndGet());

        // Create the WebhookSubscription
        WebhookSubscriptionDTO webhookSubscriptionDTO = webhookSubscriptionMapper.toDto(webhookSubscription);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restWebhookSubscriptionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(webhookSubscriptionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the WebhookSubscription in the database
        List<WebhookSubscription> webhookSubscriptionList = webhookSubscriptionRepository.findAll();
        assertThat(webhookSubscriptionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamWebhookSubscription() throws Exception {
        int databaseSizeBeforeUpdate = webhookSubscriptionRepository.findAll().size();
        webhookSubscription.setId(count.incrementAndGet());

        // Create the WebhookSubscription
        WebhookSubscriptionDTO webhookSubscriptionDTO = webhookSubscriptionMapper.toDto(webhookSubscription);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restWebhookSubscriptionMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(webhookSubscriptionDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the WebhookSubscription in the database
        List<WebhookSubscription> webhookSubscriptionList = webhookSubscriptionRepository.findAll();
        assertThat(webhookSubscriptionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateWebhookSubscriptionWithPatch() throws Exception {
        // Initialize the database
        webhookSubscriptionRepository.saveAndFlush(webhookSubscription);

        int databaseSizeBeforeUpdate = webhookSubscriptionRepository.findAll().size();

        // Update the webhookSubscription using partial update
        WebhookSubscription partialUpdatedWebhookSubscription = new WebhookSubscription();
        partialUpdatedWebhookSubscription.setId(webhookSubscription.getId());

        partialUpdatedWebhookSubscription
            .type(UPDATED_TYPE)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE);

        restWebhookSubscriptionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedWebhookSubscription.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedWebhookSubscription))
            )
            .andExpect(status().isOk());

        // Validate the WebhookSubscription in the database
        List<WebhookSubscription> webhookSubscriptionList = webhookSubscriptionRepository.findAll();
        assertThat(webhookSubscriptionList).hasSize(databaseSizeBeforeUpdate);
        WebhookSubscription testWebhookSubscription = webhookSubscriptionList.get(webhookSubscriptionList.size() - 1);
        assertThat(testWebhookSubscription.getExternalId()).isEqualTo(DEFAULT_EXTERNAL_ID);
        assertThat(testWebhookSubscription.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testWebhookSubscription.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testWebhookSubscription.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testWebhookSubscription.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testWebhookSubscription.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testWebhookSubscription.getVersion()).isEqualTo(DEFAULT_VERSION);
    }

    @Test
    @Transactional
    void fullUpdateWebhookSubscriptionWithPatch() throws Exception {
        // Initialize the database
        webhookSubscriptionRepository.saveAndFlush(webhookSubscription);

        int databaseSizeBeforeUpdate = webhookSubscriptionRepository.findAll().size();

        // Update the webhookSubscription using partial update
        WebhookSubscription partialUpdatedWebhookSubscription = new WebhookSubscription();
        partialUpdatedWebhookSubscription.setId(webhookSubscription.getId());

        partialUpdatedWebhookSubscription
            .externalId(UPDATED_EXTERNAL_ID)
            .type(UPDATED_TYPE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);

        restWebhookSubscriptionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedWebhookSubscription.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedWebhookSubscription))
            )
            .andExpect(status().isOk());

        // Validate the WebhookSubscription in the database
        List<WebhookSubscription> webhookSubscriptionList = webhookSubscriptionRepository.findAll();
        assertThat(webhookSubscriptionList).hasSize(databaseSizeBeforeUpdate);
        WebhookSubscription testWebhookSubscription = webhookSubscriptionList.get(webhookSubscriptionList.size() - 1);
        assertThat(testWebhookSubscription.getExternalId()).isEqualTo(UPDATED_EXTERNAL_ID);
        assertThat(testWebhookSubscription.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testWebhookSubscription.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testWebhookSubscription.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testWebhookSubscription.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testWebhookSubscription.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testWebhookSubscription.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void patchNonExistingWebhookSubscription() throws Exception {
        int databaseSizeBeforeUpdate = webhookSubscriptionRepository.findAll().size();
        webhookSubscription.setId(count.incrementAndGet());

        // Create the WebhookSubscription
        WebhookSubscriptionDTO webhookSubscriptionDTO = webhookSubscriptionMapper.toDto(webhookSubscription);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restWebhookSubscriptionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, webhookSubscriptionDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(webhookSubscriptionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the WebhookSubscription in the database
        List<WebhookSubscription> webhookSubscriptionList = webhookSubscriptionRepository.findAll();
        assertThat(webhookSubscriptionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchWebhookSubscription() throws Exception {
        int databaseSizeBeforeUpdate = webhookSubscriptionRepository.findAll().size();
        webhookSubscription.setId(count.incrementAndGet());

        // Create the WebhookSubscription
        WebhookSubscriptionDTO webhookSubscriptionDTO = webhookSubscriptionMapper.toDto(webhookSubscription);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restWebhookSubscriptionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(webhookSubscriptionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the WebhookSubscription in the database
        List<WebhookSubscription> webhookSubscriptionList = webhookSubscriptionRepository.findAll();
        assertThat(webhookSubscriptionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamWebhookSubscription() throws Exception {
        int databaseSizeBeforeUpdate = webhookSubscriptionRepository.findAll().size();
        webhookSubscription.setId(count.incrementAndGet());

        // Create the WebhookSubscription
        WebhookSubscriptionDTO webhookSubscriptionDTO = webhookSubscriptionMapper.toDto(webhookSubscription);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restWebhookSubscriptionMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(webhookSubscriptionDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the WebhookSubscription in the database
        List<WebhookSubscription> webhookSubscriptionList = webhookSubscriptionRepository.findAll();
        assertThat(webhookSubscriptionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteWebhookSubscription() throws Exception {
        // Initialize the database
        webhookSubscriptionRepository.saveAndFlush(webhookSubscription);

        int databaseSizeBeforeDelete = webhookSubscriptionRepository.findAll().size();

        // Delete the webhookSubscription
        restWebhookSubscriptionMockMvc
            .perform(delete(ENTITY_API_URL_ID, webhookSubscription.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<WebhookSubscription> webhookSubscriptionList = webhookSubscriptionRepository.findAll();
        assertThat(webhookSubscriptionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
