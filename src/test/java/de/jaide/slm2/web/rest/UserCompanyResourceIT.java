package de.jaide.slm2.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import de.jaide.slm2.IntegrationTest;
import de.jaide.slm2.domain.Company;
import de.jaide.slm2.domain.User;
import de.jaide.slm2.domain.UserCompany;
import de.jaide.slm2.domain.enumeration.Role;
import de.jaide.slm2.repository.UserCompanyRepository;
import de.jaide.slm2.service.dto.UserCompanyDTO;
import de.jaide.slm2.service.mapper.UserCompanyMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link UserCompanyResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class UserCompanyResourceIT {

    private static final Role DEFAULT_ROLE = Role.MANAGER;
    private static final Role UPDATED_ROLE = Role.EMPLOYEE;

    private static final Boolean DEFAULT_PERMANENT = false;
    private static final Boolean UPDATED_PERMANENT = true;

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_VERSION = 1L;
    private static final Long UPDATED_VERSION = 2L;

    private static final String ENTITY_API_URL = "/api/user-companies";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private UserCompanyRepository userCompanyRepository;

    @Autowired
    private UserCompanyMapper userCompanyMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restUserCompanyMockMvc;

    private UserCompany userCompany;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserCompany createEntity(EntityManager em) {
        UserCompany userCompany = new UserCompany()
            .role(DEFAULT_ROLE)
            .permanent(DEFAULT_PERMANENT)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .version(DEFAULT_VERSION);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        userCompany.setUser(user);
        // Add required entity
        Company company;
        if (TestUtil.findAll(em, Company.class).isEmpty()) {
            company = CompanyResourceIT.createEntity(em);
            em.persist(company);
            em.flush();
        } else {
            company = TestUtil.findAll(em, Company.class).get(0);
        }
        userCompany.setCompany(company);
        return userCompany;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserCompany createUpdatedEntity(EntityManager em) {
        UserCompany userCompany = new UserCompany()
            .role(UPDATED_ROLE)
            .permanent(UPDATED_PERMANENT)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);
        // Add required entity
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        userCompany.setUser(user);
        // Add required entity
        Company company;
        if (TestUtil.findAll(em, Company.class).isEmpty()) {
            company = CompanyResourceIT.createUpdatedEntity(em);
            em.persist(company);
            em.flush();
        } else {
            company = TestUtil.findAll(em, Company.class).get(0);
        }
        userCompany.setCompany(company);
        return userCompany;
    }

    @BeforeEach
    public void initTest() {
        userCompany = createEntity(em);
    }

    @Test
    @Transactional
    void createUserCompany() throws Exception {
        int databaseSizeBeforeCreate = userCompanyRepository.findAll().size();
        // Create the UserCompany
        UserCompanyDTO userCompanyDTO = userCompanyMapper.toDto(userCompany);
        restUserCompanyMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(userCompanyDTO))
            )
            .andExpect(status().isCreated());

        // Validate the UserCompany in the database
        List<UserCompany> userCompanyList = userCompanyRepository.findAll();
        assertThat(userCompanyList).hasSize(databaseSizeBeforeCreate + 1);
        UserCompany testUserCompany = userCompanyList.get(userCompanyList.size() - 1);
        assertThat(testUserCompany.getRole()).isEqualTo(DEFAULT_ROLE);
        assertThat(testUserCompany.getPermanent()).isEqualTo(DEFAULT_PERMANENT);
        assertThat(testUserCompany.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testUserCompany.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testUserCompany.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testUserCompany.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testUserCompany.getVersion()).isEqualTo(DEFAULT_VERSION);
    }

    @Test
    @Transactional
    void createUserCompanyWithExistingId() throws Exception {
        // Create the UserCompany with an existing ID
        userCompany.setId(1L);
        UserCompanyDTO userCompanyDTO = userCompanyMapper.toDto(userCompany);

        int databaseSizeBeforeCreate = userCompanyRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserCompanyMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(userCompanyDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserCompany in the database
        List<UserCompany> userCompanyList = userCompanyRepository.findAll();
        assertThat(userCompanyList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkRoleIsRequired() throws Exception {
        int databaseSizeBeforeTest = userCompanyRepository.findAll().size();
        // set the field null
        userCompany.setRole(null);

        // Create the UserCompany, which fails.
        UserCompanyDTO userCompanyDTO = userCompanyMapper.toDto(userCompany);

        restUserCompanyMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(userCompanyDTO))
            )
            .andExpect(status().isBadRequest());

        List<UserCompany> userCompanyList = userCompanyRepository.findAll();
        assertThat(userCompanyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPermanentIsRequired() throws Exception {
        int databaseSizeBeforeTest = userCompanyRepository.findAll().size();
        // set the field null
        userCompany.setPermanent(null);

        // Create the UserCompany, which fails.
        UserCompanyDTO userCompanyDTO = userCompanyMapper.toDto(userCompany);

        restUserCompanyMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(userCompanyDTO))
            )
            .andExpect(status().isBadRequest());

        List<UserCompany> userCompanyList = userCompanyRepository.findAll();
        assertThat(userCompanyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllUserCompanies() throws Exception {
        // Initialize the database
        userCompanyRepository.saveAndFlush(userCompany);

        // Get all the userCompanyList
        restUserCompanyMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userCompany.getId().intValue())))
            .andExpect(jsonPath("$.[*].role").value(hasItem(DEFAULT_ROLE.toString())))
            .andExpect(jsonPath("$.[*].permanent").value(hasItem(DEFAULT_PERMANENT.booleanValue())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.intValue())));
    }

    @Test
    @Transactional
    void getUserCompany() throws Exception {
        // Initialize the database
        userCompanyRepository.saveAndFlush(userCompany);

        // Get the userCompany
        restUserCompanyMockMvc
            .perform(get(ENTITY_API_URL_ID, userCompany.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(userCompany.getId().intValue()))
            .andExpect(jsonPath("$.role").value(DEFAULT_ROLE.toString()))
            .andExpect(jsonPath("$.permanent").value(DEFAULT_PERMANENT.booleanValue()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION.intValue()));
    }

    @Test
    @Transactional
    void getNonExistingUserCompany() throws Exception {
        // Get the userCompany
        restUserCompanyMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewUserCompany() throws Exception {
        // Initialize the database
        userCompanyRepository.saveAndFlush(userCompany);

        int databaseSizeBeforeUpdate = userCompanyRepository.findAll().size();

        // Update the userCompany
        UserCompany updatedUserCompany = userCompanyRepository.findById(userCompany.getId()).get();
        // Disconnect from session so that the updates on updatedUserCompany are not directly saved in db
        em.detach(updatedUserCompany);
        updatedUserCompany
            .role(UPDATED_ROLE)
            .permanent(UPDATED_PERMANENT)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);
        UserCompanyDTO userCompanyDTO = userCompanyMapper.toDto(updatedUserCompany);

        restUserCompanyMockMvc
            .perform(
                put(ENTITY_API_URL_ID, userCompanyDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userCompanyDTO))
            )
            .andExpect(status().isOk());

        // Validate the UserCompany in the database
        List<UserCompany> userCompanyList = userCompanyRepository.findAll();
        assertThat(userCompanyList).hasSize(databaseSizeBeforeUpdate);
        UserCompany testUserCompany = userCompanyList.get(userCompanyList.size() - 1);
        assertThat(testUserCompany.getRole()).isEqualTo(UPDATED_ROLE);
        assertThat(testUserCompany.getPermanent()).isEqualTo(UPDATED_PERMANENT);
        assertThat(testUserCompany.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testUserCompany.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testUserCompany.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testUserCompany.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testUserCompany.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void putNonExistingUserCompany() throws Exception {
        int databaseSizeBeforeUpdate = userCompanyRepository.findAll().size();
        userCompany.setId(count.incrementAndGet());

        // Create the UserCompany
        UserCompanyDTO userCompanyDTO = userCompanyMapper.toDto(userCompany);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserCompanyMockMvc
            .perform(
                put(ENTITY_API_URL_ID, userCompanyDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userCompanyDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserCompany in the database
        List<UserCompany> userCompanyList = userCompanyRepository.findAll();
        assertThat(userCompanyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchUserCompany() throws Exception {
        int databaseSizeBeforeUpdate = userCompanyRepository.findAll().size();
        userCompany.setId(count.incrementAndGet());

        // Create the UserCompany
        UserCompanyDTO userCompanyDTO = userCompanyMapper.toDto(userCompany);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserCompanyMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(userCompanyDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserCompany in the database
        List<UserCompany> userCompanyList = userCompanyRepository.findAll();
        assertThat(userCompanyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamUserCompany() throws Exception {
        int databaseSizeBeforeUpdate = userCompanyRepository.findAll().size();
        userCompany.setId(count.incrementAndGet());

        // Create the UserCompany
        UserCompanyDTO userCompanyDTO = userCompanyMapper.toDto(userCompany);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserCompanyMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(userCompanyDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the UserCompany in the database
        List<UserCompany> userCompanyList = userCompanyRepository.findAll();
        assertThat(userCompanyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateUserCompanyWithPatch() throws Exception {
        // Initialize the database
        userCompanyRepository.saveAndFlush(userCompany);

        int databaseSizeBeforeUpdate = userCompanyRepository.findAll().size();

        // Update the userCompany using partial update
        UserCompany partialUpdatedUserCompany = new UserCompany();
        partialUpdatedUserCompany.setId(userCompany.getId());

        partialUpdatedUserCompany.createdDate(UPDATED_CREATED_DATE);

        restUserCompanyMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedUserCompany.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedUserCompany))
            )
            .andExpect(status().isOk());

        // Validate the UserCompany in the database
        List<UserCompany> userCompanyList = userCompanyRepository.findAll();
        assertThat(userCompanyList).hasSize(databaseSizeBeforeUpdate);
        UserCompany testUserCompany = userCompanyList.get(userCompanyList.size() - 1);
        assertThat(testUserCompany.getRole()).isEqualTo(DEFAULT_ROLE);
        assertThat(testUserCompany.getPermanent()).isEqualTo(DEFAULT_PERMANENT);
        assertThat(testUserCompany.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testUserCompany.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testUserCompany.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testUserCompany.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testUserCompany.getVersion()).isEqualTo(DEFAULT_VERSION);
    }

    @Test
    @Transactional
    void fullUpdateUserCompanyWithPatch() throws Exception {
        // Initialize the database
        userCompanyRepository.saveAndFlush(userCompany);

        int databaseSizeBeforeUpdate = userCompanyRepository.findAll().size();

        // Update the userCompany using partial update
        UserCompany partialUpdatedUserCompany = new UserCompany();
        partialUpdatedUserCompany.setId(userCompany.getId());

        partialUpdatedUserCompany
            .role(UPDATED_ROLE)
            .permanent(UPDATED_PERMANENT)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);

        restUserCompanyMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedUserCompany.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedUserCompany))
            )
            .andExpect(status().isOk());

        // Validate the UserCompany in the database
        List<UserCompany> userCompanyList = userCompanyRepository.findAll();
        assertThat(userCompanyList).hasSize(databaseSizeBeforeUpdate);
        UserCompany testUserCompany = userCompanyList.get(userCompanyList.size() - 1);
        assertThat(testUserCompany.getRole()).isEqualTo(UPDATED_ROLE);
        assertThat(testUserCompany.getPermanent()).isEqualTo(UPDATED_PERMANENT);
        assertThat(testUserCompany.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testUserCompany.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testUserCompany.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testUserCompany.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testUserCompany.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void patchNonExistingUserCompany() throws Exception {
        int databaseSizeBeforeUpdate = userCompanyRepository.findAll().size();
        userCompany.setId(count.incrementAndGet());

        // Create the UserCompany
        UserCompanyDTO userCompanyDTO = userCompanyMapper.toDto(userCompany);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restUserCompanyMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, userCompanyDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(userCompanyDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserCompany in the database
        List<UserCompany> userCompanyList = userCompanyRepository.findAll();
        assertThat(userCompanyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchUserCompany() throws Exception {
        int databaseSizeBeforeUpdate = userCompanyRepository.findAll().size();
        userCompany.setId(count.incrementAndGet());

        // Create the UserCompany
        UserCompanyDTO userCompanyDTO = userCompanyMapper.toDto(userCompany);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserCompanyMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(userCompanyDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the UserCompany in the database
        List<UserCompany> userCompanyList = userCompanyRepository.findAll();
        assertThat(userCompanyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamUserCompany() throws Exception {
        int databaseSizeBeforeUpdate = userCompanyRepository.findAll().size();
        userCompany.setId(count.incrementAndGet());

        // Create the UserCompany
        UserCompanyDTO userCompanyDTO = userCompanyMapper.toDto(userCompany);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restUserCompanyMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(userCompanyDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the UserCompany in the database
        List<UserCompany> userCompanyList = userCompanyRepository.findAll();
        assertThat(userCompanyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteUserCompany() throws Exception {
        // Initialize the database
        userCompanyRepository.saveAndFlush(userCompany);

        int databaseSizeBeforeDelete = userCompanyRepository.findAll().size();

        // Delete the userCompany
        restUserCompanyMockMvc
            .perform(delete(ENTITY_API_URL_ID, userCompany.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<UserCompany> userCompanyList = userCompanyRepository.findAll();
        assertThat(userCompanyList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
