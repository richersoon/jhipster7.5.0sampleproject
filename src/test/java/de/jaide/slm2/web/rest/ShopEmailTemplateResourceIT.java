package de.jaide.slm2.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import de.jaide.slm2.IntegrationTest;
import de.jaide.slm2.domain.DefaultEmailTemplate;
import de.jaide.slm2.domain.Shop;
import de.jaide.slm2.domain.ShopEmailTemplate;
import de.jaide.slm2.repository.ShopEmailTemplateRepository;
import de.jaide.slm2.service.dto.ShopEmailTemplateDTO;
import de.jaide.slm2.service.mapper.ShopEmailTemplateMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link ShopEmailTemplateResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ShopEmailTemplateResourceIT {

    private static final String DEFAULT_SUBJECT = "AAAAAAAAAA";
    private static final String UPDATED_SUBJECT = "BBBBBBBBBB";

    private static final String DEFAULT_HEADER = "AAAAAAAAAA";
    private static final String UPDATED_HEADER = "BBBBBBBBBB";

    private static final String DEFAULT_BODY = "AAAAAAAAAA";
    private static final String UPDATED_BODY = "BBBBBBBBBB";

    private static final String DEFAULT_FOOTER = "AAAAAAAAAA";
    private static final String UPDATED_FOOTER = "BBBBBBBBBB";

    private static final String DEFAULT_REPLY_TO = "AAAAAAAAAA";
    private static final String UPDATED_REPLY_TO = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_VERSION = 1L;
    private static final Long UPDATED_VERSION = 2L;

    private static final String ENTITY_API_URL = "/api/shop-email-templates";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ShopEmailTemplateRepository shopEmailTemplateRepository;

    @Autowired
    private ShopEmailTemplateMapper shopEmailTemplateMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restShopEmailTemplateMockMvc;

    private ShopEmailTemplate shopEmailTemplate;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShopEmailTemplate createEntity(EntityManager em) {
        ShopEmailTemplate shopEmailTemplate = new ShopEmailTemplate()
            .subject(DEFAULT_SUBJECT)
            .header(DEFAULT_HEADER)
            .body(DEFAULT_BODY)
            .footer(DEFAULT_FOOTER)
            .replyTo(DEFAULT_REPLY_TO)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .version(DEFAULT_VERSION);
        // Add required entity
        DefaultEmailTemplate defaultEmailTemplate;
        if (TestUtil.findAll(em, DefaultEmailTemplate.class).isEmpty()) {
            defaultEmailTemplate = DefaultEmailTemplateResourceIT.createEntity(em);
            em.persist(defaultEmailTemplate);
            em.flush();
        } else {
            defaultEmailTemplate = TestUtil.findAll(em, DefaultEmailTemplate.class).get(0);
        }
        shopEmailTemplate.setDefaultEmailTemplate(defaultEmailTemplate);
        // Add required entity
        Shop shop;
        if (TestUtil.findAll(em, Shop.class).isEmpty()) {
            shop = ShopResourceIT.createEntity(em);
            em.persist(shop);
            em.flush();
        } else {
            shop = TestUtil.findAll(em, Shop.class).get(0);
        }
        shopEmailTemplate.setShop(shop);
        return shopEmailTemplate;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShopEmailTemplate createUpdatedEntity(EntityManager em) {
        ShopEmailTemplate shopEmailTemplate = new ShopEmailTemplate()
            .subject(UPDATED_SUBJECT)
            .header(UPDATED_HEADER)
            .body(UPDATED_BODY)
            .footer(UPDATED_FOOTER)
            .replyTo(UPDATED_REPLY_TO)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);
        // Add required entity
        DefaultEmailTemplate defaultEmailTemplate;
        if (TestUtil.findAll(em, DefaultEmailTemplate.class).isEmpty()) {
            defaultEmailTemplate = DefaultEmailTemplateResourceIT.createUpdatedEntity(em);
            em.persist(defaultEmailTemplate);
            em.flush();
        } else {
            defaultEmailTemplate = TestUtil.findAll(em, DefaultEmailTemplate.class).get(0);
        }
        shopEmailTemplate.setDefaultEmailTemplate(defaultEmailTemplate);
        // Add required entity
        Shop shop;
        if (TestUtil.findAll(em, Shop.class).isEmpty()) {
            shop = ShopResourceIT.createUpdatedEntity(em);
            em.persist(shop);
            em.flush();
        } else {
            shop = TestUtil.findAll(em, Shop.class).get(0);
        }
        shopEmailTemplate.setShop(shop);
        return shopEmailTemplate;
    }

    @BeforeEach
    public void initTest() {
        shopEmailTemplate = createEntity(em);
    }

    @Test
    @Transactional
    void createShopEmailTemplate() throws Exception {
        int databaseSizeBeforeCreate = shopEmailTemplateRepository.findAll().size();
        // Create the ShopEmailTemplate
        ShopEmailTemplateDTO shopEmailTemplateDTO = shopEmailTemplateMapper.toDto(shopEmailTemplate);
        restShopEmailTemplateMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(shopEmailTemplateDTO))
            )
            .andExpect(status().isCreated());

        // Validate the ShopEmailTemplate in the database
        List<ShopEmailTemplate> shopEmailTemplateList = shopEmailTemplateRepository.findAll();
        assertThat(shopEmailTemplateList).hasSize(databaseSizeBeforeCreate + 1);
        ShopEmailTemplate testShopEmailTemplate = shopEmailTemplateList.get(shopEmailTemplateList.size() - 1);
        assertThat(testShopEmailTemplate.getSubject()).isEqualTo(DEFAULT_SUBJECT);
        assertThat(testShopEmailTemplate.getHeader()).isEqualTo(DEFAULT_HEADER);
        assertThat(testShopEmailTemplate.getBody()).isEqualTo(DEFAULT_BODY);
        assertThat(testShopEmailTemplate.getFooter()).isEqualTo(DEFAULT_FOOTER);
        assertThat(testShopEmailTemplate.getReplyTo()).isEqualTo(DEFAULT_REPLY_TO);
        assertThat(testShopEmailTemplate.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testShopEmailTemplate.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testShopEmailTemplate.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testShopEmailTemplate.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testShopEmailTemplate.getVersion()).isEqualTo(DEFAULT_VERSION);
    }

    @Test
    @Transactional
    void createShopEmailTemplateWithExistingId() throws Exception {
        // Create the ShopEmailTemplate with an existing ID
        shopEmailTemplate.setId(1L);
        ShopEmailTemplateDTO shopEmailTemplateDTO = shopEmailTemplateMapper.toDto(shopEmailTemplate);

        int databaseSizeBeforeCreate = shopEmailTemplateRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restShopEmailTemplateMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(shopEmailTemplateDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ShopEmailTemplate in the database
        List<ShopEmailTemplate> shopEmailTemplateList = shopEmailTemplateRepository.findAll();
        assertThat(shopEmailTemplateList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkSubjectIsRequired() throws Exception {
        int databaseSizeBeforeTest = shopEmailTemplateRepository.findAll().size();
        // set the field null
        shopEmailTemplate.setSubject(null);

        // Create the ShopEmailTemplate, which fails.
        ShopEmailTemplateDTO shopEmailTemplateDTO = shopEmailTemplateMapper.toDto(shopEmailTemplate);

        restShopEmailTemplateMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(shopEmailTemplateDTO))
            )
            .andExpect(status().isBadRequest());

        List<ShopEmailTemplate> shopEmailTemplateList = shopEmailTemplateRepository.findAll();
        assertThat(shopEmailTemplateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkReplyToIsRequired() throws Exception {
        int databaseSizeBeforeTest = shopEmailTemplateRepository.findAll().size();
        // set the field null
        shopEmailTemplate.setReplyTo(null);

        // Create the ShopEmailTemplate, which fails.
        ShopEmailTemplateDTO shopEmailTemplateDTO = shopEmailTemplateMapper.toDto(shopEmailTemplate);

        restShopEmailTemplateMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(shopEmailTemplateDTO))
            )
            .andExpect(status().isBadRequest());

        List<ShopEmailTemplate> shopEmailTemplateList = shopEmailTemplateRepository.findAll();
        assertThat(shopEmailTemplateList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllShopEmailTemplates() throws Exception {
        // Initialize the database
        shopEmailTemplateRepository.saveAndFlush(shopEmailTemplate);

        // Get all the shopEmailTemplateList
        restShopEmailTemplateMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shopEmailTemplate.getId().intValue())))
            .andExpect(jsonPath("$.[*].subject").value(hasItem(DEFAULT_SUBJECT)))
            .andExpect(jsonPath("$.[*].header").value(hasItem(DEFAULT_HEADER.toString())))
            .andExpect(jsonPath("$.[*].body").value(hasItem(DEFAULT_BODY.toString())))
            .andExpect(jsonPath("$.[*].footer").value(hasItem(DEFAULT_FOOTER.toString())))
            .andExpect(jsonPath("$.[*].replyTo").value(hasItem(DEFAULT_REPLY_TO)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.intValue())));
    }

    @Test
    @Transactional
    void getShopEmailTemplate() throws Exception {
        // Initialize the database
        shopEmailTemplateRepository.saveAndFlush(shopEmailTemplate);

        // Get the shopEmailTemplate
        restShopEmailTemplateMockMvc
            .perform(get(ENTITY_API_URL_ID, shopEmailTemplate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(shopEmailTemplate.getId().intValue()))
            .andExpect(jsonPath("$.subject").value(DEFAULT_SUBJECT))
            .andExpect(jsonPath("$.header").value(DEFAULT_HEADER.toString()))
            .andExpect(jsonPath("$.body").value(DEFAULT_BODY.toString()))
            .andExpect(jsonPath("$.footer").value(DEFAULT_FOOTER.toString()))
            .andExpect(jsonPath("$.replyTo").value(DEFAULT_REPLY_TO))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION.intValue()));
    }

    @Test
    @Transactional
    void getNonExistingShopEmailTemplate() throws Exception {
        // Get the shopEmailTemplate
        restShopEmailTemplateMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewShopEmailTemplate() throws Exception {
        // Initialize the database
        shopEmailTemplateRepository.saveAndFlush(shopEmailTemplate);

        int databaseSizeBeforeUpdate = shopEmailTemplateRepository.findAll().size();

        // Update the shopEmailTemplate
        ShopEmailTemplate updatedShopEmailTemplate = shopEmailTemplateRepository.findById(shopEmailTemplate.getId()).get();
        // Disconnect from session so that the updates on updatedShopEmailTemplate are not directly saved in db
        em.detach(updatedShopEmailTemplate);
        updatedShopEmailTemplate
            .subject(UPDATED_SUBJECT)
            .header(UPDATED_HEADER)
            .body(UPDATED_BODY)
            .footer(UPDATED_FOOTER)
            .replyTo(UPDATED_REPLY_TO)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);
        ShopEmailTemplateDTO shopEmailTemplateDTO = shopEmailTemplateMapper.toDto(updatedShopEmailTemplate);

        restShopEmailTemplateMockMvc
            .perform(
                put(ENTITY_API_URL_ID, shopEmailTemplateDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(shopEmailTemplateDTO))
            )
            .andExpect(status().isOk());

        // Validate the ShopEmailTemplate in the database
        List<ShopEmailTemplate> shopEmailTemplateList = shopEmailTemplateRepository.findAll();
        assertThat(shopEmailTemplateList).hasSize(databaseSizeBeforeUpdate);
        ShopEmailTemplate testShopEmailTemplate = shopEmailTemplateList.get(shopEmailTemplateList.size() - 1);
        assertThat(testShopEmailTemplate.getSubject()).isEqualTo(UPDATED_SUBJECT);
        assertThat(testShopEmailTemplate.getHeader()).isEqualTo(UPDATED_HEADER);
        assertThat(testShopEmailTemplate.getBody()).isEqualTo(UPDATED_BODY);
        assertThat(testShopEmailTemplate.getFooter()).isEqualTo(UPDATED_FOOTER);
        assertThat(testShopEmailTemplate.getReplyTo()).isEqualTo(UPDATED_REPLY_TO);
        assertThat(testShopEmailTemplate.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testShopEmailTemplate.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testShopEmailTemplate.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testShopEmailTemplate.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testShopEmailTemplate.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void putNonExistingShopEmailTemplate() throws Exception {
        int databaseSizeBeforeUpdate = shopEmailTemplateRepository.findAll().size();
        shopEmailTemplate.setId(count.incrementAndGet());

        // Create the ShopEmailTemplate
        ShopEmailTemplateDTO shopEmailTemplateDTO = shopEmailTemplateMapper.toDto(shopEmailTemplate);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restShopEmailTemplateMockMvc
            .perform(
                put(ENTITY_API_URL_ID, shopEmailTemplateDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(shopEmailTemplateDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ShopEmailTemplate in the database
        List<ShopEmailTemplate> shopEmailTemplateList = shopEmailTemplateRepository.findAll();
        assertThat(shopEmailTemplateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchShopEmailTemplate() throws Exception {
        int databaseSizeBeforeUpdate = shopEmailTemplateRepository.findAll().size();
        shopEmailTemplate.setId(count.incrementAndGet());

        // Create the ShopEmailTemplate
        ShopEmailTemplateDTO shopEmailTemplateDTO = shopEmailTemplateMapper.toDto(shopEmailTemplate);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restShopEmailTemplateMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(shopEmailTemplateDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ShopEmailTemplate in the database
        List<ShopEmailTemplate> shopEmailTemplateList = shopEmailTemplateRepository.findAll();
        assertThat(shopEmailTemplateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamShopEmailTemplate() throws Exception {
        int databaseSizeBeforeUpdate = shopEmailTemplateRepository.findAll().size();
        shopEmailTemplate.setId(count.incrementAndGet());

        // Create the ShopEmailTemplate
        ShopEmailTemplateDTO shopEmailTemplateDTO = shopEmailTemplateMapper.toDto(shopEmailTemplate);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restShopEmailTemplateMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(shopEmailTemplateDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ShopEmailTemplate in the database
        List<ShopEmailTemplate> shopEmailTemplateList = shopEmailTemplateRepository.findAll();
        assertThat(shopEmailTemplateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateShopEmailTemplateWithPatch() throws Exception {
        // Initialize the database
        shopEmailTemplateRepository.saveAndFlush(shopEmailTemplate);

        int databaseSizeBeforeUpdate = shopEmailTemplateRepository.findAll().size();

        // Update the shopEmailTemplate using partial update
        ShopEmailTemplate partialUpdatedShopEmailTemplate = new ShopEmailTemplate();
        partialUpdatedShopEmailTemplate.setId(shopEmailTemplate.getId());

        partialUpdatedShopEmailTemplate.header(UPDATED_HEADER).body(UPDATED_BODY);

        restShopEmailTemplateMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedShopEmailTemplate.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedShopEmailTemplate))
            )
            .andExpect(status().isOk());

        // Validate the ShopEmailTemplate in the database
        List<ShopEmailTemplate> shopEmailTemplateList = shopEmailTemplateRepository.findAll();
        assertThat(shopEmailTemplateList).hasSize(databaseSizeBeforeUpdate);
        ShopEmailTemplate testShopEmailTemplate = shopEmailTemplateList.get(shopEmailTemplateList.size() - 1);
        assertThat(testShopEmailTemplate.getSubject()).isEqualTo(DEFAULT_SUBJECT);
        assertThat(testShopEmailTemplate.getHeader()).isEqualTo(UPDATED_HEADER);
        assertThat(testShopEmailTemplate.getBody()).isEqualTo(UPDATED_BODY);
        assertThat(testShopEmailTemplate.getFooter()).isEqualTo(DEFAULT_FOOTER);
        assertThat(testShopEmailTemplate.getReplyTo()).isEqualTo(DEFAULT_REPLY_TO);
        assertThat(testShopEmailTemplate.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testShopEmailTemplate.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testShopEmailTemplate.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testShopEmailTemplate.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testShopEmailTemplate.getVersion()).isEqualTo(DEFAULT_VERSION);
    }

    @Test
    @Transactional
    void fullUpdateShopEmailTemplateWithPatch() throws Exception {
        // Initialize the database
        shopEmailTemplateRepository.saveAndFlush(shopEmailTemplate);

        int databaseSizeBeforeUpdate = shopEmailTemplateRepository.findAll().size();

        // Update the shopEmailTemplate using partial update
        ShopEmailTemplate partialUpdatedShopEmailTemplate = new ShopEmailTemplate();
        partialUpdatedShopEmailTemplate.setId(shopEmailTemplate.getId());

        partialUpdatedShopEmailTemplate
            .subject(UPDATED_SUBJECT)
            .header(UPDATED_HEADER)
            .body(UPDATED_BODY)
            .footer(UPDATED_FOOTER)
            .replyTo(UPDATED_REPLY_TO)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);

        restShopEmailTemplateMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedShopEmailTemplate.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedShopEmailTemplate))
            )
            .andExpect(status().isOk());

        // Validate the ShopEmailTemplate in the database
        List<ShopEmailTemplate> shopEmailTemplateList = shopEmailTemplateRepository.findAll();
        assertThat(shopEmailTemplateList).hasSize(databaseSizeBeforeUpdate);
        ShopEmailTemplate testShopEmailTemplate = shopEmailTemplateList.get(shopEmailTemplateList.size() - 1);
        assertThat(testShopEmailTemplate.getSubject()).isEqualTo(UPDATED_SUBJECT);
        assertThat(testShopEmailTemplate.getHeader()).isEqualTo(UPDATED_HEADER);
        assertThat(testShopEmailTemplate.getBody()).isEqualTo(UPDATED_BODY);
        assertThat(testShopEmailTemplate.getFooter()).isEqualTo(UPDATED_FOOTER);
        assertThat(testShopEmailTemplate.getReplyTo()).isEqualTo(UPDATED_REPLY_TO);
        assertThat(testShopEmailTemplate.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testShopEmailTemplate.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testShopEmailTemplate.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testShopEmailTemplate.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testShopEmailTemplate.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void patchNonExistingShopEmailTemplate() throws Exception {
        int databaseSizeBeforeUpdate = shopEmailTemplateRepository.findAll().size();
        shopEmailTemplate.setId(count.incrementAndGet());

        // Create the ShopEmailTemplate
        ShopEmailTemplateDTO shopEmailTemplateDTO = shopEmailTemplateMapper.toDto(shopEmailTemplate);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restShopEmailTemplateMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, shopEmailTemplateDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(shopEmailTemplateDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ShopEmailTemplate in the database
        List<ShopEmailTemplate> shopEmailTemplateList = shopEmailTemplateRepository.findAll();
        assertThat(shopEmailTemplateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchShopEmailTemplate() throws Exception {
        int databaseSizeBeforeUpdate = shopEmailTemplateRepository.findAll().size();
        shopEmailTemplate.setId(count.incrementAndGet());

        // Create the ShopEmailTemplate
        ShopEmailTemplateDTO shopEmailTemplateDTO = shopEmailTemplateMapper.toDto(shopEmailTemplate);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restShopEmailTemplateMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(shopEmailTemplateDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ShopEmailTemplate in the database
        List<ShopEmailTemplate> shopEmailTemplateList = shopEmailTemplateRepository.findAll();
        assertThat(shopEmailTemplateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamShopEmailTemplate() throws Exception {
        int databaseSizeBeforeUpdate = shopEmailTemplateRepository.findAll().size();
        shopEmailTemplate.setId(count.incrementAndGet());

        // Create the ShopEmailTemplate
        ShopEmailTemplateDTO shopEmailTemplateDTO = shopEmailTemplateMapper.toDto(shopEmailTemplate);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restShopEmailTemplateMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(shopEmailTemplateDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ShopEmailTemplate in the database
        List<ShopEmailTemplate> shopEmailTemplateList = shopEmailTemplateRepository.findAll();
        assertThat(shopEmailTemplateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteShopEmailTemplate() throws Exception {
        // Initialize the database
        shopEmailTemplateRepository.saveAndFlush(shopEmailTemplate);

        int databaseSizeBeforeDelete = shopEmailTemplateRepository.findAll().size();

        // Delete the shopEmailTemplate
        restShopEmailTemplateMockMvc
            .perform(delete(ENTITY_API_URL_ID, shopEmailTemplate.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ShopEmailTemplate> shopEmailTemplateList = shopEmailTemplateRepository.findAll();
        assertThat(shopEmailTemplateList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
