package de.jaide.slm2.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import de.jaide.slm2.IntegrationTest;
import de.jaide.slm2.domain.Shop;
import de.jaide.slm2.domain.ShopSmtp;
import de.jaide.slm2.repository.ShopSmtpRepository;
import de.jaide.slm2.service.dto.ShopSmtpDTO;
import de.jaide.slm2.service.mapper.ShopSmtpMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ShopSmtpResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ShopSmtpResourceIT {

    private static final String DEFAULT_HOST = "AAAAAAAAAA";
    private static final String UPDATED_HOST = "BBBBBBBBBB";

    private static final Integer DEFAULT_PORT = 1;
    private static final Integer UPDATED_PORT = 2;

    private static final String DEFAULT_FROM_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_FROM_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_FROM_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FROM_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_REPLY_TO = "AAAAAAAAAA";
    private static final String UPDATED_REPLY_TO = "BBBBBBBBBB";

    private static final String DEFAULT_USERNAME = "AAAAAAAAAA";
    private static final String UPDATED_USERNAME = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_VERSION = 1L;
    private static final Long UPDATED_VERSION = 2L;

    private static final String ENTITY_API_URL = "/api/shop-smtps";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ShopSmtpRepository shopSmtpRepository;

    @Autowired
    private ShopSmtpMapper shopSmtpMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restShopSmtpMockMvc;

    private ShopSmtp shopSmtp;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShopSmtp createEntity(EntityManager em) {
        ShopSmtp shopSmtp = new ShopSmtp()
            .host(DEFAULT_HOST)
            .port(DEFAULT_PORT)
            .fromEmail(DEFAULT_FROM_EMAIL)
            .fromName(DEFAULT_FROM_NAME)
            .replyTo(DEFAULT_REPLY_TO)
            .username(DEFAULT_USERNAME)
            .password(DEFAULT_PASSWORD)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .version(DEFAULT_VERSION);
        // Add required entity
        Shop shop;
        if (TestUtil.findAll(em, Shop.class).isEmpty()) {
            shop = ShopResourceIT.createEntity(em);
            em.persist(shop);
            em.flush();
        } else {
            shop = TestUtil.findAll(em, Shop.class).get(0);
        }
        shopSmtp.setShop(shop);
        return shopSmtp;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShopSmtp createUpdatedEntity(EntityManager em) {
        ShopSmtp shopSmtp = new ShopSmtp()
            .host(UPDATED_HOST)
            .port(UPDATED_PORT)
            .fromEmail(UPDATED_FROM_EMAIL)
            .fromName(UPDATED_FROM_NAME)
            .replyTo(UPDATED_REPLY_TO)
            .username(UPDATED_USERNAME)
            .password(UPDATED_PASSWORD)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);
        // Add required entity
        Shop shop;
        if (TestUtil.findAll(em, Shop.class).isEmpty()) {
            shop = ShopResourceIT.createUpdatedEntity(em);
            em.persist(shop);
            em.flush();
        } else {
            shop = TestUtil.findAll(em, Shop.class).get(0);
        }
        shopSmtp.setShop(shop);
        return shopSmtp;
    }

    @BeforeEach
    public void initTest() {
        shopSmtp = createEntity(em);
    }

    @Test
    @Transactional
    void createShopSmtp() throws Exception {
        int databaseSizeBeforeCreate = shopSmtpRepository.findAll().size();
        // Create the ShopSmtp
        ShopSmtpDTO shopSmtpDTO = shopSmtpMapper.toDto(shopSmtp);
        restShopSmtpMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(shopSmtpDTO)))
            .andExpect(status().isCreated());

        // Validate the ShopSmtp in the database
        List<ShopSmtp> shopSmtpList = shopSmtpRepository.findAll();
        assertThat(shopSmtpList).hasSize(databaseSizeBeforeCreate + 1);
        ShopSmtp testShopSmtp = shopSmtpList.get(shopSmtpList.size() - 1);
        assertThat(testShopSmtp.getHost()).isEqualTo(DEFAULT_HOST);
        assertThat(testShopSmtp.getPort()).isEqualTo(DEFAULT_PORT);
        assertThat(testShopSmtp.getFromEmail()).isEqualTo(DEFAULT_FROM_EMAIL);
        assertThat(testShopSmtp.getFromName()).isEqualTo(DEFAULT_FROM_NAME);
        assertThat(testShopSmtp.getReplyTo()).isEqualTo(DEFAULT_REPLY_TO);
        assertThat(testShopSmtp.getUsername()).isEqualTo(DEFAULT_USERNAME);
        assertThat(testShopSmtp.getPassword()).isEqualTo(DEFAULT_PASSWORD);
        assertThat(testShopSmtp.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testShopSmtp.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testShopSmtp.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testShopSmtp.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testShopSmtp.getVersion()).isEqualTo(DEFAULT_VERSION);
    }

    @Test
    @Transactional
    void createShopSmtpWithExistingId() throws Exception {
        // Create the ShopSmtp with an existing ID
        shopSmtp.setId(1L);
        ShopSmtpDTO shopSmtpDTO = shopSmtpMapper.toDto(shopSmtp);

        int databaseSizeBeforeCreate = shopSmtpRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restShopSmtpMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(shopSmtpDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ShopSmtp in the database
        List<ShopSmtp> shopSmtpList = shopSmtpRepository.findAll();
        assertThat(shopSmtpList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkHostIsRequired() throws Exception {
        int databaseSizeBeforeTest = shopSmtpRepository.findAll().size();
        // set the field null
        shopSmtp.setHost(null);

        // Create the ShopSmtp, which fails.
        ShopSmtpDTO shopSmtpDTO = shopSmtpMapper.toDto(shopSmtp);

        restShopSmtpMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(shopSmtpDTO)))
            .andExpect(status().isBadRequest());

        List<ShopSmtp> shopSmtpList = shopSmtpRepository.findAll();
        assertThat(shopSmtpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPortIsRequired() throws Exception {
        int databaseSizeBeforeTest = shopSmtpRepository.findAll().size();
        // set the field null
        shopSmtp.setPort(null);

        // Create the ShopSmtp, which fails.
        ShopSmtpDTO shopSmtpDTO = shopSmtpMapper.toDto(shopSmtp);

        restShopSmtpMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(shopSmtpDTO)))
            .andExpect(status().isBadRequest());

        List<ShopSmtp> shopSmtpList = shopSmtpRepository.findAll();
        assertThat(shopSmtpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkFromEmailIsRequired() throws Exception {
        int databaseSizeBeforeTest = shopSmtpRepository.findAll().size();
        // set the field null
        shopSmtp.setFromEmail(null);

        // Create the ShopSmtp, which fails.
        ShopSmtpDTO shopSmtpDTO = shopSmtpMapper.toDto(shopSmtp);

        restShopSmtpMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(shopSmtpDTO)))
            .andExpect(status().isBadRequest());

        List<ShopSmtp> shopSmtpList = shopSmtpRepository.findAll();
        assertThat(shopSmtpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkFromNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = shopSmtpRepository.findAll().size();
        // set the field null
        shopSmtp.setFromName(null);

        // Create the ShopSmtp, which fails.
        ShopSmtpDTO shopSmtpDTO = shopSmtpMapper.toDto(shopSmtp);

        restShopSmtpMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(shopSmtpDTO)))
            .andExpect(status().isBadRequest());

        List<ShopSmtp> shopSmtpList = shopSmtpRepository.findAll();
        assertThat(shopSmtpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkReplyToIsRequired() throws Exception {
        int databaseSizeBeforeTest = shopSmtpRepository.findAll().size();
        // set the field null
        shopSmtp.setReplyTo(null);

        // Create the ShopSmtp, which fails.
        ShopSmtpDTO shopSmtpDTO = shopSmtpMapper.toDto(shopSmtp);

        restShopSmtpMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(shopSmtpDTO)))
            .andExpect(status().isBadRequest());

        List<ShopSmtp> shopSmtpList = shopSmtpRepository.findAll();
        assertThat(shopSmtpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkUsernameIsRequired() throws Exception {
        int databaseSizeBeforeTest = shopSmtpRepository.findAll().size();
        // set the field null
        shopSmtp.setUsername(null);

        // Create the ShopSmtp, which fails.
        ShopSmtpDTO shopSmtpDTO = shopSmtpMapper.toDto(shopSmtp);

        restShopSmtpMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(shopSmtpDTO)))
            .andExpect(status().isBadRequest());

        List<ShopSmtp> shopSmtpList = shopSmtpRepository.findAll();
        assertThat(shopSmtpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPasswordIsRequired() throws Exception {
        int databaseSizeBeforeTest = shopSmtpRepository.findAll().size();
        // set the field null
        shopSmtp.setPassword(null);

        // Create the ShopSmtp, which fails.
        ShopSmtpDTO shopSmtpDTO = shopSmtpMapper.toDto(shopSmtp);

        restShopSmtpMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(shopSmtpDTO)))
            .andExpect(status().isBadRequest());

        List<ShopSmtp> shopSmtpList = shopSmtpRepository.findAll();
        assertThat(shopSmtpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllShopSmtps() throws Exception {
        // Initialize the database
        shopSmtpRepository.saveAndFlush(shopSmtp);

        // Get all the shopSmtpList
        restShopSmtpMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shopSmtp.getId().intValue())))
            .andExpect(jsonPath("$.[*].host").value(hasItem(DEFAULT_HOST)))
            .andExpect(jsonPath("$.[*].port").value(hasItem(DEFAULT_PORT)))
            .andExpect(jsonPath("$.[*].fromEmail").value(hasItem(DEFAULT_FROM_EMAIL)))
            .andExpect(jsonPath("$.[*].fromName").value(hasItem(DEFAULT_FROM_NAME)))
            .andExpect(jsonPath("$.[*].replyTo").value(hasItem(DEFAULT_REPLY_TO)))
            .andExpect(jsonPath("$.[*].username").value(hasItem(DEFAULT_USERNAME)))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.intValue())));
    }

    @Test
    @Transactional
    void getShopSmtp() throws Exception {
        // Initialize the database
        shopSmtpRepository.saveAndFlush(shopSmtp);

        // Get the shopSmtp
        restShopSmtpMockMvc
            .perform(get(ENTITY_API_URL_ID, shopSmtp.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(shopSmtp.getId().intValue()))
            .andExpect(jsonPath("$.host").value(DEFAULT_HOST))
            .andExpect(jsonPath("$.port").value(DEFAULT_PORT))
            .andExpect(jsonPath("$.fromEmail").value(DEFAULT_FROM_EMAIL))
            .andExpect(jsonPath("$.fromName").value(DEFAULT_FROM_NAME))
            .andExpect(jsonPath("$.replyTo").value(DEFAULT_REPLY_TO))
            .andExpect(jsonPath("$.username").value(DEFAULT_USERNAME))
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION.intValue()));
    }

    @Test
    @Transactional
    void getNonExistingShopSmtp() throws Exception {
        // Get the shopSmtp
        restShopSmtpMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewShopSmtp() throws Exception {
        // Initialize the database
        shopSmtpRepository.saveAndFlush(shopSmtp);

        int databaseSizeBeforeUpdate = shopSmtpRepository.findAll().size();

        // Update the shopSmtp
        ShopSmtp updatedShopSmtp = shopSmtpRepository.findById(shopSmtp.getId()).get();
        // Disconnect from session so that the updates on updatedShopSmtp are not directly saved in db
        em.detach(updatedShopSmtp);
        updatedShopSmtp
            .host(UPDATED_HOST)
            .port(UPDATED_PORT)
            .fromEmail(UPDATED_FROM_EMAIL)
            .fromName(UPDATED_FROM_NAME)
            .replyTo(UPDATED_REPLY_TO)
            .username(UPDATED_USERNAME)
            .password(UPDATED_PASSWORD)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);
        ShopSmtpDTO shopSmtpDTO = shopSmtpMapper.toDto(updatedShopSmtp);

        restShopSmtpMockMvc
            .perform(
                put(ENTITY_API_URL_ID, shopSmtpDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(shopSmtpDTO))
            )
            .andExpect(status().isOk());

        // Validate the ShopSmtp in the database
        List<ShopSmtp> shopSmtpList = shopSmtpRepository.findAll();
        assertThat(shopSmtpList).hasSize(databaseSizeBeforeUpdate);
        ShopSmtp testShopSmtp = shopSmtpList.get(shopSmtpList.size() - 1);
        assertThat(testShopSmtp.getHost()).isEqualTo(UPDATED_HOST);
        assertThat(testShopSmtp.getPort()).isEqualTo(UPDATED_PORT);
        assertThat(testShopSmtp.getFromEmail()).isEqualTo(UPDATED_FROM_EMAIL);
        assertThat(testShopSmtp.getFromName()).isEqualTo(UPDATED_FROM_NAME);
        assertThat(testShopSmtp.getReplyTo()).isEqualTo(UPDATED_REPLY_TO);
        assertThat(testShopSmtp.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testShopSmtp.getPassword()).isEqualTo(UPDATED_PASSWORD);
        assertThat(testShopSmtp.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testShopSmtp.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testShopSmtp.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testShopSmtp.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testShopSmtp.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void putNonExistingShopSmtp() throws Exception {
        int databaseSizeBeforeUpdate = shopSmtpRepository.findAll().size();
        shopSmtp.setId(count.incrementAndGet());

        // Create the ShopSmtp
        ShopSmtpDTO shopSmtpDTO = shopSmtpMapper.toDto(shopSmtp);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restShopSmtpMockMvc
            .perform(
                put(ENTITY_API_URL_ID, shopSmtpDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(shopSmtpDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ShopSmtp in the database
        List<ShopSmtp> shopSmtpList = shopSmtpRepository.findAll();
        assertThat(shopSmtpList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchShopSmtp() throws Exception {
        int databaseSizeBeforeUpdate = shopSmtpRepository.findAll().size();
        shopSmtp.setId(count.incrementAndGet());

        // Create the ShopSmtp
        ShopSmtpDTO shopSmtpDTO = shopSmtpMapper.toDto(shopSmtp);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restShopSmtpMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(shopSmtpDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ShopSmtp in the database
        List<ShopSmtp> shopSmtpList = shopSmtpRepository.findAll();
        assertThat(shopSmtpList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamShopSmtp() throws Exception {
        int databaseSizeBeforeUpdate = shopSmtpRepository.findAll().size();
        shopSmtp.setId(count.incrementAndGet());

        // Create the ShopSmtp
        ShopSmtpDTO shopSmtpDTO = shopSmtpMapper.toDto(shopSmtp);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restShopSmtpMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(shopSmtpDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the ShopSmtp in the database
        List<ShopSmtp> shopSmtpList = shopSmtpRepository.findAll();
        assertThat(shopSmtpList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateShopSmtpWithPatch() throws Exception {
        // Initialize the database
        shopSmtpRepository.saveAndFlush(shopSmtp);

        int databaseSizeBeforeUpdate = shopSmtpRepository.findAll().size();

        // Update the shopSmtp using partial update
        ShopSmtp partialUpdatedShopSmtp = new ShopSmtp();
        partialUpdatedShopSmtp.setId(shopSmtp.getId());

        partialUpdatedShopSmtp
            .host(UPDATED_HOST)
            .port(UPDATED_PORT)
            .fromEmail(UPDATED_FROM_EMAIL)
            .password(UPDATED_PASSWORD)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE);

        restShopSmtpMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedShopSmtp.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedShopSmtp))
            )
            .andExpect(status().isOk());

        // Validate the ShopSmtp in the database
        List<ShopSmtp> shopSmtpList = shopSmtpRepository.findAll();
        assertThat(shopSmtpList).hasSize(databaseSizeBeforeUpdate);
        ShopSmtp testShopSmtp = shopSmtpList.get(shopSmtpList.size() - 1);
        assertThat(testShopSmtp.getHost()).isEqualTo(UPDATED_HOST);
        assertThat(testShopSmtp.getPort()).isEqualTo(UPDATED_PORT);
        assertThat(testShopSmtp.getFromEmail()).isEqualTo(UPDATED_FROM_EMAIL);
        assertThat(testShopSmtp.getFromName()).isEqualTo(DEFAULT_FROM_NAME);
        assertThat(testShopSmtp.getReplyTo()).isEqualTo(DEFAULT_REPLY_TO);
        assertThat(testShopSmtp.getUsername()).isEqualTo(DEFAULT_USERNAME);
        assertThat(testShopSmtp.getPassword()).isEqualTo(UPDATED_PASSWORD);
        assertThat(testShopSmtp.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testShopSmtp.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testShopSmtp.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testShopSmtp.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testShopSmtp.getVersion()).isEqualTo(DEFAULT_VERSION);
    }

    @Test
    @Transactional
    void fullUpdateShopSmtpWithPatch() throws Exception {
        // Initialize the database
        shopSmtpRepository.saveAndFlush(shopSmtp);

        int databaseSizeBeforeUpdate = shopSmtpRepository.findAll().size();

        // Update the shopSmtp using partial update
        ShopSmtp partialUpdatedShopSmtp = new ShopSmtp();
        partialUpdatedShopSmtp.setId(shopSmtp.getId());

        partialUpdatedShopSmtp
            .host(UPDATED_HOST)
            .port(UPDATED_PORT)
            .fromEmail(UPDATED_FROM_EMAIL)
            .fromName(UPDATED_FROM_NAME)
            .replyTo(UPDATED_REPLY_TO)
            .username(UPDATED_USERNAME)
            .password(UPDATED_PASSWORD)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);

        restShopSmtpMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedShopSmtp.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedShopSmtp))
            )
            .andExpect(status().isOk());

        // Validate the ShopSmtp in the database
        List<ShopSmtp> shopSmtpList = shopSmtpRepository.findAll();
        assertThat(shopSmtpList).hasSize(databaseSizeBeforeUpdate);
        ShopSmtp testShopSmtp = shopSmtpList.get(shopSmtpList.size() - 1);
        assertThat(testShopSmtp.getHost()).isEqualTo(UPDATED_HOST);
        assertThat(testShopSmtp.getPort()).isEqualTo(UPDATED_PORT);
        assertThat(testShopSmtp.getFromEmail()).isEqualTo(UPDATED_FROM_EMAIL);
        assertThat(testShopSmtp.getFromName()).isEqualTo(UPDATED_FROM_NAME);
        assertThat(testShopSmtp.getReplyTo()).isEqualTo(UPDATED_REPLY_TO);
        assertThat(testShopSmtp.getUsername()).isEqualTo(UPDATED_USERNAME);
        assertThat(testShopSmtp.getPassword()).isEqualTo(UPDATED_PASSWORD);
        assertThat(testShopSmtp.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testShopSmtp.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testShopSmtp.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testShopSmtp.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testShopSmtp.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void patchNonExistingShopSmtp() throws Exception {
        int databaseSizeBeforeUpdate = shopSmtpRepository.findAll().size();
        shopSmtp.setId(count.incrementAndGet());

        // Create the ShopSmtp
        ShopSmtpDTO shopSmtpDTO = shopSmtpMapper.toDto(shopSmtp);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restShopSmtpMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, shopSmtpDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(shopSmtpDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ShopSmtp in the database
        List<ShopSmtp> shopSmtpList = shopSmtpRepository.findAll();
        assertThat(shopSmtpList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchShopSmtp() throws Exception {
        int databaseSizeBeforeUpdate = shopSmtpRepository.findAll().size();
        shopSmtp.setId(count.incrementAndGet());

        // Create the ShopSmtp
        ShopSmtpDTO shopSmtpDTO = shopSmtpMapper.toDto(shopSmtp);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restShopSmtpMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(shopSmtpDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ShopSmtp in the database
        List<ShopSmtp> shopSmtpList = shopSmtpRepository.findAll();
        assertThat(shopSmtpList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamShopSmtp() throws Exception {
        int databaseSizeBeforeUpdate = shopSmtpRepository.findAll().size();
        shopSmtp.setId(count.incrementAndGet());

        // Create the ShopSmtp
        ShopSmtpDTO shopSmtpDTO = shopSmtpMapper.toDto(shopSmtp);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restShopSmtpMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(shopSmtpDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ShopSmtp in the database
        List<ShopSmtp> shopSmtpList = shopSmtpRepository.findAll();
        assertThat(shopSmtpList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteShopSmtp() throws Exception {
        // Initialize the database
        shopSmtpRepository.saveAndFlush(shopSmtp);

        int databaseSizeBeforeDelete = shopSmtpRepository.findAll().size();

        // Delete the shopSmtp
        restShopSmtpMockMvc
            .perform(delete(ENTITY_API_URL_ID, shopSmtp.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ShopSmtp> shopSmtpList = shopSmtpRepository.findAll();
        assertThat(shopSmtpList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
