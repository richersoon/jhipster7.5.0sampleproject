package de.jaide.slm2.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import de.jaide.slm2.IntegrationTest;
import de.jaide.slm2.domain.EventMessage;
import de.jaide.slm2.domain.enumeration.EventMessageStatus;
import de.jaide.slm2.repository.EventMessageRepository;
import de.jaide.slm2.service.dto.EventMessageDTO;
import de.jaide.slm2.service.mapper.EventMessageMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link EventMessageResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class EventMessageResourceIT {

    private static final String DEFAULT_PAYLOAD = "AAAAAAAAAA";
    private static final String UPDATED_PAYLOAD = "BBBBBBBBBB";

    private static final String DEFAULT_HEADER = "AAAAAAAAAA";
    private static final String UPDATED_HEADER = "BBBBBBBBBB";

    private static final EventMessageStatus DEFAULT_STATUS = EventMessageStatus.PENDING;
    private static final EventMessageStatus UPDATED_STATUS = EventMessageStatus.COMPLETED;

    private static final String DEFAULT_NOTES = "AAAAAAAAAA";
    private static final String UPDATED_NOTES = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_VERSION = 1L;
    private static final Long UPDATED_VERSION = 2L;

    private static final String ENTITY_API_URL = "/api/event-messages";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private EventMessageRepository eventMessageRepository;

    @Autowired
    private EventMessageMapper eventMessageMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEventMessageMockMvc;

    private EventMessage eventMessage;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EventMessage createEntity(EntityManager em) {
        EventMessage eventMessage = new EventMessage()
            .payload(DEFAULT_PAYLOAD)
            .header(DEFAULT_HEADER)
            .status(DEFAULT_STATUS)
            .notes(DEFAULT_NOTES)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .version(DEFAULT_VERSION);
        return eventMessage;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EventMessage createUpdatedEntity(EntityManager em) {
        EventMessage eventMessage = new EventMessage()
            .payload(UPDATED_PAYLOAD)
            .header(UPDATED_HEADER)
            .status(UPDATED_STATUS)
            .notes(UPDATED_NOTES)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);
        return eventMessage;
    }

    @BeforeEach
    public void initTest() {
        eventMessage = createEntity(em);
    }

    @Test
    @Transactional
    void createEventMessage() throws Exception {
        int databaseSizeBeforeCreate = eventMessageRepository.findAll().size();
        // Create the EventMessage
        EventMessageDTO eventMessageDTO = eventMessageMapper.toDto(eventMessage);
        restEventMessageMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(eventMessageDTO))
            )
            .andExpect(status().isCreated());

        // Validate the EventMessage in the database
        List<EventMessage> eventMessageList = eventMessageRepository.findAll();
        assertThat(eventMessageList).hasSize(databaseSizeBeforeCreate + 1);
        EventMessage testEventMessage = eventMessageList.get(eventMessageList.size() - 1);
        assertThat(testEventMessage.getPayload()).isEqualTo(DEFAULT_PAYLOAD);
        assertThat(testEventMessage.getHeader()).isEqualTo(DEFAULT_HEADER);
        assertThat(testEventMessage.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testEventMessage.getNotes()).isEqualTo(DEFAULT_NOTES);
        assertThat(testEventMessage.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testEventMessage.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testEventMessage.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testEventMessage.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testEventMessage.getVersion()).isEqualTo(DEFAULT_VERSION);
    }

    @Test
    @Transactional
    void createEventMessageWithExistingId() throws Exception {
        // Create the EventMessage with an existing ID
        eventMessage.setId(1L);
        EventMessageDTO eventMessageDTO = eventMessageMapper.toDto(eventMessage);

        int databaseSizeBeforeCreate = eventMessageRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restEventMessageMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(eventMessageDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the EventMessage in the database
        List<EventMessage> eventMessageList = eventMessageRepository.findAll();
        assertThat(eventMessageList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = eventMessageRepository.findAll().size();
        // set the field null
        eventMessage.setStatus(null);

        // Create the EventMessage, which fails.
        EventMessageDTO eventMessageDTO = eventMessageMapper.toDto(eventMessage);

        restEventMessageMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(eventMessageDTO))
            )
            .andExpect(status().isBadRequest());

        List<EventMessage> eventMessageList = eventMessageRepository.findAll();
        assertThat(eventMessageList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllEventMessages() throws Exception {
        // Initialize the database
        eventMessageRepository.saveAndFlush(eventMessage);

        // Get all the eventMessageList
        restEventMessageMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(eventMessage.getId().intValue())))
            .andExpect(jsonPath("$.[*].payload").value(hasItem(DEFAULT_PAYLOAD.toString())))
            .andExpect(jsonPath("$.[*].header").value(hasItem(DEFAULT_HEADER.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].notes").value(hasItem(DEFAULT_NOTES.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.intValue())));
    }

    @Test
    @Transactional
    void getEventMessage() throws Exception {
        // Initialize the database
        eventMessageRepository.saveAndFlush(eventMessage);

        // Get the eventMessage
        restEventMessageMockMvc
            .perform(get(ENTITY_API_URL_ID, eventMessage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(eventMessage.getId().intValue()))
            .andExpect(jsonPath("$.payload").value(DEFAULT_PAYLOAD.toString()))
            .andExpect(jsonPath("$.header").value(DEFAULT_HEADER.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.notes").value(DEFAULT_NOTES.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION.intValue()));
    }

    @Test
    @Transactional
    void getNonExistingEventMessage() throws Exception {
        // Get the eventMessage
        restEventMessageMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewEventMessage() throws Exception {
        // Initialize the database
        eventMessageRepository.saveAndFlush(eventMessage);

        int databaseSizeBeforeUpdate = eventMessageRepository.findAll().size();

        // Update the eventMessage
        EventMessage updatedEventMessage = eventMessageRepository.findById(eventMessage.getId()).get();
        // Disconnect from session so that the updates on updatedEventMessage are not directly saved in db
        em.detach(updatedEventMessage);
        updatedEventMessage
            .payload(UPDATED_PAYLOAD)
            .header(UPDATED_HEADER)
            .status(UPDATED_STATUS)
            .notes(UPDATED_NOTES)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);
        EventMessageDTO eventMessageDTO = eventMessageMapper.toDto(updatedEventMessage);

        restEventMessageMockMvc
            .perform(
                put(ENTITY_API_URL_ID, eventMessageDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(eventMessageDTO))
            )
            .andExpect(status().isOk());

        // Validate the EventMessage in the database
        List<EventMessage> eventMessageList = eventMessageRepository.findAll();
        assertThat(eventMessageList).hasSize(databaseSizeBeforeUpdate);
        EventMessage testEventMessage = eventMessageList.get(eventMessageList.size() - 1);
        assertThat(testEventMessage.getPayload()).isEqualTo(UPDATED_PAYLOAD);
        assertThat(testEventMessage.getHeader()).isEqualTo(UPDATED_HEADER);
        assertThat(testEventMessage.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testEventMessage.getNotes()).isEqualTo(UPDATED_NOTES);
        assertThat(testEventMessage.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testEventMessage.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testEventMessage.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testEventMessage.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testEventMessage.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void putNonExistingEventMessage() throws Exception {
        int databaseSizeBeforeUpdate = eventMessageRepository.findAll().size();
        eventMessage.setId(count.incrementAndGet());

        // Create the EventMessage
        EventMessageDTO eventMessageDTO = eventMessageMapper.toDto(eventMessage);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEventMessageMockMvc
            .perform(
                put(ENTITY_API_URL_ID, eventMessageDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(eventMessageDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the EventMessage in the database
        List<EventMessage> eventMessageList = eventMessageRepository.findAll();
        assertThat(eventMessageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchEventMessage() throws Exception {
        int databaseSizeBeforeUpdate = eventMessageRepository.findAll().size();
        eventMessage.setId(count.incrementAndGet());

        // Create the EventMessage
        EventMessageDTO eventMessageDTO = eventMessageMapper.toDto(eventMessage);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEventMessageMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(eventMessageDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the EventMessage in the database
        List<EventMessage> eventMessageList = eventMessageRepository.findAll();
        assertThat(eventMessageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamEventMessage() throws Exception {
        int databaseSizeBeforeUpdate = eventMessageRepository.findAll().size();
        eventMessage.setId(count.incrementAndGet());

        // Create the EventMessage
        EventMessageDTO eventMessageDTO = eventMessageMapper.toDto(eventMessage);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEventMessageMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(eventMessageDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the EventMessage in the database
        List<EventMessage> eventMessageList = eventMessageRepository.findAll();
        assertThat(eventMessageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateEventMessageWithPatch() throws Exception {
        // Initialize the database
        eventMessageRepository.saveAndFlush(eventMessage);

        int databaseSizeBeforeUpdate = eventMessageRepository.findAll().size();

        // Update the eventMessage using partial update
        EventMessage partialUpdatedEventMessage = new EventMessage();
        partialUpdatedEventMessage.setId(eventMessage.getId());

        partialUpdatedEventMessage
            .status(UPDATED_STATUS)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);

        restEventMessageMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEventMessage.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEventMessage))
            )
            .andExpect(status().isOk());

        // Validate the EventMessage in the database
        List<EventMessage> eventMessageList = eventMessageRepository.findAll();
        assertThat(eventMessageList).hasSize(databaseSizeBeforeUpdate);
        EventMessage testEventMessage = eventMessageList.get(eventMessageList.size() - 1);
        assertThat(testEventMessage.getPayload()).isEqualTo(DEFAULT_PAYLOAD);
        assertThat(testEventMessage.getHeader()).isEqualTo(DEFAULT_HEADER);
        assertThat(testEventMessage.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testEventMessage.getNotes()).isEqualTo(DEFAULT_NOTES);
        assertThat(testEventMessage.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testEventMessage.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testEventMessage.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testEventMessage.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testEventMessage.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void fullUpdateEventMessageWithPatch() throws Exception {
        // Initialize the database
        eventMessageRepository.saveAndFlush(eventMessage);

        int databaseSizeBeforeUpdate = eventMessageRepository.findAll().size();

        // Update the eventMessage using partial update
        EventMessage partialUpdatedEventMessage = new EventMessage();
        partialUpdatedEventMessage.setId(eventMessage.getId());

        partialUpdatedEventMessage
            .payload(UPDATED_PAYLOAD)
            .header(UPDATED_HEADER)
            .status(UPDATED_STATUS)
            .notes(UPDATED_NOTES)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);

        restEventMessageMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEventMessage.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEventMessage))
            )
            .andExpect(status().isOk());

        // Validate the EventMessage in the database
        List<EventMessage> eventMessageList = eventMessageRepository.findAll();
        assertThat(eventMessageList).hasSize(databaseSizeBeforeUpdate);
        EventMessage testEventMessage = eventMessageList.get(eventMessageList.size() - 1);
        assertThat(testEventMessage.getPayload()).isEqualTo(UPDATED_PAYLOAD);
        assertThat(testEventMessage.getHeader()).isEqualTo(UPDATED_HEADER);
        assertThat(testEventMessage.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testEventMessage.getNotes()).isEqualTo(UPDATED_NOTES);
        assertThat(testEventMessage.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testEventMessage.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testEventMessage.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testEventMessage.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testEventMessage.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void patchNonExistingEventMessage() throws Exception {
        int databaseSizeBeforeUpdate = eventMessageRepository.findAll().size();
        eventMessage.setId(count.incrementAndGet());

        // Create the EventMessage
        EventMessageDTO eventMessageDTO = eventMessageMapper.toDto(eventMessage);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEventMessageMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, eventMessageDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(eventMessageDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the EventMessage in the database
        List<EventMessage> eventMessageList = eventMessageRepository.findAll();
        assertThat(eventMessageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchEventMessage() throws Exception {
        int databaseSizeBeforeUpdate = eventMessageRepository.findAll().size();
        eventMessage.setId(count.incrementAndGet());

        // Create the EventMessage
        EventMessageDTO eventMessageDTO = eventMessageMapper.toDto(eventMessage);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEventMessageMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(eventMessageDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the EventMessage in the database
        List<EventMessage> eventMessageList = eventMessageRepository.findAll();
        assertThat(eventMessageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamEventMessage() throws Exception {
        int databaseSizeBeforeUpdate = eventMessageRepository.findAll().size();
        eventMessage.setId(count.incrementAndGet());

        // Create the EventMessage
        EventMessageDTO eventMessageDTO = eventMessageMapper.toDto(eventMessage);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEventMessageMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(eventMessageDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the EventMessage in the database
        List<EventMessage> eventMessageList = eventMessageRepository.findAll();
        assertThat(eventMessageList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteEventMessage() throws Exception {
        // Initialize the database
        eventMessageRepository.saveAndFlush(eventMessage);

        int databaseSizeBeforeDelete = eventMessageRepository.findAll().size();

        // Delete the eventMessage
        restEventMessageMockMvc
            .perform(delete(ENTITY_API_URL_ID, eventMessage.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<EventMessage> eventMessageList = eventMessageRepository.findAll();
        assertThat(eventMessageList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
