package de.jaide.slm2.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import de.jaide.slm2.IntegrationTest;
import de.jaide.slm2.domain.Order;
import de.jaide.slm2.domain.OrderNote;
import de.jaide.slm2.repository.OrderNoteRepository;
import de.jaide.slm2.service.dto.OrderNoteDTO;
import de.jaide.slm2.service.mapper.OrderNoteMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link OrderNoteResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class OrderNoteResourceIT {

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_VERSION = 1L;
    private static final Long UPDATED_VERSION = 2L;

    private static final String ENTITY_API_URL = "/api/order-notes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private OrderNoteRepository orderNoteRepository;

    @Autowired
    private OrderNoteMapper orderNoteMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOrderNoteMockMvc;

    private OrderNote orderNote;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderNote createEntity(EntityManager em) {
        OrderNote orderNote = new OrderNote()
            .note(DEFAULT_NOTE)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .version(DEFAULT_VERSION);
        // Add required entity
        Order order;
        if (TestUtil.findAll(em, Order.class).isEmpty()) {
            order = OrderResourceIT.createEntity(em);
            em.persist(order);
            em.flush();
        } else {
            order = TestUtil.findAll(em, Order.class).get(0);
        }
        orderNote.setOrder(order);
        return orderNote;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderNote createUpdatedEntity(EntityManager em) {
        OrderNote orderNote = new OrderNote()
            .note(UPDATED_NOTE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);
        // Add required entity
        Order order;
        if (TestUtil.findAll(em, Order.class).isEmpty()) {
            order = OrderResourceIT.createUpdatedEntity(em);
            em.persist(order);
            em.flush();
        } else {
            order = TestUtil.findAll(em, Order.class).get(0);
        }
        orderNote.setOrder(order);
        return orderNote;
    }

    @BeforeEach
    public void initTest() {
        orderNote = createEntity(em);
    }

    @Test
    @Transactional
    void createOrderNote() throws Exception {
        int databaseSizeBeforeCreate = orderNoteRepository.findAll().size();
        // Create the OrderNote
        OrderNoteDTO orderNoteDTO = orderNoteMapper.toDto(orderNote);
        restOrderNoteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(orderNoteDTO)))
            .andExpect(status().isCreated());

        // Validate the OrderNote in the database
        List<OrderNote> orderNoteList = orderNoteRepository.findAll();
        assertThat(orderNoteList).hasSize(databaseSizeBeforeCreate + 1);
        OrderNote testOrderNote = orderNoteList.get(orderNoteList.size() - 1);
        assertThat(testOrderNote.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testOrderNote.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testOrderNote.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testOrderNote.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testOrderNote.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testOrderNote.getVersion()).isEqualTo(DEFAULT_VERSION);
    }

    @Test
    @Transactional
    void createOrderNoteWithExistingId() throws Exception {
        // Create the OrderNote with an existing ID
        orderNote.setId(1L);
        OrderNoteDTO orderNoteDTO = orderNoteMapper.toDto(orderNote);

        int databaseSizeBeforeCreate = orderNoteRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrderNoteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(orderNoteDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OrderNote in the database
        List<OrderNote> orderNoteList = orderNoteRepository.findAll();
        assertThat(orderNoteList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllOrderNotes() throws Exception {
        // Initialize the database
        orderNoteRepository.saveAndFlush(orderNote);

        // Get all the orderNoteList
        restOrderNoteMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(orderNote.getId().intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.intValue())));
    }

    @Test
    @Transactional
    void getOrderNote() throws Exception {
        // Initialize the database
        orderNoteRepository.saveAndFlush(orderNote);

        // Get the orderNote
        restOrderNoteMockMvc
            .perform(get(ENTITY_API_URL_ID, orderNote.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(orderNote.getId().intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION.intValue()));
    }

    @Test
    @Transactional
    void getNonExistingOrderNote() throws Exception {
        // Get the orderNote
        restOrderNoteMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewOrderNote() throws Exception {
        // Initialize the database
        orderNoteRepository.saveAndFlush(orderNote);

        int databaseSizeBeforeUpdate = orderNoteRepository.findAll().size();

        // Update the orderNote
        OrderNote updatedOrderNote = orderNoteRepository.findById(orderNote.getId()).get();
        // Disconnect from session so that the updates on updatedOrderNote are not directly saved in db
        em.detach(updatedOrderNote);
        updatedOrderNote
            .note(UPDATED_NOTE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);
        OrderNoteDTO orderNoteDTO = orderNoteMapper.toDto(updatedOrderNote);

        restOrderNoteMockMvc
            .perform(
                put(ENTITY_API_URL_ID, orderNoteDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(orderNoteDTO))
            )
            .andExpect(status().isOk());

        // Validate the OrderNote in the database
        List<OrderNote> orderNoteList = orderNoteRepository.findAll();
        assertThat(orderNoteList).hasSize(databaseSizeBeforeUpdate);
        OrderNote testOrderNote = orderNoteList.get(orderNoteList.size() - 1);
        assertThat(testOrderNote.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testOrderNote.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testOrderNote.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testOrderNote.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testOrderNote.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testOrderNote.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void putNonExistingOrderNote() throws Exception {
        int databaseSizeBeforeUpdate = orderNoteRepository.findAll().size();
        orderNote.setId(count.incrementAndGet());

        // Create the OrderNote
        OrderNoteDTO orderNoteDTO = orderNoteMapper.toDto(orderNote);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrderNoteMockMvc
            .perform(
                put(ENTITY_API_URL_ID, orderNoteDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(orderNoteDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrderNote in the database
        List<OrderNote> orderNoteList = orderNoteRepository.findAll();
        assertThat(orderNoteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchOrderNote() throws Exception {
        int databaseSizeBeforeUpdate = orderNoteRepository.findAll().size();
        orderNote.setId(count.incrementAndGet());

        // Create the OrderNote
        OrderNoteDTO orderNoteDTO = orderNoteMapper.toDto(orderNote);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrderNoteMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(orderNoteDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrderNote in the database
        List<OrderNote> orderNoteList = orderNoteRepository.findAll();
        assertThat(orderNoteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamOrderNote() throws Exception {
        int databaseSizeBeforeUpdate = orderNoteRepository.findAll().size();
        orderNote.setId(count.incrementAndGet());

        // Create the OrderNote
        OrderNoteDTO orderNoteDTO = orderNoteMapper.toDto(orderNote);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrderNoteMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(orderNoteDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the OrderNote in the database
        List<OrderNote> orderNoteList = orderNoteRepository.findAll();
        assertThat(orderNoteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateOrderNoteWithPatch() throws Exception {
        // Initialize the database
        orderNoteRepository.saveAndFlush(orderNote);

        int databaseSizeBeforeUpdate = orderNoteRepository.findAll().size();

        // Update the orderNote using partial update
        OrderNote partialUpdatedOrderNote = new OrderNote();
        partialUpdatedOrderNote.setId(orderNote.getId());

        partialUpdatedOrderNote.note(UPDATED_NOTE).version(UPDATED_VERSION);

        restOrderNoteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOrderNote.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOrderNote))
            )
            .andExpect(status().isOk());

        // Validate the OrderNote in the database
        List<OrderNote> orderNoteList = orderNoteRepository.findAll();
        assertThat(orderNoteList).hasSize(databaseSizeBeforeUpdate);
        OrderNote testOrderNote = orderNoteList.get(orderNoteList.size() - 1);
        assertThat(testOrderNote.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testOrderNote.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testOrderNote.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testOrderNote.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testOrderNote.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testOrderNote.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void fullUpdateOrderNoteWithPatch() throws Exception {
        // Initialize the database
        orderNoteRepository.saveAndFlush(orderNote);

        int databaseSizeBeforeUpdate = orderNoteRepository.findAll().size();

        // Update the orderNote using partial update
        OrderNote partialUpdatedOrderNote = new OrderNote();
        partialUpdatedOrderNote.setId(orderNote.getId());

        partialUpdatedOrderNote
            .note(UPDATED_NOTE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);

        restOrderNoteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOrderNote.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOrderNote))
            )
            .andExpect(status().isOk());

        // Validate the OrderNote in the database
        List<OrderNote> orderNoteList = orderNoteRepository.findAll();
        assertThat(orderNoteList).hasSize(databaseSizeBeforeUpdate);
        OrderNote testOrderNote = orderNoteList.get(orderNoteList.size() - 1);
        assertThat(testOrderNote.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testOrderNote.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testOrderNote.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testOrderNote.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testOrderNote.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testOrderNote.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void patchNonExistingOrderNote() throws Exception {
        int databaseSizeBeforeUpdate = orderNoteRepository.findAll().size();
        orderNote.setId(count.incrementAndGet());

        // Create the OrderNote
        OrderNoteDTO orderNoteDTO = orderNoteMapper.toDto(orderNote);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrderNoteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, orderNoteDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(orderNoteDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrderNote in the database
        List<OrderNote> orderNoteList = orderNoteRepository.findAll();
        assertThat(orderNoteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchOrderNote() throws Exception {
        int databaseSizeBeforeUpdate = orderNoteRepository.findAll().size();
        orderNote.setId(count.incrementAndGet());

        // Create the OrderNote
        OrderNoteDTO orderNoteDTO = orderNoteMapper.toDto(orderNote);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrderNoteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(orderNoteDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrderNote in the database
        List<OrderNote> orderNoteList = orderNoteRepository.findAll();
        assertThat(orderNoteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamOrderNote() throws Exception {
        int databaseSizeBeforeUpdate = orderNoteRepository.findAll().size();
        orderNote.setId(count.incrementAndGet());

        // Create the OrderNote
        OrderNoteDTO orderNoteDTO = orderNoteMapper.toDto(orderNote);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrderNoteMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(orderNoteDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the OrderNote in the database
        List<OrderNote> orderNoteList = orderNoteRepository.findAll();
        assertThat(orderNoteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteOrderNote() throws Exception {
        // Initialize the database
        orderNoteRepository.saveAndFlush(orderNote);

        int databaseSizeBeforeDelete = orderNoteRepository.findAll().size();

        // Delete the orderNote
        restOrderNoteMockMvc
            .perform(delete(ENTITY_API_URL_ID, orderNote.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OrderNote> orderNoteList = orderNoteRepository.findAll();
        assertThat(orderNoteList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
