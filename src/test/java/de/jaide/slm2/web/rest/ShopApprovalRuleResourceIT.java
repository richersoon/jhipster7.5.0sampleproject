package de.jaide.slm2.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import de.jaide.slm2.IntegrationTest;
import de.jaide.slm2.domain.Shop;
import de.jaide.slm2.domain.ShopApprovalRule;
import de.jaide.slm2.domain.enumeration.ApprovalRule;
import de.jaide.slm2.repository.ShopApprovalRuleRepository;
import de.jaide.slm2.service.dto.ShopApprovalRuleDTO;
import de.jaide.slm2.service.mapper.ShopApprovalRuleMapper;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ShopApprovalRuleResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ShopApprovalRuleResourceIT {

    private static final ApprovalRule DEFAULT_APPROVAL_RULE = ApprovalRule.QUANTITY;
    private static final ApprovalRule UPDATED_APPROVAL_RULE = ApprovalRule.PRICE;

    private static final String DEFAULT_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_VALUE = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_VERSION = 1L;
    private static final Long UPDATED_VERSION = 2L;

    private static final String ENTITY_API_URL = "/api/shop-approval-rules";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ShopApprovalRuleRepository shopApprovalRuleRepository;

    @Autowired
    private ShopApprovalRuleMapper shopApprovalRuleMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restShopApprovalRuleMockMvc;

    private ShopApprovalRule shopApprovalRule;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShopApprovalRule createEntity(EntityManager em) {
        ShopApprovalRule shopApprovalRule = new ShopApprovalRule()
            .approvalRule(DEFAULT_APPROVAL_RULE)
            .value(DEFAULT_VALUE)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .version(DEFAULT_VERSION);
        // Add required entity
        Shop shop;
        if (TestUtil.findAll(em, Shop.class).isEmpty()) {
            shop = ShopResourceIT.createEntity(em);
            em.persist(shop);
            em.flush();
        } else {
            shop = TestUtil.findAll(em, Shop.class).get(0);
        }
        shopApprovalRule.setShop(shop);
        return shopApprovalRule;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ShopApprovalRule createUpdatedEntity(EntityManager em) {
        ShopApprovalRule shopApprovalRule = new ShopApprovalRule()
            .approvalRule(UPDATED_APPROVAL_RULE)
            .value(UPDATED_VALUE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);
        // Add required entity
        Shop shop;
        if (TestUtil.findAll(em, Shop.class).isEmpty()) {
            shop = ShopResourceIT.createUpdatedEntity(em);
            em.persist(shop);
            em.flush();
        } else {
            shop = TestUtil.findAll(em, Shop.class).get(0);
        }
        shopApprovalRule.setShop(shop);
        return shopApprovalRule;
    }

    @BeforeEach
    public void initTest() {
        shopApprovalRule = createEntity(em);
    }

    @Test
    @Transactional
    void createShopApprovalRule() throws Exception {
        int databaseSizeBeforeCreate = shopApprovalRuleRepository.findAll().size();
        // Create the ShopApprovalRule
        ShopApprovalRuleDTO shopApprovalRuleDTO = shopApprovalRuleMapper.toDto(shopApprovalRule);
        restShopApprovalRuleMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(shopApprovalRuleDTO))
            )
            .andExpect(status().isCreated());

        // Validate the ShopApprovalRule in the database
        List<ShopApprovalRule> shopApprovalRuleList = shopApprovalRuleRepository.findAll();
        assertThat(shopApprovalRuleList).hasSize(databaseSizeBeforeCreate + 1);
        ShopApprovalRule testShopApprovalRule = shopApprovalRuleList.get(shopApprovalRuleList.size() - 1);
        assertThat(testShopApprovalRule.getApprovalRule()).isEqualTo(DEFAULT_APPROVAL_RULE);
        assertThat(testShopApprovalRule.getValue()).isEqualTo(DEFAULT_VALUE);
        assertThat(testShopApprovalRule.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testShopApprovalRule.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testShopApprovalRule.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testShopApprovalRule.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testShopApprovalRule.getVersion()).isEqualTo(DEFAULT_VERSION);
    }

    @Test
    @Transactional
    void createShopApprovalRuleWithExistingId() throws Exception {
        // Create the ShopApprovalRule with an existing ID
        shopApprovalRule.setId(1L);
        ShopApprovalRuleDTO shopApprovalRuleDTO = shopApprovalRuleMapper.toDto(shopApprovalRule);

        int databaseSizeBeforeCreate = shopApprovalRuleRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restShopApprovalRuleMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(shopApprovalRuleDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ShopApprovalRule in the database
        List<ShopApprovalRule> shopApprovalRuleList = shopApprovalRuleRepository.findAll();
        assertThat(shopApprovalRuleList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkApprovalRuleIsRequired() throws Exception {
        int databaseSizeBeforeTest = shopApprovalRuleRepository.findAll().size();
        // set the field null
        shopApprovalRule.setApprovalRule(null);

        // Create the ShopApprovalRule, which fails.
        ShopApprovalRuleDTO shopApprovalRuleDTO = shopApprovalRuleMapper.toDto(shopApprovalRule);

        restShopApprovalRuleMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(shopApprovalRuleDTO))
            )
            .andExpect(status().isBadRequest());

        List<ShopApprovalRule> shopApprovalRuleList = shopApprovalRuleRepository.findAll();
        assertThat(shopApprovalRuleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = shopApprovalRuleRepository.findAll().size();
        // set the field null
        shopApprovalRule.setValue(null);

        // Create the ShopApprovalRule, which fails.
        ShopApprovalRuleDTO shopApprovalRuleDTO = shopApprovalRuleMapper.toDto(shopApprovalRule);

        restShopApprovalRuleMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(shopApprovalRuleDTO))
            )
            .andExpect(status().isBadRequest());

        List<ShopApprovalRule> shopApprovalRuleList = shopApprovalRuleRepository.findAll();
        assertThat(shopApprovalRuleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllShopApprovalRules() throws Exception {
        // Initialize the database
        shopApprovalRuleRepository.saveAndFlush(shopApprovalRule);

        // Get all the shopApprovalRuleList
        restShopApprovalRuleMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shopApprovalRule.getId().intValue())))
            .andExpect(jsonPath("$.[*].approvalRule").value(hasItem(DEFAULT_APPROVAL_RULE.toString())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.intValue())));
    }

    @Test
    @Transactional
    void getShopApprovalRule() throws Exception {
        // Initialize the database
        shopApprovalRuleRepository.saveAndFlush(shopApprovalRule);

        // Get the shopApprovalRule
        restShopApprovalRuleMockMvc
            .perform(get(ENTITY_API_URL_ID, shopApprovalRule.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(shopApprovalRule.getId().intValue()))
            .andExpect(jsonPath("$.approvalRule").value(DEFAULT_APPROVAL_RULE.toString()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION.intValue()));
    }

    @Test
    @Transactional
    void getNonExistingShopApprovalRule() throws Exception {
        // Get the shopApprovalRule
        restShopApprovalRuleMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewShopApprovalRule() throws Exception {
        // Initialize the database
        shopApprovalRuleRepository.saveAndFlush(shopApprovalRule);

        int databaseSizeBeforeUpdate = shopApprovalRuleRepository.findAll().size();

        // Update the shopApprovalRule
        ShopApprovalRule updatedShopApprovalRule = shopApprovalRuleRepository.findById(shopApprovalRule.getId()).get();
        // Disconnect from session so that the updates on updatedShopApprovalRule are not directly saved in db
        em.detach(updatedShopApprovalRule);
        updatedShopApprovalRule
            .approvalRule(UPDATED_APPROVAL_RULE)
            .value(UPDATED_VALUE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);
        ShopApprovalRuleDTO shopApprovalRuleDTO = shopApprovalRuleMapper.toDto(updatedShopApprovalRule);

        restShopApprovalRuleMockMvc
            .perform(
                put(ENTITY_API_URL_ID, shopApprovalRuleDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(shopApprovalRuleDTO))
            )
            .andExpect(status().isOk());

        // Validate the ShopApprovalRule in the database
        List<ShopApprovalRule> shopApprovalRuleList = shopApprovalRuleRepository.findAll();
        assertThat(shopApprovalRuleList).hasSize(databaseSizeBeforeUpdate);
        ShopApprovalRule testShopApprovalRule = shopApprovalRuleList.get(shopApprovalRuleList.size() - 1);
        assertThat(testShopApprovalRule.getApprovalRule()).isEqualTo(UPDATED_APPROVAL_RULE);
        assertThat(testShopApprovalRule.getValue()).isEqualTo(UPDATED_VALUE);
        assertThat(testShopApprovalRule.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testShopApprovalRule.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testShopApprovalRule.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testShopApprovalRule.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testShopApprovalRule.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void putNonExistingShopApprovalRule() throws Exception {
        int databaseSizeBeforeUpdate = shopApprovalRuleRepository.findAll().size();
        shopApprovalRule.setId(count.incrementAndGet());

        // Create the ShopApprovalRule
        ShopApprovalRuleDTO shopApprovalRuleDTO = shopApprovalRuleMapper.toDto(shopApprovalRule);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restShopApprovalRuleMockMvc
            .perform(
                put(ENTITY_API_URL_ID, shopApprovalRuleDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(shopApprovalRuleDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ShopApprovalRule in the database
        List<ShopApprovalRule> shopApprovalRuleList = shopApprovalRuleRepository.findAll();
        assertThat(shopApprovalRuleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchShopApprovalRule() throws Exception {
        int databaseSizeBeforeUpdate = shopApprovalRuleRepository.findAll().size();
        shopApprovalRule.setId(count.incrementAndGet());

        // Create the ShopApprovalRule
        ShopApprovalRuleDTO shopApprovalRuleDTO = shopApprovalRuleMapper.toDto(shopApprovalRule);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restShopApprovalRuleMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(shopApprovalRuleDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ShopApprovalRule in the database
        List<ShopApprovalRule> shopApprovalRuleList = shopApprovalRuleRepository.findAll();
        assertThat(shopApprovalRuleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamShopApprovalRule() throws Exception {
        int databaseSizeBeforeUpdate = shopApprovalRuleRepository.findAll().size();
        shopApprovalRule.setId(count.incrementAndGet());

        // Create the ShopApprovalRule
        ShopApprovalRuleDTO shopApprovalRuleDTO = shopApprovalRuleMapper.toDto(shopApprovalRule);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restShopApprovalRuleMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(shopApprovalRuleDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ShopApprovalRule in the database
        List<ShopApprovalRule> shopApprovalRuleList = shopApprovalRuleRepository.findAll();
        assertThat(shopApprovalRuleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateShopApprovalRuleWithPatch() throws Exception {
        // Initialize the database
        shopApprovalRuleRepository.saveAndFlush(shopApprovalRule);

        int databaseSizeBeforeUpdate = shopApprovalRuleRepository.findAll().size();

        // Update the shopApprovalRule using partial update
        ShopApprovalRule partialUpdatedShopApprovalRule = new ShopApprovalRule();
        partialUpdatedShopApprovalRule.setId(shopApprovalRule.getId());

        partialUpdatedShopApprovalRule
            .value(UPDATED_VALUE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);

        restShopApprovalRuleMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedShopApprovalRule.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedShopApprovalRule))
            )
            .andExpect(status().isOk());

        // Validate the ShopApprovalRule in the database
        List<ShopApprovalRule> shopApprovalRuleList = shopApprovalRuleRepository.findAll();
        assertThat(shopApprovalRuleList).hasSize(databaseSizeBeforeUpdate);
        ShopApprovalRule testShopApprovalRule = shopApprovalRuleList.get(shopApprovalRuleList.size() - 1);
        assertThat(testShopApprovalRule.getApprovalRule()).isEqualTo(DEFAULT_APPROVAL_RULE);
        assertThat(testShopApprovalRule.getValue()).isEqualTo(UPDATED_VALUE);
        assertThat(testShopApprovalRule.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testShopApprovalRule.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testShopApprovalRule.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testShopApprovalRule.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testShopApprovalRule.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void fullUpdateShopApprovalRuleWithPatch() throws Exception {
        // Initialize the database
        shopApprovalRuleRepository.saveAndFlush(shopApprovalRule);

        int databaseSizeBeforeUpdate = shopApprovalRuleRepository.findAll().size();

        // Update the shopApprovalRule using partial update
        ShopApprovalRule partialUpdatedShopApprovalRule = new ShopApprovalRule();
        partialUpdatedShopApprovalRule.setId(shopApprovalRule.getId());

        partialUpdatedShopApprovalRule
            .approvalRule(UPDATED_APPROVAL_RULE)
            .value(UPDATED_VALUE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);

        restShopApprovalRuleMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedShopApprovalRule.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedShopApprovalRule))
            )
            .andExpect(status().isOk());

        // Validate the ShopApprovalRule in the database
        List<ShopApprovalRule> shopApprovalRuleList = shopApprovalRuleRepository.findAll();
        assertThat(shopApprovalRuleList).hasSize(databaseSizeBeforeUpdate);
        ShopApprovalRule testShopApprovalRule = shopApprovalRuleList.get(shopApprovalRuleList.size() - 1);
        assertThat(testShopApprovalRule.getApprovalRule()).isEqualTo(UPDATED_APPROVAL_RULE);
        assertThat(testShopApprovalRule.getValue()).isEqualTo(UPDATED_VALUE);
        assertThat(testShopApprovalRule.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testShopApprovalRule.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testShopApprovalRule.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testShopApprovalRule.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testShopApprovalRule.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void patchNonExistingShopApprovalRule() throws Exception {
        int databaseSizeBeforeUpdate = shopApprovalRuleRepository.findAll().size();
        shopApprovalRule.setId(count.incrementAndGet());

        // Create the ShopApprovalRule
        ShopApprovalRuleDTO shopApprovalRuleDTO = shopApprovalRuleMapper.toDto(shopApprovalRule);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restShopApprovalRuleMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, shopApprovalRuleDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(shopApprovalRuleDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ShopApprovalRule in the database
        List<ShopApprovalRule> shopApprovalRuleList = shopApprovalRuleRepository.findAll();
        assertThat(shopApprovalRuleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchShopApprovalRule() throws Exception {
        int databaseSizeBeforeUpdate = shopApprovalRuleRepository.findAll().size();
        shopApprovalRule.setId(count.incrementAndGet());

        // Create the ShopApprovalRule
        ShopApprovalRuleDTO shopApprovalRuleDTO = shopApprovalRuleMapper.toDto(shopApprovalRule);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restShopApprovalRuleMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(shopApprovalRuleDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ShopApprovalRule in the database
        List<ShopApprovalRule> shopApprovalRuleList = shopApprovalRuleRepository.findAll();
        assertThat(shopApprovalRuleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamShopApprovalRule() throws Exception {
        int databaseSizeBeforeUpdate = shopApprovalRuleRepository.findAll().size();
        shopApprovalRule.setId(count.incrementAndGet());

        // Create the ShopApprovalRule
        ShopApprovalRuleDTO shopApprovalRuleDTO = shopApprovalRuleMapper.toDto(shopApprovalRule);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restShopApprovalRuleMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(shopApprovalRuleDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ShopApprovalRule in the database
        List<ShopApprovalRule> shopApprovalRuleList = shopApprovalRuleRepository.findAll();
        assertThat(shopApprovalRuleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteShopApprovalRule() throws Exception {
        // Initialize the database
        shopApprovalRuleRepository.saveAndFlush(shopApprovalRule);

        int databaseSizeBeforeDelete = shopApprovalRuleRepository.findAll().size();

        // Delete the shopApprovalRule
        restShopApprovalRuleMockMvc
            .perform(delete(ENTITY_API_URL_ID, shopApprovalRule.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ShopApprovalRule> shopApprovalRuleList = shopApprovalRuleRepository.findAll();
        assertThat(shopApprovalRuleList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
