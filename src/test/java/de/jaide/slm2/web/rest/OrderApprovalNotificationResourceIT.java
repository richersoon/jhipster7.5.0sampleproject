package de.jaide.slm2.web.rest;

import static de.jaide.slm2.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import de.jaide.slm2.IntegrationTest;
import de.jaide.slm2.domain.OrderApprovalNotification;
import de.jaide.slm2.domain.enumeration.OrderApprovalStatus;
import de.jaide.slm2.repository.OrderApprovalNotificationRepository;
import de.jaide.slm2.service.dto.OrderApprovalNotificationDTO;
import de.jaide.slm2.service.mapper.OrderApprovalNotificationMapper;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link OrderApprovalNotificationResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class OrderApprovalNotificationResourceIT {

    private static final OrderApprovalStatus DEFAULT_STATUS = OrderApprovalStatus.PENDING;
    private static final OrderApprovalStatus UPDATED_STATUS = OrderApprovalStatus.COMPLETED;

    private static final String DEFAULT_BROWSER_IP_CITY = "AAAAAAAAAA";
    private static final String UPDATED_BROWSER_IP_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_BROWSER_IP_PROVINCE = "AAAAAAAAAA";
    private static final String UPDATED_BROWSER_IP_PROVINCE = "BBBBBBBBBB";

    private static final String DEFAULT_BROWSER_IP_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_BROWSER_IP_COUNTRY = "BBBBBBBBBB";

    private static final String DEFAULT_BROWSER_IP_LATITUDE = "AAAAAAAAAA";
    private static final String UPDATED_BROWSER_IP_LATITUDE = "BBBBBBBBBB";

    private static final String DEFAULT_BROWSER_IP_LONGITUDE = "AAAAAAAAAA";
    private static final String UPDATED_BROWSER_IP_LONGITUDE = "BBBBBBBBBB";

    private static final String DEFAULT_BROWSER_IP_ZIP = "AAAAAAAAAA";
    private static final String UPDATED_BROWSER_IP_ZIP = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_DISTANCE_KM = new BigDecimal(1);
    private static final BigDecimal UPDATED_DISTANCE_KM = new BigDecimal(2);

    private static final Integer DEFAULT_DURATION_SECONDS = 1;
    private static final Integer UPDATED_DURATION_SECONDS = 2;

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_VERSION = 1L;
    private static final Long UPDATED_VERSION = 2L;

    private static final String ENTITY_API_URL = "/api/order-approval-notifications";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private OrderApprovalNotificationRepository orderApprovalNotificationRepository;

    @Autowired
    private OrderApprovalNotificationMapper orderApprovalNotificationMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOrderApprovalNotificationMockMvc;

    private OrderApprovalNotification orderApprovalNotification;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderApprovalNotification createEntity(EntityManager em) {
        OrderApprovalNotification orderApprovalNotification = new OrderApprovalNotification()
            .status(DEFAULT_STATUS)
            .browserIpCity(DEFAULT_BROWSER_IP_CITY)
            .browserIpProvince(DEFAULT_BROWSER_IP_PROVINCE)
            .browserIpCountry(DEFAULT_BROWSER_IP_COUNTRY)
            .browserIpLatitude(DEFAULT_BROWSER_IP_LATITUDE)
            .browserIpLongitude(DEFAULT_BROWSER_IP_LONGITUDE)
            .browserIpZip(DEFAULT_BROWSER_IP_ZIP)
            .distanceKm(DEFAULT_DISTANCE_KM)
            .durationSeconds(DEFAULT_DURATION_SECONDS)
            .note(DEFAULT_NOTE)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .version(DEFAULT_VERSION);
        return orderApprovalNotification;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderApprovalNotification createUpdatedEntity(EntityManager em) {
        OrderApprovalNotification orderApprovalNotification = new OrderApprovalNotification()
            .status(UPDATED_STATUS)
            .browserIpCity(UPDATED_BROWSER_IP_CITY)
            .browserIpProvince(UPDATED_BROWSER_IP_PROVINCE)
            .browserIpCountry(UPDATED_BROWSER_IP_COUNTRY)
            .browserIpLatitude(UPDATED_BROWSER_IP_LATITUDE)
            .browserIpLongitude(UPDATED_BROWSER_IP_LONGITUDE)
            .browserIpZip(UPDATED_BROWSER_IP_ZIP)
            .distanceKm(UPDATED_DISTANCE_KM)
            .durationSeconds(UPDATED_DURATION_SECONDS)
            .note(UPDATED_NOTE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);
        return orderApprovalNotification;
    }

    @BeforeEach
    public void initTest() {
        orderApprovalNotification = createEntity(em);
    }

    @Test
    @Transactional
    void createOrderApprovalNotification() throws Exception {
        int databaseSizeBeforeCreate = orderApprovalNotificationRepository.findAll().size();
        // Create the OrderApprovalNotification
        OrderApprovalNotificationDTO orderApprovalNotificationDTO = orderApprovalNotificationMapper.toDto(orderApprovalNotification);
        restOrderApprovalNotificationMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(orderApprovalNotificationDTO))
            )
            .andExpect(status().isCreated());

        // Validate the OrderApprovalNotification in the database
        List<OrderApprovalNotification> orderApprovalNotificationList = orderApprovalNotificationRepository.findAll();
        assertThat(orderApprovalNotificationList).hasSize(databaseSizeBeforeCreate + 1);
        OrderApprovalNotification testOrderApprovalNotification = orderApprovalNotificationList.get(
            orderApprovalNotificationList.size() - 1
        );
        assertThat(testOrderApprovalNotification.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testOrderApprovalNotification.getBrowserIpCity()).isEqualTo(DEFAULT_BROWSER_IP_CITY);
        assertThat(testOrderApprovalNotification.getBrowserIpProvince()).isEqualTo(DEFAULT_BROWSER_IP_PROVINCE);
        assertThat(testOrderApprovalNotification.getBrowserIpCountry()).isEqualTo(DEFAULT_BROWSER_IP_COUNTRY);
        assertThat(testOrderApprovalNotification.getBrowserIpLatitude()).isEqualTo(DEFAULT_BROWSER_IP_LATITUDE);
        assertThat(testOrderApprovalNotification.getBrowserIpLongitude()).isEqualTo(DEFAULT_BROWSER_IP_LONGITUDE);
        assertThat(testOrderApprovalNotification.getBrowserIpZip()).isEqualTo(DEFAULT_BROWSER_IP_ZIP);
        assertThat(testOrderApprovalNotification.getDistanceKm()).isEqualByComparingTo(DEFAULT_DISTANCE_KM);
        assertThat(testOrderApprovalNotification.getDurationSeconds()).isEqualTo(DEFAULT_DURATION_SECONDS);
        assertThat(testOrderApprovalNotification.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testOrderApprovalNotification.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testOrderApprovalNotification.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testOrderApprovalNotification.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testOrderApprovalNotification.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testOrderApprovalNotification.getVersion()).isEqualTo(DEFAULT_VERSION);
    }

    @Test
    @Transactional
    void createOrderApprovalNotificationWithExistingId() throws Exception {
        // Create the OrderApprovalNotification with an existing ID
        orderApprovalNotification.setId(1L);
        OrderApprovalNotificationDTO orderApprovalNotificationDTO = orderApprovalNotificationMapper.toDto(orderApprovalNotification);

        int databaseSizeBeforeCreate = orderApprovalNotificationRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrderApprovalNotificationMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(orderApprovalNotificationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrderApprovalNotification in the database
        List<OrderApprovalNotification> orderApprovalNotificationList = orderApprovalNotificationRepository.findAll();
        assertThat(orderApprovalNotificationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllOrderApprovalNotifications() throws Exception {
        // Initialize the database
        orderApprovalNotificationRepository.saveAndFlush(orderApprovalNotification);

        // Get all the orderApprovalNotificationList
        restOrderApprovalNotificationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(orderApprovalNotification.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].browserIpCity").value(hasItem(DEFAULT_BROWSER_IP_CITY)))
            .andExpect(jsonPath("$.[*].browserIpProvince").value(hasItem(DEFAULT_BROWSER_IP_PROVINCE)))
            .andExpect(jsonPath("$.[*].browserIpCountry").value(hasItem(DEFAULT_BROWSER_IP_COUNTRY)))
            .andExpect(jsonPath("$.[*].browserIpLatitude").value(hasItem(DEFAULT_BROWSER_IP_LATITUDE)))
            .andExpect(jsonPath("$.[*].browserIpLongitude").value(hasItem(DEFAULT_BROWSER_IP_LONGITUDE)))
            .andExpect(jsonPath("$.[*].browserIpZip").value(hasItem(DEFAULT_BROWSER_IP_ZIP)))
            .andExpect(jsonPath("$.[*].distanceKm").value(hasItem(sameNumber(DEFAULT_DISTANCE_KM))))
            .andExpect(jsonPath("$.[*].durationSeconds").value(hasItem(DEFAULT_DURATION_SECONDS)))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.intValue())));
    }

    @Test
    @Transactional
    void getOrderApprovalNotification() throws Exception {
        // Initialize the database
        orderApprovalNotificationRepository.saveAndFlush(orderApprovalNotification);

        // Get the orderApprovalNotification
        restOrderApprovalNotificationMockMvc
            .perform(get(ENTITY_API_URL_ID, orderApprovalNotification.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(orderApprovalNotification.getId().intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.browserIpCity").value(DEFAULT_BROWSER_IP_CITY))
            .andExpect(jsonPath("$.browserIpProvince").value(DEFAULT_BROWSER_IP_PROVINCE))
            .andExpect(jsonPath("$.browserIpCountry").value(DEFAULT_BROWSER_IP_COUNTRY))
            .andExpect(jsonPath("$.browserIpLatitude").value(DEFAULT_BROWSER_IP_LATITUDE))
            .andExpect(jsonPath("$.browserIpLongitude").value(DEFAULT_BROWSER_IP_LONGITUDE))
            .andExpect(jsonPath("$.browserIpZip").value(DEFAULT_BROWSER_IP_ZIP))
            .andExpect(jsonPath("$.distanceKm").value(sameNumber(DEFAULT_DISTANCE_KM)))
            .andExpect(jsonPath("$.durationSeconds").value(DEFAULT_DURATION_SECONDS))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION.intValue()));
    }

    @Test
    @Transactional
    void getNonExistingOrderApprovalNotification() throws Exception {
        // Get the orderApprovalNotification
        restOrderApprovalNotificationMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewOrderApprovalNotification() throws Exception {
        // Initialize the database
        orderApprovalNotificationRepository.saveAndFlush(orderApprovalNotification);

        int databaseSizeBeforeUpdate = orderApprovalNotificationRepository.findAll().size();

        // Update the orderApprovalNotification
        OrderApprovalNotification updatedOrderApprovalNotification = orderApprovalNotificationRepository
            .findById(orderApprovalNotification.getId())
            .get();
        // Disconnect from session so that the updates on updatedOrderApprovalNotification are not directly saved in db
        em.detach(updatedOrderApprovalNotification);
        updatedOrderApprovalNotification
            .status(UPDATED_STATUS)
            .browserIpCity(UPDATED_BROWSER_IP_CITY)
            .browserIpProvince(UPDATED_BROWSER_IP_PROVINCE)
            .browserIpCountry(UPDATED_BROWSER_IP_COUNTRY)
            .browserIpLatitude(UPDATED_BROWSER_IP_LATITUDE)
            .browserIpLongitude(UPDATED_BROWSER_IP_LONGITUDE)
            .browserIpZip(UPDATED_BROWSER_IP_ZIP)
            .distanceKm(UPDATED_DISTANCE_KM)
            .durationSeconds(UPDATED_DURATION_SECONDS)
            .note(UPDATED_NOTE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);
        OrderApprovalNotificationDTO orderApprovalNotificationDTO = orderApprovalNotificationMapper.toDto(updatedOrderApprovalNotification);

        restOrderApprovalNotificationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, orderApprovalNotificationDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(orderApprovalNotificationDTO))
            )
            .andExpect(status().isOk());

        // Validate the OrderApprovalNotification in the database
        List<OrderApprovalNotification> orderApprovalNotificationList = orderApprovalNotificationRepository.findAll();
        assertThat(orderApprovalNotificationList).hasSize(databaseSizeBeforeUpdate);
        OrderApprovalNotification testOrderApprovalNotification = orderApprovalNotificationList.get(
            orderApprovalNotificationList.size() - 1
        );
        assertThat(testOrderApprovalNotification.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testOrderApprovalNotification.getBrowserIpCity()).isEqualTo(UPDATED_BROWSER_IP_CITY);
        assertThat(testOrderApprovalNotification.getBrowserIpProvince()).isEqualTo(UPDATED_BROWSER_IP_PROVINCE);
        assertThat(testOrderApprovalNotification.getBrowserIpCountry()).isEqualTo(UPDATED_BROWSER_IP_COUNTRY);
        assertThat(testOrderApprovalNotification.getBrowserIpLatitude()).isEqualTo(UPDATED_BROWSER_IP_LATITUDE);
        assertThat(testOrderApprovalNotification.getBrowserIpLongitude()).isEqualTo(UPDATED_BROWSER_IP_LONGITUDE);
        assertThat(testOrderApprovalNotification.getBrowserIpZip()).isEqualTo(UPDATED_BROWSER_IP_ZIP);
        assertThat(testOrderApprovalNotification.getDistanceKm()).isEqualTo(UPDATED_DISTANCE_KM);
        assertThat(testOrderApprovalNotification.getDurationSeconds()).isEqualTo(UPDATED_DURATION_SECONDS);
        assertThat(testOrderApprovalNotification.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testOrderApprovalNotification.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testOrderApprovalNotification.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testOrderApprovalNotification.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testOrderApprovalNotification.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testOrderApprovalNotification.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void putNonExistingOrderApprovalNotification() throws Exception {
        int databaseSizeBeforeUpdate = orderApprovalNotificationRepository.findAll().size();
        orderApprovalNotification.setId(count.incrementAndGet());

        // Create the OrderApprovalNotification
        OrderApprovalNotificationDTO orderApprovalNotificationDTO = orderApprovalNotificationMapper.toDto(orderApprovalNotification);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrderApprovalNotificationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, orderApprovalNotificationDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(orderApprovalNotificationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrderApprovalNotification in the database
        List<OrderApprovalNotification> orderApprovalNotificationList = orderApprovalNotificationRepository.findAll();
        assertThat(orderApprovalNotificationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchOrderApprovalNotification() throws Exception {
        int databaseSizeBeforeUpdate = orderApprovalNotificationRepository.findAll().size();
        orderApprovalNotification.setId(count.incrementAndGet());

        // Create the OrderApprovalNotification
        OrderApprovalNotificationDTO orderApprovalNotificationDTO = orderApprovalNotificationMapper.toDto(orderApprovalNotification);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrderApprovalNotificationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(orderApprovalNotificationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrderApprovalNotification in the database
        List<OrderApprovalNotification> orderApprovalNotificationList = orderApprovalNotificationRepository.findAll();
        assertThat(orderApprovalNotificationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamOrderApprovalNotification() throws Exception {
        int databaseSizeBeforeUpdate = orderApprovalNotificationRepository.findAll().size();
        orderApprovalNotification.setId(count.incrementAndGet());

        // Create the OrderApprovalNotification
        OrderApprovalNotificationDTO orderApprovalNotificationDTO = orderApprovalNotificationMapper.toDto(orderApprovalNotification);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrderApprovalNotificationMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(orderApprovalNotificationDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the OrderApprovalNotification in the database
        List<OrderApprovalNotification> orderApprovalNotificationList = orderApprovalNotificationRepository.findAll();
        assertThat(orderApprovalNotificationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateOrderApprovalNotificationWithPatch() throws Exception {
        // Initialize the database
        orderApprovalNotificationRepository.saveAndFlush(orderApprovalNotification);

        int databaseSizeBeforeUpdate = orderApprovalNotificationRepository.findAll().size();

        // Update the orderApprovalNotification using partial update
        OrderApprovalNotification partialUpdatedOrderApprovalNotification = new OrderApprovalNotification();
        partialUpdatedOrderApprovalNotification.setId(orderApprovalNotification.getId());

        partialUpdatedOrderApprovalNotification
            .status(UPDATED_STATUS)
            .browserIpProvince(UPDATED_BROWSER_IP_PROVINCE)
            .browserIpCountry(UPDATED_BROWSER_IP_COUNTRY)
            .browserIpZip(UPDATED_BROWSER_IP_ZIP)
            .durationSeconds(UPDATED_DURATION_SECONDS)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .version(UPDATED_VERSION);

        restOrderApprovalNotificationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOrderApprovalNotification.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOrderApprovalNotification))
            )
            .andExpect(status().isOk());

        // Validate the OrderApprovalNotification in the database
        List<OrderApprovalNotification> orderApprovalNotificationList = orderApprovalNotificationRepository.findAll();
        assertThat(orderApprovalNotificationList).hasSize(databaseSizeBeforeUpdate);
        OrderApprovalNotification testOrderApprovalNotification = orderApprovalNotificationList.get(
            orderApprovalNotificationList.size() - 1
        );
        assertThat(testOrderApprovalNotification.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testOrderApprovalNotification.getBrowserIpCity()).isEqualTo(DEFAULT_BROWSER_IP_CITY);
        assertThat(testOrderApprovalNotification.getBrowserIpProvince()).isEqualTo(UPDATED_BROWSER_IP_PROVINCE);
        assertThat(testOrderApprovalNotification.getBrowserIpCountry()).isEqualTo(UPDATED_BROWSER_IP_COUNTRY);
        assertThat(testOrderApprovalNotification.getBrowserIpLatitude()).isEqualTo(DEFAULT_BROWSER_IP_LATITUDE);
        assertThat(testOrderApprovalNotification.getBrowserIpLongitude()).isEqualTo(DEFAULT_BROWSER_IP_LONGITUDE);
        assertThat(testOrderApprovalNotification.getBrowserIpZip()).isEqualTo(UPDATED_BROWSER_IP_ZIP);
        assertThat(testOrderApprovalNotification.getDistanceKm()).isEqualByComparingTo(DEFAULT_DISTANCE_KM);
        assertThat(testOrderApprovalNotification.getDurationSeconds()).isEqualTo(UPDATED_DURATION_SECONDS);
        assertThat(testOrderApprovalNotification.getNote()).isEqualTo(DEFAULT_NOTE);
        assertThat(testOrderApprovalNotification.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testOrderApprovalNotification.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testOrderApprovalNotification.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testOrderApprovalNotification.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testOrderApprovalNotification.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void fullUpdateOrderApprovalNotificationWithPatch() throws Exception {
        // Initialize the database
        orderApprovalNotificationRepository.saveAndFlush(orderApprovalNotification);

        int databaseSizeBeforeUpdate = orderApprovalNotificationRepository.findAll().size();

        // Update the orderApprovalNotification using partial update
        OrderApprovalNotification partialUpdatedOrderApprovalNotification = new OrderApprovalNotification();
        partialUpdatedOrderApprovalNotification.setId(orderApprovalNotification.getId());

        partialUpdatedOrderApprovalNotification
            .status(UPDATED_STATUS)
            .browserIpCity(UPDATED_BROWSER_IP_CITY)
            .browserIpProvince(UPDATED_BROWSER_IP_PROVINCE)
            .browserIpCountry(UPDATED_BROWSER_IP_COUNTRY)
            .browserIpLatitude(UPDATED_BROWSER_IP_LATITUDE)
            .browserIpLongitude(UPDATED_BROWSER_IP_LONGITUDE)
            .browserIpZip(UPDATED_BROWSER_IP_ZIP)
            .distanceKm(UPDATED_DISTANCE_KM)
            .durationSeconds(UPDATED_DURATION_SECONDS)
            .note(UPDATED_NOTE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .version(UPDATED_VERSION);

        restOrderApprovalNotificationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedOrderApprovalNotification.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOrderApprovalNotification))
            )
            .andExpect(status().isOk());

        // Validate the OrderApprovalNotification in the database
        List<OrderApprovalNotification> orderApprovalNotificationList = orderApprovalNotificationRepository.findAll();
        assertThat(orderApprovalNotificationList).hasSize(databaseSizeBeforeUpdate);
        OrderApprovalNotification testOrderApprovalNotification = orderApprovalNotificationList.get(
            orderApprovalNotificationList.size() - 1
        );
        assertThat(testOrderApprovalNotification.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testOrderApprovalNotification.getBrowserIpCity()).isEqualTo(UPDATED_BROWSER_IP_CITY);
        assertThat(testOrderApprovalNotification.getBrowserIpProvince()).isEqualTo(UPDATED_BROWSER_IP_PROVINCE);
        assertThat(testOrderApprovalNotification.getBrowserIpCountry()).isEqualTo(UPDATED_BROWSER_IP_COUNTRY);
        assertThat(testOrderApprovalNotification.getBrowserIpLatitude()).isEqualTo(UPDATED_BROWSER_IP_LATITUDE);
        assertThat(testOrderApprovalNotification.getBrowserIpLongitude()).isEqualTo(UPDATED_BROWSER_IP_LONGITUDE);
        assertThat(testOrderApprovalNotification.getBrowserIpZip()).isEqualTo(UPDATED_BROWSER_IP_ZIP);
        assertThat(testOrderApprovalNotification.getDistanceKm()).isEqualByComparingTo(UPDATED_DISTANCE_KM);
        assertThat(testOrderApprovalNotification.getDurationSeconds()).isEqualTo(UPDATED_DURATION_SECONDS);
        assertThat(testOrderApprovalNotification.getNote()).isEqualTo(UPDATED_NOTE);
        assertThat(testOrderApprovalNotification.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testOrderApprovalNotification.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testOrderApprovalNotification.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testOrderApprovalNotification.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testOrderApprovalNotification.getVersion()).isEqualTo(UPDATED_VERSION);
    }

    @Test
    @Transactional
    void patchNonExistingOrderApprovalNotification() throws Exception {
        int databaseSizeBeforeUpdate = orderApprovalNotificationRepository.findAll().size();
        orderApprovalNotification.setId(count.incrementAndGet());

        // Create the OrderApprovalNotification
        OrderApprovalNotificationDTO orderApprovalNotificationDTO = orderApprovalNotificationMapper.toDto(orderApprovalNotification);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrderApprovalNotificationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, orderApprovalNotificationDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(orderApprovalNotificationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrderApprovalNotification in the database
        List<OrderApprovalNotification> orderApprovalNotificationList = orderApprovalNotificationRepository.findAll();
        assertThat(orderApprovalNotificationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchOrderApprovalNotification() throws Exception {
        int databaseSizeBeforeUpdate = orderApprovalNotificationRepository.findAll().size();
        orderApprovalNotification.setId(count.incrementAndGet());

        // Create the OrderApprovalNotification
        OrderApprovalNotificationDTO orderApprovalNotificationDTO = orderApprovalNotificationMapper.toDto(orderApprovalNotification);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrderApprovalNotificationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(orderApprovalNotificationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the OrderApprovalNotification in the database
        List<OrderApprovalNotification> orderApprovalNotificationList = orderApprovalNotificationRepository.findAll();
        assertThat(orderApprovalNotificationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamOrderApprovalNotification() throws Exception {
        int databaseSizeBeforeUpdate = orderApprovalNotificationRepository.findAll().size();
        orderApprovalNotification.setId(count.incrementAndGet());

        // Create the OrderApprovalNotification
        OrderApprovalNotificationDTO orderApprovalNotificationDTO = orderApprovalNotificationMapper.toDto(orderApprovalNotification);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restOrderApprovalNotificationMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(orderApprovalNotificationDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the OrderApprovalNotification in the database
        List<OrderApprovalNotification> orderApprovalNotificationList = orderApprovalNotificationRepository.findAll();
        assertThat(orderApprovalNotificationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteOrderApprovalNotification() throws Exception {
        // Initialize the database
        orderApprovalNotificationRepository.saveAndFlush(orderApprovalNotification);

        int databaseSizeBeforeDelete = orderApprovalNotificationRepository.findAll().size();

        // Delete the orderApprovalNotification
        restOrderApprovalNotificationMockMvc
            .perform(delete(ENTITY_API_URL_ID, orderApprovalNotification.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OrderApprovalNotification> orderApprovalNotificationList = orderApprovalNotificationRepository.findAll();
        assertThat(orderApprovalNotificationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
