package de.jaide.slm2.domain;

import static org.assertj.core.api.Assertions.assertThat;

import de.jaide.slm2.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class OrderNoteTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrderNote.class);
        OrderNote orderNote1 = new OrderNote();
        orderNote1.setId(1L);
        OrderNote orderNote2 = new OrderNote();
        orderNote2.setId(orderNote1.getId());
        assertThat(orderNote1).isEqualTo(orderNote2);
        orderNote2.setId(2L);
        assertThat(orderNote1).isNotEqualTo(orderNote2);
        orderNote1.setId(null);
        assertThat(orderNote1).isNotEqualTo(orderNote2);
    }
}
