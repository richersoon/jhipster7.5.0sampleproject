package de.jaide.slm2.domain;

import static org.assertj.core.api.Assertions.assertThat;

import de.jaide.slm2.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class WebhookSubscriptionTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(WebhookSubscription.class);
        WebhookSubscription webhookSubscription1 = new WebhookSubscription();
        webhookSubscription1.setId(1L);
        WebhookSubscription webhookSubscription2 = new WebhookSubscription();
        webhookSubscription2.setId(webhookSubscription1.getId());
        assertThat(webhookSubscription1).isEqualTo(webhookSubscription2);
        webhookSubscription2.setId(2L);
        assertThat(webhookSubscription1).isNotEqualTo(webhookSubscription2);
        webhookSubscription1.setId(null);
        assertThat(webhookSubscription1).isNotEqualTo(webhookSubscription2);
    }
}
