package de.jaide.slm2.domain;

import static org.assertj.core.api.Assertions.assertThat;

import de.jaide.slm2.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ShopSmtpTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShopSmtp.class);
        ShopSmtp shopSmtp1 = new ShopSmtp();
        shopSmtp1.setId(1L);
        ShopSmtp shopSmtp2 = new ShopSmtp();
        shopSmtp2.setId(shopSmtp1.getId());
        assertThat(shopSmtp1).isEqualTo(shopSmtp2);
        shopSmtp2.setId(2L);
        assertThat(shopSmtp1).isNotEqualTo(shopSmtp2);
        shopSmtp1.setId(null);
        assertThat(shopSmtp1).isNotEqualTo(shopSmtp2);
    }
}
