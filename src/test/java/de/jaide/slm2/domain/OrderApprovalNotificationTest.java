package de.jaide.slm2.domain;

import static org.assertj.core.api.Assertions.assertThat;

import de.jaide.slm2.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class OrderApprovalNotificationTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrderApprovalNotification.class);
        OrderApprovalNotification orderApprovalNotification1 = new OrderApprovalNotification();
        orderApprovalNotification1.setId(1L);
        OrderApprovalNotification orderApprovalNotification2 = new OrderApprovalNotification();
        orderApprovalNotification2.setId(orderApprovalNotification1.getId());
        assertThat(orderApprovalNotification1).isEqualTo(orderApprovalNotification2);
        orderApprovalNotification2.setId(2L);
        assertThat(orderApprovalNotification1).isNotEqualTo(orderApprovalNotification2);
        orderApprovalNotification1.setId(null);
        assertThat(orderApprovalNotification1).isNotEqualTo(orderApprovalNotification2);
    }
}
