package de.jaide.slm2.domain;

import static org.assertj.core.api.Assertions.assertThat;

import de.jaide.slm2.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ShopApprovalRuleTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShopApprovalRule.class);
        ShopApprovalRule shopApprovalRule1 = new ShopApprovalRule();
        shopApprovalRule1.setId(1L);
        ShopApprovalRule shopApprovalRule2 = new ShopApprovalRule();
        shopApprovalRule2.setId(shopApprovalRule1.getId());
        assertThat(shopApprovalRule1).isEqualTo(shopApprovalRule2);
        shopApprovalRule2.setId(2L);
        assertThat(shopApprovalRule1).isNotEqualTo(shopApprovalRule2);
        shopApprovalRule1.setId(null);
        assertThat(shopApprovalRule1).isNotEqualTo(shopApprovalRule2);
    }
}
