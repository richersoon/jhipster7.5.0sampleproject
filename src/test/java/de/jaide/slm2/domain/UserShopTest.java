package de.jaide.slm2.domain;

import static org.assertj.core.api.Assertions.assertThat;

import de.jaide.slm2.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class UserShopTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserShop.class);
        UserShop userShop1 = new UserShop();
        userShop1.setId(1L);
        UserShop userShop2 = new UserShop();
        userShop2.setId(userShop1.getId());
        assertThat(userShop1).isEqualTo(userShop2);
        userShop2.setId(2L);
        assertThat(userShop1).isNotEqualTo(userShop2);
        userShop1.setId(null);
        assertThat(userShop1).isNotEqualTo(userShop2);
    }
}
