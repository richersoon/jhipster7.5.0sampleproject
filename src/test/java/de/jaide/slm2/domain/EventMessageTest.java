package de.jaide.slm2.domain;

import static org.assertj.core.api.Assertions.assertThat;

import de.jaide.slm2.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class EventMessageTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EventMessage.class);
        EventMessage eventMessage1 = new EventMessage();
        eventMessage1.setId(1L);
        EventMessage eventMessage2 = new EventMessage();
        eventMessage2.setId(eventMessage1.getId());
        assertThat(eventMessage1).isEqualTo(eventMessage2);
        eventMessage2.setId(2L);
        assertThat(eventMessage1).isNotEqualTo(eventMessage2);
        eventMessage1.setId(null);
        assertThat(eventMessage1).isNotEqualTo(eventMessage2);
    }
}
