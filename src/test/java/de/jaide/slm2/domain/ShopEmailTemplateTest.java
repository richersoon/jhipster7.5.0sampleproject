package de.jaide.slm2.domain;

import static org.assertj.core.api.Assertions.assertThat;

import de.jaide.slm2.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ShopEmailTemplateTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ShopEmailTemplate.class);
        ShopEmailTemplate shopEmailTemplate1 = new ShopEmailTemplate();
        shopEmailTemplate1.setId(1L);
        ShopEmailTemplate shopEmailTemplate2 = new ShopEmailTemplate();
        shopEmailTemplate2.setId(shopEmailTemplate1.getId());
        assertThat(shopEmailTemplate1).isEqualTo(shopEmailTemplate2);
        shopEmailTemplate2.setId(2L);
        assertThat(shopEmailTemplate1).isNotEqualTo(shopEmailTemplate2);
        shopEmailTemplate1.setId(null);
        assertThat(shopEmailTemplate1).isNotEqualTo(shopEmailTemplate2);
    }
}
