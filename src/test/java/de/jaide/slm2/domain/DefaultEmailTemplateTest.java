package de.jaide.slm2.domain;

import static org.assertj.core.api.Assertions.assertThat;

import de.jaide.slm2.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DefaultEmailTemplateTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DefaultEmailTemplate.class);
        DefaultEmailTemplate defaultEmailTemplate1 = new DefaultEmailTemplate();
        defaultEmailTemplate1.setId(1L);
        DefaultEmailTemplate defaultEmailTemplate2 = new DefaultEmailTemplate();
        defaultEmailTemplate2.setId(defaultEmailTemplate1.getId());
        assertThat(defaultEmailTemplate1).isEqualTo(defaultEmailTemplate2);
        defaultEmailTemplate2.setId(2L);
        assertThat(defaultEmailTemplate1).isNotEqualTo(defaultEmailTemplate2);
        defaultEmailTemplate1.setId(null);
        assertThat(defaultEmailTemplate1).isNotEqualTo(defaultEmailTemplate2);
    }
}
